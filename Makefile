MAIN = thesis
PLAN = plan
SILENT = &>/dev/null
BATCH = -interaction=batchmode
LATEX = pdflatex -shell-escape $(BATCH) -output-directory=$(1) $(2) $(SILENT)
BIBTEX = bibtex -terse $(1) $(SILENT)

MAINEXT= .pdf
BUILDCOMMAND = $(call LATEX,$(2),$(1)) && $(call BIBTEX,$(1)) && $(call LATEX,$(2),$(1)) && $(call LATEX,$(2),$(1))
#BUILDCOMMAND = @rubber -d $(1)

# list of all source files
TEXSOURCES = $(shell find . -type f -name "*.tex")
BIBSOURCES = $(shell find . -type f -name "*.bib")
BSTSOURCES = $(shell find . -type f -name "*.bst")
STYSOURCES = $(shell find . -type f -name "*.sty")
FIGSOURCES = $(shell find . -type f -name "*.pdf" | grep "figs")

DOTSOURCES = $(shell find . -type f -name "*.dot")
DOTOUTPUTS = $(patsubst %.dot, %_dotfig.pdf, $(DOTSOURCES))

OTHSOURCES = $(BIBSOURCES) $(BSTSOURCES) $(STYSOURCES) $(FIGSOURCES) $(DOTOUTPUTS)

SEPSOURCES = $(shell find . -type f -name "standalone.tex")
SEPOUTPUTS = $(patsubst ./body/%/standalone.tex, ./chapters/%$(MAINEXT), $(SEPSOURCES))

BUILDFILES = *.aux *.log *.bbl *.blg *.dvi *.tmp *.out *.blg *.bbl *.toc *.lof *.lot

OUTPUT = $(SEPOUTPUTS) $(MAIN)$(MAINEXT)

all: $(OUTPUT)
	@rm -f $(BUILDFILES)

debug: SILENT =
debug: BATCH =
debug: all

plan: $(PLAN)$(MAINEXT)
	@echo "Building $@"
	@rm -f $(BUILDFILES)

%_dotfig.pdf: %.dot
	@echo "Building $@"
	@dot -Tpdf $< -o $@

$(MAIN)$(MAINEXT): $(TEXSOURCES) $(OTHSOURCES)
	@echo "Building $@"
	@$(call BUILDCOMMAND,$(MAIN),.)

$(PLAN)$(MAINEXT): body/abstract.tex body/*/plan.tex $(STYSOURCES)
	@echo "Building $@"
	@$(call BUILDCOMMAND,$(PLAN),.)

chapters/%$(MAINEXT): body/%/standalone.tex $(OTHSOURCES) body/%/*.tex head/*.tex | chapters
	@echo "Building $@"
	@$(call BUILDCOMMAND,body/$*/standalone,./body/$*/) || sh -c "cd body/$* && rm -f $(BUILDFILES) && echo -e \"\033[31mERROR:\e[0m Build failed for $@\""
	@mv body/$*/standalone$(MAINEXT) $@
	@cd body/$* && rm -f $(BUILDFILES)

chapters:
	@mkdir -p $@

.PHONY : clean
clean: tidy rmout

.PHONY : tidy
tidy:
	@rm -f $(BUILDFILES)

.PHONY: rmout
rmout:
	@rm -f $(OUTPUT) $(PLAN)$(MAINEXT)
