\section{Hadronic \decay{\bquark}{\squark} transitions}

This thesis presents measurements of charmless \decay{\bquark}{\squark} transitions.
These are processes that, in the Standard Model, are only allowed at loop-level.
This suppression makes them sensitive to contributions from physics beyond the Standard Model~\cite{Raidal:2002ph}.
A major area of interest in \decay{\bquark}{\squark\squarkbar\squark} transitions is the study of \CP violation in charmless \Bs decays.
The decays \BsPhiPhi and \BsPhiKK are examples of \decay{\bquark}{\squark\squarkbar\squark} processes, discussed in Section~\ref{sec:theory:btosss}, while \BdPhiPhi is an example of a \decay{\bquarkbar\dquark}{\squarkbar\squark} annihilation process, discussed in Section~\ref{sec:theory:bdtoss}.

\subsection{$\Bs-\Bsb$ mixing}
\label{sec:theory:mixing}

As discussed in Section~\ref{sec:theory:CPV}, quark mixing allows neutral mesons with non-zero flavour, such as the \Bs, to transform into their own antiparticle without violating conservation laws.
This process is illustrated by the Feynman diagram in Figure~\ref{fig:theory:bsosc} and is a source of indirect \CP violation in \Bs decays.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.6\textwidth]{body/theory/figs/BsMixing}
  \caption{Feynman diagram of $B^0_s-\overline{B^0_s}$ mixing.}
  \label{fig:theory:bsosc}
\end{figure}

The time-evolution of a \Bs meson is described by the Schr\"odinger equation:
\begin{equation}
  i\hbar \frac{\partial}{\partial t} \ket{\PB(t)} = \left( \mathbf{M} - \frac{i}{2}\mathbf{\Gamma} \right)\ket{\PB(t)},
  \label{eq:theory:BsTDSE}
\end{equation}
where $\mathbf{M}$ is the mass matrix, and $\mathbf{\Gamma}$ is the decay matrix.
There are two solutions of the form
\begin{equation}
  \ket{\PB_j(t)} = e^{\left(-im_j - \Gamma_j/2\right)t}\ket{\PB_j(0)},
  \label{eq:theory:BsTDSEsln}
\end{equation}
where $j$ denotes the mass eigenstates, `heavy' ($H$) or `light' ($L$).
These are related to the flavour eigenstates by
\begin{equation}
  \begin{array}{rcl}
    \ket{\PB_H} &=& p\ket{\Bs} + q\ket{\Bsb},\\
    \ket{\PB_L} &=& p\ket{\Bs} - q\ket{\Bsb},
  \end{array}
  \label{eq:theory:Bsmasseigen}
\end{equation}
where $p$ and $q$ are complex numbers.
From Equations~\ref{eq:theory:BsTDSEsln} and \ref{eq:theory:Bsmasseigen}, the time-evolution of a \Bs meson produced in a particular flavour eigenstate at $t=0$ is
\begin{eqnarray}
  \begin{array}{rcl}
    \ket{\Bs(t)}  &=& g_{+}(t)\ket{\Bs}  + \frac{q}{p} g_{-}(t)\ket{\Bsb},\\
    \ket{\Bsb(t)} &=& g_{+}(t)\ket{\Bsb} + \frac{p}{q} g_{-}(t)\ket{\Bs},
  \end{array}
\end{eqnarray}
where
\begin{equation}
  g_{\pm}(t) = \frac{1}{2} \left(
                e^{\left(-im_H - \Gamma_H/2\right)t}
            \pm e^{\left(-im_L - \Gamma_L/2\right)t}
            \right).
\end{equation}

The time-dependent rates of a \Bs decay to a final state, \Pf, and its charge-conjugate process, are defined as
\begin{equation}
  \begin{array}{rcccl}
    \Gamma(t) &=& \left|A(t)\right|^2 &\equiv& \left|\bra{f}H\ket{\Bs(t)}\right|^2\\
    \overline{\Gamma}(t) &=&\left|\overline{A}(t)\right|^2 &\equiv& \left|\bra{\overline{f}}H\ket{\Bsb(t)}\right|^2.
  \end{array}
  \label{eq:theory:decayrate}
\end{equation}

For the decay \BsPhiKK, the final state is self-charge-conjugate $(\ket{\overline{f}} = \ket{f})$.
The analysis presented in Chapter~\ref{ch:BsPhiKK} is time-independent and does not distinguish between \Bs and \Bsb.
Therefore, the measured decay rate is the time-integral of the sum of $\Gamma(t)$ and $\overline{\Gamma}(t)$.
Under the assumptions that equal numbers of \Bs and \Bsb mesons are produced and that \CP violation is small, the time-integral, in terms of the amplitudes at $t=0$, is~\cite{Zhang:2012zk}
\begin{equation}
  \begin{array}{rcl}
    \int^{\infty}_{0} \Gamma(t) + \overline{\Gamma}(t) dt &=& \frac{1}{2}\left(\left|A(0)\right|^2+\left|\overline{A}(0)\right|^2\right)\left(\frac{1}{\Gamma_L}+\frac{1}{\Gamma_H}\right),\\
    &+&\Re\left(\overline{A}(0)A(0)^*\right) \left(\frac{1}{\Gamma_L}-\frac{1}{\Gamma_H}\right).
  \end{array}
  \label{eq:theory:integrateddecayrate}
\end{equation}
The term proportional to $\left(\frac{1}{\Gamma_L}-\frac{1}{\Gamma_H}\right)$ arises from $\Bs-\Bsb$ oscillation and affects the angular distribution of the final state.

\subsection{\decay{\bquark}{\squark\squarkbar\squark} transitions}
\label{sec:theory:btosss}

The transition \decay{\bquark}{\squark\squarkbar\squark} is a flavour-changing neutral current process.
In the Standard Model, there are no vertices which change quark flavour with a neutral gauge boson, so this process is forbidden at tree-level.
The dominant Feynman diagram for this decay, shown in Figure~\ref{fig:theory:penguin}, therefore contains a loop.
The presence of the loop enhances the sensitivity of the decay to the existence of beyond-Standard Model particles with masses too large to be produced directly at the \lhc~\cite{Raidal:2002ph}.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.6\textwidth]{body/theory/figs/btosss}
  \caption[Feynman diagram of a \decay{\bquark}{\squark\squarkbar\squark} decay.]{Feynman diagram of a \decay{\bquark}{\squark\squarkbar\squark} decay. This is an example of a `penguin' diagram, characterised by a single loop formed from a $W$~boson propagator and a quark propagator, with a leg emerging from the quark propagator.}
  \label{fig:theory:penguin}
\end{figure}

Measurements of \CP violation in \decay{\bquark}{\squark\squarkbar\squark} transitions have been performed with decay modes such as \BdPhiKstar~\cite{Chen:2003jfa,Aubert:2003mm,LHCb-PAPER-2014-005}, \BsPhiPhi~\cite{Aaltonen:2011rs,LHCb-PAPER-2014-026} and \decay{\Lb}{\Lambda\phi}~\cite{LHCb-PAPER-2016-002}.
These processes differ by the `spectator' quark content, \ie quarks which appear in the initial and final state and don't partake in the interaction in the leading-order diagrams.
So far, these measurements agree with Standard Model predictions.

Since the weak phase of the diagram in Figure~\ref{fig:theory:penguin}, $\phi_D$, is half that of the diagram in Figure~\ref{fig:theory:bsosc}, $\phi_M$, the residual phase, $\phi_s = \phi_M - 2\phi_D$, in longitudinally polarised \decay{\bquark}{\squark\squarkbar\squark} decays involving a \Bs meson is expected to be close to zero.
The observation of large \CP violation in decays such as \BsPhiPhi or \BsPhiftwop would indicate physics beyond the Standard Model~\cite{Raidal:2002ph}.
The value of $\phi_s$ in \BsPhiPhi has been measured by \lhcb using Run 1 data: $\phi_s = -0.17 \pm 0.15\,\text{(stat)} \pm 0.03\,\text{(syst)}$~\cite{LHCb-PAPER-2014-026}.
The uncertainty is mostly statistical and will be reduced with the addition of data collected by \lhcb in Run 2 and beyond~\cite{LHCb-PAPER-2012-031}.

To search for physics beyond the Standard Model, precise theoretical predictions are needed.
Standard Model predictions for observables in \decay{\bquark}{\squark\squarkbar\squark} decays have large uncertainties due to the difficulty inherent in making QCD calculations.
The technique of QCD factorisation~\cite{Collins:1989gx}, discussed further in Section~\ref{theory:sec:qcdprediction}, uses long-range form-factors which are determined empirically, \eg from global fits to experimental results.
The uncertainties can be reduced by making precision measurements of observables in \decay{\bquark}{\squark\squarkbar\squark} transitions, such as branching fractions and polarisation amplitudes.

