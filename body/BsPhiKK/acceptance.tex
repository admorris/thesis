\section{Acceptance}
\label{sec:BsphiKK:acceptance}
%Introduction
The fit function used in this analysis accounts for the acceptance due to detecting, triggering and selecting events as a function of the decay kinematics.
This is done by multiplying the total decay rate by a four-dimensional acceptance function.
%Method
It is defined in terms of Legendre polynomial ($P_l$) and spherical harmonic ($Y^m_l$) functions of \mass{\Kp\Km} and the helicity angles as
\begin{equation}
  \varepsilon\left(m,\theta_1,\theta_2,\Phi\right) = \sum\limits_{i,j,k,l} c^{ijkl} P_{i} (\cos{\theta_2}) Y_{j}^{k}(\cos{\theta_1},\Phi) P_{l}(\mu),
  \label{eq:BsPhiKK:angacc}
\end{equation}
where $\mu$ is \mass{\Kp\Km} mapped onto the interval $[-1,1]$.
The coefficients $c^{ijkl}$ are calculated from the fully selected \BsPhiKK phase-space simulation sample as the following weighted average:
$$ c^{ijkl} = \frac{1}{N} \sum\limits_{n=0}^{N} \frac{1}{p_nq_n} \frac{2i+1}{2} \frac{2l+1}{2} P_{i} (\cos{\theta_{2}})_n Y_{j}^{k}(\cos{\theta_1},\Phi)_n P_{l}(\mu)_n,$$
where $n$ is the index of each event, and $N$ is the number of events in the sample.
In order to minimise the effect of fluctuations in the simulation sample, only coefficients with values greater than four times their statistical error are kept.

\begin{table}
  \centering
  \caption[Legendre moment coefficients.]{Values of the Legendre moment coefficients found using the \BsPhiKK phase-space simulation sample split by different trigger conditions.}
  \begin{tabular}{|l|r|r|}
    \hline
    $c^{ijkl}$ &                  TOS &              not-TOS \\
    \hline
    $c^{0000}$ & $ 0.0642 \pm 0.0003$ & $ 0.0670 \pm 0.0003$ \\
    $c^{0002}$ & ---                  & $-0.0035 \pm 0.0006$ \\
    $c^{0200}$ & ---                  & $-0.0292 \pm 0.0012$ \\
    $c^{0400}$ & $-0.0135 \pm 0.0015$ & ---                  \\
    $c^{1000}$ & ---                  & $-0.0176 \pm 0.0012$ \\
    $c^{1200}$ & $-0.0134 \pm 0.0026$ & $-0.0164 \pm 0.0025$ \\
    \hline
  \end{tabular}
  \label{tab:BsPhiKK:acceptance}
\end{table}
\def\accfigwidth{0.4}
\begin{figure}[h]
  \centering
  \includegraphics[width=\accfigwidth\textwidth]{body/BsPhiKK/figs/acceptance_mKK_proj_TOS}
  \includegraphics[width=\accfigwidth\textwidth]{body/BsPhiKK/figs/acceptance_phi_proj_TOS}\\
  \includegraphics[width=\accfigwidth\textwidth]{body/BsPhiKK/figs/acceptance_ctheta_1_proj_TOS}
  \includegraphics[width=\accfigwidth\textwidth]{body/BsPhiKK/figs/acceptance_ctheta_2_proj_TOS}
  \caption[1D projections of the acceptance function for TOS events]{1D projections of the acceptance function (red line) calculated for TOS events, overlaid onto the \BsPhiKK simulation sample (black points).}
  \label{fig:BsPhiKK:1DacceptanceTOS}
\end{figure}
\begin{figure}[h]
  \centering
  \includegraphics[width=\accfigwidth\textwidth]{body/BsPhiKK/figs/acceptance_mKK_proj_notTOS}
  \includegraphics[width=\accfigwidth\textwidth]{body/BsPhiKK/figs/acceptance_phi_proj_notTOS}\\
  \includegraphics[width=\accfigwidth\textwidth]{body/BsPhiKK/figs/acceptance_ctheta_1_proj_notTOS}
  \includegraphics[width=\accfigwidth\textwidth]{body/BsPhiKK/figs/acceptance_ctheta_2_proj_notTOS}
  \caption[1D projections of the acceptance function for not-TOS events]{1D projections of the acceptance function (red line) calculated for not-TOS events, overlaid onto the \BsPhiKK simulation sample (black points).}
  \label{fig:BsPhiKK:1DacceptancenotTOS}
\end{figure}

From studies of other modes (\eg Ref.~\cite{LHCb-PAPER-2014-005}), it is known that the shape of the acceptance is different for TIS and TOS trigger categories, as defined in Section~\ref{sec:detector:trig}.
Therefore, the acceptance function coefficients are calculated separately for the TOS and not-TOS samples.
This splitting, rather than TIS and not-TIS, is chosen as it gives a more even distribution of events between the two categories.
As a cross-check, the effect of splitting by TIS and not-TIS events is also investigated in Appendix~\ref{ch:altacc}.

%Near threshold
A condition is imposed that the acceptance function must be zero at exactly $\mass{\Kp\Km} = 2\mass{\Kp}$ and rise smoothly such that the acceptance is described solely by the function given in Equation~\ref{eq:BsPhiKK:angacc} once sufficiently far from threshold.
This is achieved by multiplying the acceptance function by a function of the form
\begin{equation}
  \varepsilon_t\left(m\right) = \mathrm{erf}{[A(m-2\mass{\Kp})]},
  \label{eq:BsPhiKK:thracc}
\end{equation}
where `erf' is the error function, and the parameter $A$ is found from fitting the \BsPhiKK simulation sample.

%Results
The calculated values of the coefficients are summarised in Table~\ref{tab:BsPhiKK:acceptance}.
The 1D projections of the acceptance function are shown in Figure~\ref{fig:BsPhiKK:1DacceptanceTOS} for TOS events and Figure~\ref{fig:BsPhiKK:1DacceptancenotTOS} for not-TOS events.
The 2D projections of the acceptance function can be found in Appendix~\ref{ch:acceptance}.
\FloatBarrier


%TIS
%Calculating acceptance coefficients for combination 2
%Sum over 24809 events
%Divide by phase space
%Keeping coefficients more significant than 4σ
%c[0][0][0][0] = 0.066375 \pm 0.000248 with significance 267.826568σ
%c[0][0][0][2] = -0.002485 \pm 0.000486 with significance 5.115678σ
%c[0][2][0][0] = -0.022017 \pm 0.001028 with significance 21.420337σ
%c[0][4][0][0] = -0.006961 \pm 0.001391 with significance 5.003638σ
%c[1][0][0][0] = -0.014402 \pm 0.001001 with significance 14.385061σ
%c[1][2][0][0] = -0.013987 \pm 0.002142 with significance 6.528877σ

%not TIS
%Calculating acceptance coefficients for combination 1
%Sum over 14335 events
%Divide by phase space
%Keeping coefficients more significant than 4σ
%c[0][0][0][0] = 0.063847 \pm 0.000310 with significance 206.169617σ
%c[0][4][0][0] = -0.014792 \pm 0.001795 with significance 8.242832σ
%c[1][2][0][0] = -0.013254 \pm 0.002644 with significance 5.012474σ


%TOS
%Calculating acceptance coefficients for combination 2
%Sum over 21652 events
%Divide by phase space
%Keeping coefficients more significant than 4σ
%c[0][0][0][0] = 0.064181 \pm 0.000266 with significance 241.637907σ
%c[0][4][0][0] = -0.013521 \pm 0.001481 with significance 9.126582σ
%c[1][2][0][0] = -0.011575 \pm 0.002248 with significance 5.148490σ

%not TOS
%Calculating acceptance coefficients for combination 1
%Sum over 17492 events
%Divide by phase space
%Keeping coefficients more significant than 4σ
%c[0][0][0][0] = 0.067018 \pm 0.000282 with significance 237.244433σ
%c[0][0][0][2] = -0.003454 \pm 0.000571 with significance 6.049689σ
%c[0][2][0][0] = -0.029227 \pm 0.001197 with significance 24.407250σ
%c[1][0][0][0] = -0.017558 \pm 0.001169 with significance 15.017139σ
%c[1][2][0][0] = -0.016372 \pm 0.002486 with significance 6.584298σ


%\subsection{Decay-time acceptance}
%\label{sec:BsPhiKK:timeacc}

%Although the amplitude fit presented in this chapter is time-independent, the angular distribution of the time-integrated decay rate is affected by $\Bs-\Bsb$ mixing, as discussed in Section~\ref{sec:theory:mixing}.
%Furthermore, the decay-time acceptance of the detector also affects the angular distribution.
%The \Bs decay time distribution \BsPhiKK simulation sample is weighted by the expected distribution without acceptance effects: $e^{t/\tau}$, where $t$ is the decay time and $\tau$ is the exact value of the \Bs lifetime used in the simulation (1.512\ps).
%This distribution is fitted by an acceptance function of the form
%\begin{equation}
%  \varepsilon(t) = \left(1 - \frac{1}{1 + [b(t-t_0)]^2}\right)\Theta(t-t_0),
%  \label{eq:BsPhiKK:timeacc}
%\end{equation}
%where $t_0$ is the minimum value of \Bs decay time which can be detected by the \lhcb detector, $b$ is a free parameter that controls the shape of the function, and $\Theta$ is the Heaviside function.
%The result of the fit is shown in Figure~\ref{fig:BsPhiKK:timeacc}.
%The value of $b$ is found to be $1.68 \pm 0.02\invps$, and $t_0$ is found to be $0.236 \pm 0.003\ps$.

%\begin{figure}[h]
%  \centering
%  \includegraphics[width=0.7\textwidth]{body/BsPhiKK/figs/timeacc_power_law}
%  \caption[Time acceptance.]{Time acceptance using the \BsPhiKK simulation sample (black points) modelled by the function defined in Equation~\ref{eq:BsPhiKK:timeacc} (red dotted line). The black dotted line indicates where the acceptance function reaches 50\,\% of its maximum value.}
%  \label{fig:BsPhiKK:timeacc}
%\end{figure}

%\def\tlow{t_\frac12}

%The function in Equation~\ref{eq:BsPhiKK:timeacc} would ideally be used to weight the integrand of the time integral, given in Equation~\ref{eq:theory:integrateddecayrate}.
%However, since it has no analytical integral, the time acceptance function is approximated by the Heaviside function $\Theta(t-\tlow)$, where $\varepsilon(\tlow) = \frac12$, which is equivalent to changing the lower bound of the time integral from zero (as in Equation~\ref{eq:theory:integrateddecayrate}) to $\tlow$.
%The time integral then becomes
%%\frac{1}{2}\left(\left|A(0)\right|^2+\left|\overline{A}(0)\right|^2\right)\left(\frac{1}{\Gamma_L}+\frac{1}{\Gamma_H}\right)\\
%%    &+&\Re\left(\overline{A}(0)A(0)^*\right) \left(\frac{1}{\Gamma_L}-\frac{1}{\Gamma_H}\right).

%\def\termone{\left|A(0)\right|^2+\left|\overline{A}(0)\right|^2}
%\def\termtwo{2\Re\left(\overline{A}(0)A(0)^*\right)}
%\newcommand\Gfactor[1]{\Gamma_#1 e^{\tlow \Gamma_#1}}
%\begin{equation}
%  \begin{array}{rcl}
%    \int^{\infty}_{\tlow} \Gamma(t) + \overline{\Gamma}(t) dt &=& C
%    \Big( \Gfactor{H} \left[ \termone + \termtwo \right] \\
%    &+& \Gfactor{L} \left[ \termone - \termtwo \right]\Big),
%  \end{array}
%\end{equation}
%where $C$ is a constant given by
%$$C = \frac{e^{-\tlow(\Gamma_H+\Gamma_L)}}{2\Gamma_H\Gamma_L},$$
%and the rest of the symbols take the same meanings as in Section~\ref{sec:theory:mixing}.

%double Bs2PhiKKSignal::TimeIntegratedMsq(const Bs2PhiKK::amplitude_t& Amp) const
%{
%	double termone = (std::norm(Amp[true]) + std::norm(Amp[false]));
%	double termtwo = 2*std::real(Amp[true] * std::conj(Amp[false]));
%	double factor0 = std::exp(-tlow.value * (GH.value + GL.value)) / (2 * GH.value * GL.value);
%	double factorH = GH.value * std::exp(tlow.value * GH.value);
%	double factorL = GL.value * std::exp(tlow.value * GL.value);
%	return factor0 * (factorH * (termone + termtwo) + factorL * (termone - termtwo));
%}


\FloatBarrier

