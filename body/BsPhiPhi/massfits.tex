\section{Mass fits for the \BsPhiPhi branching fraction}
\label{sec:BsPhiPhi:massfits}

The numbers of candidates in the signal and normalisation modes are found by performing unbinned maximum likelihood fits to the \PhiPhi and \PhiKstar invariant mass distributions in the fully selected data samples.
In both cases, fits are first performed to the simulation samples to obtain the values of the parameters that describe the shapes of the \Bz and \Bs peaks.
In the fits to simulation, a flat component is added to account for poorly reconstructed candidates and background candidates that have been misidentified as signal.
The ratios of the widths of the components in the \Bz and \Bs peaks are fixed to the values found from the fits to simulation.
In order to account for any difference in mass resolution between the simulation and data, the values obtained in simulation are multiplied by an overall common scaling factor, $R_s$, which is left free in the fit to data.
This should be close to one, due to the momentum resolution smearing described in Section~\ref{sec:analysis:dataset}.
The mean values of the peaks, which correspond to the pole masses of the \Bz and \Bs mesons, are also left free in the data fit.

\subsection{The \PhiPhi invariant mass fit}
\label{sec:BsPhiPhi:phiphimassfit}

% phi phi model
The probability density function (PDF) used in the fit to the \PhiPhi invariant mass distribution has two components, which account for \BsPhiPhi and combinatorial background.
% signal shape
The \BsPhiPhi signal peak is modelled by the sum of three Gaussian functions with a shared mean,

\begin{equation}
  \begin{aligned}
    P(x|\mu,\sigma_\text{1},\sigma_\text{2},\sigma_\text{3},f_\text{1},f_\text{2})
    &= f_\text{1} P_\text{Gauss}(x|\mu,\sigma_\text{1})
    + f_\text{2} P_\text{Gauss}(x|\mu,\sigma_\text{2})\\
    &+ (1-f_\text{1}-f_\text{2}) P_\text{Gauss}(x|\mu,\sigma_\text{3}),
  \end{aligned}
\end{equation}

where $P_\text{Gauss}$ is a Gaussian function\footnote{See Appendix~\ref{sec:misc:gaussian} for the explicit form of this function.}, $\mu$ corresponds to the \Bs mass, $\sigma_{(1,2,3)}$ are the widths of the three Gaussian components in order of increasing width, and $f_{(1,2)}$ are the relative fractions of the two narrower components.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.65\textwidth]{body/BsPhiPhi/figs/PhiPhi_MC_remade.pdf}
  \caption[Fit to the \PhiPhi invariant mass distribution in the \BsPhiPhi simulation.]
  {
    A fit to the \PhiPhi invariant mass distribution in the \BsPhiPhi simulation sample.
    The total fitted PDF is shown as a red solid line.
  }
  \label{fig:BsPhiPhi:phiphiMC}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=0.65\textwidth]{body/BsPhiPhi/figs/Fig1_remade.pdf}
  \caption[Fit to the \PhiPhi invariant mass distribution in the data.]
  {
    A fit to the \PhiPhi invariant mass distribution in the data.
    The total fitted PDF as described in the text is shown by the (red) solid line,
    the \BsPhiPhi component by the (blue) long-dashed line,
    and the combinatorial background as the (purple) dotted line.
  }
  \label{fig:BsPhiPhi:phiphimass}
\end{figure}

\begin{table}[h]
  \centering
  \caption{Results of the fits to the \BsPhiPhi simulation and data samples.}
  \begin{tabular}{|c|r@{ $\pm$ }l|}
    \hline
    Simulation fit parameter & \multicolumn{2}{c|}{Result}\\
    \hline
    $\sigma_1$ & $ 12.9 $&$ 0.1 $\mevcc \\
    $\sigma_2$ & $ 21.4 $&$ 0.4 $\mevcc \\
    $\sigma_3$ & $ 73.4 $&$ 5.2 $\mevcc \\
    $f_1$      & $(71.4 $&$ 1.2)$\,\%   \\
    $f_2$      & $(27.0 $&$ 1.2)$\,\%   \\
    \hline
    Data fit parameter & \multicolumn{2}{c|}{Result}\\
    \hline
    $\mu$ & $5366.4 $&$ 0.3 $\mevcc \\
    $R_s$ & $  1.02 $&$ 0.02$       \\
    $N_S$ & $  2309 $&$ 49  $       \\
    $N_B$ & $   113 $&$ 16  $       \\
    \hline
  \end{tabular}
  \label{tab:BsPhiPhi:phiphifitres}
\end{table}

Figure~\ref{fig:BsPhiPhi:phiphiMC} shows the fit to the simulation.
The corresponding results are shown in the top half of Table~\ref{tab:BsPhiPhi:phiphifitres}.
% combinatorial background shape
The combinatorial background is well modelled by a uniform distribution.
Alternative models, namely a linear function and an exponential curve, were tried and found to give consistent results.
These were not used in order to remove an unnecessary degree of freedom from the fit.
% (lack of) peaking background
In the angular analysis of \BsPhiPhi,~\cite{LHCb-PAPER-2014-026}, misidentified \BdPhiKstar and \LbPhipK backgrounds were also considered.
%However, in this analysis, since \BdPhiKstar is the normalisation mode, there is a specific veto applied to remove this as a background.
When reconstructing the data, selected for \BsPhiPhi, under the $\phiz \Kstarz$ mass hypothesis, no peak is found at the \Bz mass, and the contribution from this background is therefore assumed to be negligible.
This is to be expected, since, as mentioned in Section~\ref{sec:BsPhiPhi:cuts}, events which would pass the \BdPhiKstar selection are rejected from the \BsPhiPhi sample.
When reconstructing the data under the $\phiz \proton \Km$ mass hypothesis, no peak was found at the \Lb mass.
Therefore, the contribution from \LbPhipK is considered to be negligible.
The absence of this background is due to the tighter selection in this analysis compared to the angular analysis.

% collision data fit and results
The \Bs mass, $\mu$, and the yields, $N_{\{S,B\}}$, of the signal and background components are left free in the fit to data.
Figure~\ref{fig:BsPhiPhi:phiphimass} shows the fitted PDF overlaid onto the data.
The results of the fit are given in the bottom half of Table~\ref{tab:BsPhiPhi:phiphifitres}.
%When binning the data in 100 bins, the probability of the $\chi^2$ of the fit is found to be $1-7\times 10^{-6}$.
The fitted value of $\mu$ is consistent with the world average value of $\mass{\Bs}=(5366.82\pm0.22)\mevcc$~\cite{PDG2016}.
The fitted value of $R_s$ is consistent with unity.

\subsection{The \PhiKstar invariant mass fit}
\label{sec:BsPhiPhi:phikstarmassfit}

% phi K* model
The PDF used in the fit to the $\phiz\Kstar$ invariant mass distribution contains four components, which account for \BdPhiKstar, \BsPhiKstar, misidentified \LbPhiph decays and combinatorial background.
% signal shape
The \BdPhiKstar peak is modelled by the sum of a Crystal Ball \cite{Skwarnicki:1986xj}\footnote{See Appendix~\ref{sec:misc:crystalball} for the explicit form of this function.} and two Gaussian functions with a shared mean

\begin{equation}
  \begin{aligned}
    P(x|\mu,\sigma_\text{CB},\sigma_\text{1},\sigma_\text{2},f_\text{CB},f_\text{1},\alpha,n)
    &= f_\text{CB} P_\text{CB}(x|\mu,\sigma_\text{CB},\alpha,n)
    + f_\text{1} P_\text{Gauss}(x|\mu,\sigma_\text{1})\\
    &+ (1-f_\text{CB}-f_\text{1}) P_\text{Gauss}(x|\mu,\sigma_\text{2}),
  \end{aligned}
\end{equation}

where the symbols for the Gaussian components have the same meaning as in Section~\ref{sec:BsPhiPhi:phiphimassfit}.
The parameters denoted with the subscript CB are associated with the Crystal Ball function.

\begin{figure}[h]
\centering
\includegraphics[width=0.65\textwidth]{body/BsPhiPhi/figs/PhiKst_MC_remade.pdf}
\caption[Fit to the \PhiKstar invariant mass distribution in the \BdPhiKstar simulation.]
  {
    A fit to the \PhiKstar invariant mass distribution in the \BdPhiKstar simulation sample.
    The total fitted PDF is shown as a red solid line.
    The signal PDF is shown as a blue dashed line.
    A flat component is added to account for poorly reconstructed and misidentified candidates, but is below the scale of the plot.
  }
\label{fig:BsPhiPhi:phikstMC}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=0.65\textwidth]{body/BsPhiPhi/figs/Fig2_remade.pdf}
  \caption[Fit to the \PhiKstar invariant mass distribution in the data.]
  {
    A fit to the \PhiKstar invariant mass distribution in the data.
    The total fitted PDF is shown by the (red) solid line,
    the \BdPhiKstar component by the (blue) short-dashed line,
    the \BsPhiKstar component by the (blue) long-dashed line,
    the \LbPhiph contribution by the (green) dashed-dotted line,
    and the combinatorial background by the (purple) dotted line.
  }
  \label{fig:BsPhiPhi:phikstmass}
\end{figure}

\begin{table}[h]
  \centering
  \caption{Results of the fits to \BdPhiKstar simulation and data samples.}
  \begin{tabular}{|c|r@{ $\pm$ }l|}
    \hline
    Simulation fit parameter & \multicolumn{2}{c|}{Result}\\
    \hline
    $\sigma_\text{CB}$      & $ 13.7 $&$ 0.1 $\mevcc \\
    $\sigma_\text{1}$       & $ 22.1 $&$ 0.4 $\mevcc \\
    $\sigma_\text{2}$       & $ 54.6 $&$ 3.7 $\mevcc \\
    $f_\text{CB}$           & $(72.9 $&$ 1.3)$\,\%   \\
    $f_\text{1}$            & $(25.3 $&$ 1.3)$\,\%   \\
    $\alpha$                & $  3.3 $&$ 0.1 $       \\
    \hline
    Data fit parameter & \multicolumn{2}{c|}{Result}\\
    \hline
    $\mu$     & $5278.8 $&$ 0.2 $\mevcc \\
    $R_s$     & $  1.06 $&$ 0.01$       \\
    $N_S$     & $  6680 $&$ 86  $       \\
    $N_B$     & $   842 $&$ 72  $       \\
    $N_{\Bs}$ & $   122 $&$ 20  $       \\
    $N_{\Lb}$ & $   243 $&$ 62  $       \\
    \hline
  \end{tabular}
  \label{tab:BsPhiPhi:phikstfitres}
\end{table}

The tail of the Crystal Ball distribution accounts for radiative decays.
The widths, $\sigma_{(\text{CB},1,2)}$, and relative fractions, $f_{(\text{CB},1)}$, of these components, as well as the Crystal Ball tail position parameter, $\alpha$, are found from a fit to the fully selected \BdPhiKstar simulation sample, shown in Figure~\ref{fig:BsPhiPhi:phikstMC} and the top half of Table~\ref{tab:BsPhiPhi:phikstfitres}.
The Crystal Ball tail shape parameter, $n$, is fixed to 1.

% peaking backgrounds
The \BsPhiKstar component shares the same shape parameters as the \BdPhiKstar component.
The difference between the masses of the two is fixed to the PDG average value of $87.04\pm0.21\mevcc$ \cite{PDG2016}.
The background component from misidentified \LbPhiph decays is modelled using a histogram PDF generated from a sample of generator-level phase-space simulation events. % TODO: find out fraction of phi p pi and phi p K
The mass of the \Bz component, $\mu$, and the yields, $N_{\{S,B\}}$, of the signal and background components are left free in the fit to data.

Figure~\ref{fig:BsPhiPhi:phikstmass} shows the fitted PDF overlaid onto the data.
The series of successive bins with negative pulls around 5500\mevcc is likely due to modelling of the \LbPhiph background as non-resonant decays.
The results of the fit are given in the bottom half of Table~\ref{tab:BsPhiPhi:phikstfitres}.
Binning the data in 100 bins, the probability of the $\chi^2$ of the fit is found to be $0.57$.
The fitted value of $\mu$ deviates from the world average value of $\mass{\Bd}=5279.58\pm0.17\mevcc$ by 3.9 times the statistical uncertainty.
The fitted value of $R_s$ is not consistent with unity to within the statistical uncertainty.
However, for both of these quantities, the systematic uncertainty arising from the detector calibration is similar in size to the statistical uncertainty.
The ratio of yields of the \Bs and \Bd components is $0.018\pm0.003$, which agrees with the value found in Ref.~\cite{LHCb-PAPER-2013-012} at the $2\sigma$ level.

\subsection{S-wave subtraction}
\label{sec:BsPhiPhi:swave}

%Introduction
The numbers of \BsPhiPhi and \BdPhiKstar candidates found in the fits include some fraction of $\Kp\Km$ or $\Kp\pim$ pairs which do not come from \phiz or \Kstar decays.
Instead, they originate from the tails of spin-0 resonances, such the \fzero{980} or $K^*_0(1430)^0$, or are produced in a nonresonant configuration.
Collectively, the contribution to the yield from these sources is referred to as the S-wave fraction.
The contribution from higher-spin resonances is negligible due to the applied $\Kp\Km$ and $\Kp\pim$ mass windows.
As mentioned in Section~\ref{sec:BsPhiPhi:cuts}, the mass windows for the \phiz and \Kstar candidates are chosen so that the S-wave fraction can be taken from \lhcb analyses of \BsPhiPhi~\cite{LHCb-PAPER-2014-026} and \BdPhiKstar~\cite{LHCb-PAPER-2014-005}.
The uncertainties on these S-wave fractions are considered as sources of systematic uncertainty on the final branching fraction result.

%Signal s-wave
The \BsPhiPhi signal yield is corrected using the S-wave fraction of $(2.12 \pm 1.57)\,\%$ in a window of $|\mass{\Kp\Km}-\mass{\phiz}| < 15\mevcc$, reported in Ref.~\cite{LHCb-PAPER-2014-026}.
The S-wave-subtracted yield is $2212 \pm 47 \text{\,(stat)} \pm 50 \text{\,(S-wave)}$ events.

%Normalisation s-wave
The \BdPhiKstar signal yield is corrected using the S-wave fraction found in the angular analysis presented in Ref.~\cite{LHCb-PAPER-2014-005}.
The $\Kp\Km$ S-wave fraction in the window $|\mass{\Kp\Km}-\mass{\phiz}| < 15\mevcc$ is $(12.2 \pm 1.3)\,\%$, and the $\Kp\pim$ S-wave fraction in the window $|\mass{\Kp\pim} - \mass{\Kstar}| < 150\mevcc$ is $(14.3 \pm 1.3)\,\%$.
Hence, the S-wave-subtracted yield is $5026 \pm 65 \text{\,(stat)} \pm 107 \text{\,(S-wave)}$ events.

%Assumptions
This approach to S-wave subtraction assumes that the angular acceptance for the S-wave and P-wave contributions is the same.
The validity of this assumption is discussed in Section~\ref{sec:BsPhiPhi:angaccsystematic}.

%\clearpage

