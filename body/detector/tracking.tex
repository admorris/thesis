\section{Tracking}
\label{sec:detector:tracking}

The tracking system is described in this section.
Its purpose is to reconstruct the trajectories of charged particles (tracks) using the positions of where charged particles have interacted with the sensors of the detectors (hits).
The tracking system consists of a vertex locator, a dipole bending magnet and four tracking stations: one located before the magnet and three after.
The momentum of a particle is deduced from the angle of deflection through the magnetic field.

\subsection{Magnet}
\label{sec:detector:magnet}

%magnet requirements
In order to measure the momentum of charged particles to high precision, the tracking system needs a well-known magnetic field to bend their path.
%Due to the forward geometry of the detector,
A dipole magnet is suited to this task.
In order to achieve a momentum resolution of $\delta p/p = 4\times 10^{-3}$ for 10\gevc particles, an integrated field of {4\,Tm} is required between the \velo and the tracking stations.
Since the Hybrid Photon Detectors in the \rich detectors (see Section~\ref{sec:detector:rich}) are sensitive to magnetic fields, the field strength is required to be small in the region of \richone.

\begin{figure}
  \centering
  \includegraphics[width=0.65\textwidth]{body/detector/figs/magnet}
  \caption[The \lhcb dipole magnet.]{A diagram of the \lhcb dipole magnet \cite{LHCb-TDR-001}. The positive $z$ direction is out of the page.}
  \label{fig:detector:magnet}
\end{figure}

%description
A warm\footnote{\ie not superconducting.} dipole magnet, illustrated in Figure~\ref{fig:detector:magnet}, is used to provide a magnetic field for the tracking system~\cite{LHCb-TDR-001}.
The magnetic field as a function of position in $z$ is shown in Figure~\ref{fig:detector:magnet_field}.
The magnet is composed of two saddle-shaped coils attached to a rectangular yoke with slanted poles, with a wedge-shaped window through the centre that matches the acceptance of the detector.
The yoke is made of twenty-seven layers of laminated low-carbon steel, each 100\mm thick and with a maximum mass of 25~tonnes.
Each coil is made of fifteen layers of hollow aluminium conductor with a central channel for water-cooling.
The nominal current passing through the coils is {5.85\,kA}.

\begin{figure}
  \centering
  \includegraphics[width=0.5\textwidth]{body/detector/figs/magnet_field}
  \caption[Magnetic field as a function of $z$.]{Magnetic field as a function of $z$, at $x=0\cm$ and $y=0\cm$ for both polarities \cite{LHCb-TDR-001}.}
  \label{fig:detector:magnet_field}
\end{figure}

%field measurement
To achieve the required momentum resolution, the magnetic field was measured to a relative precision of $\order(10^{-4})$ prior to data taking.
In order to eliminate artificial \CP asymmetries induced by the detector, the polarity of the magnet is reversed regularly during the operational year, so that the collected data is split evenly between the two polarities.

\subsection{Vertex locator}
\label{sec:detector:velo}

%\begin{figure}[htb]
%  \centering
%  \includegraphics[width=\textwidth]{body/detector/figs/VELO_layout}
%  \caption{Top: the positions of sensors in the \velo relative to the crossing point.
%           Bottom: relative positions of the sensors in `open' and `closed' positions.}
%  \label{fig:detector:velo_layout}
%\end{figure}

%TODO why have a VELO?

% Describe the VELO
The Vertex Locator (\velo)~\cite{LHCb-TDR-005} is a movable silicon strip detector operated in a secondary vacuum, separated from the LHC primary vacuum by an RF-foil.
It is used to make precise measurements of particle trajectories close to the interaction point.
This allows for the accurate determination of the locations of proton--proton collisions (primary vertices) and heavy quark hadron decays (secondary vertices).
The detector consists of 42 modules positioned in the plane perpendicular to the beam axis, $z$.
The \velo is split vertically into two halves, each with an equal number of modules either side of the beam.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.53\textwidth]{body/detector/figs/VELO_foil_RB}
  \includegraphics[width=0.46\textwidth]{body/detector/figs/VELO_sensors}
  \caption[The \velo sensors.]
          {Left: a view of the \velo sensors and RF foil in the `closed' position.
          Right: the $r$ and $\phi$ sensors, illustrating the size and orientation of the silicon microstrips. Note that the diagram shows two $\phi$ sensors overlaid. \cite{LHCb-TDR-005}
          }
  \label{fig:detector:velo_sensor}
\end{figure}

Each half of the \velo is enclosed in a box of thin aluminium.
The inner faces of the boxes (the RF-foil) are corrugated to allow for the modules to overlap when in the closed position, as shown on the left of Figure~\ref{fig:detector:velo_sensor}.
Each module has two semi-circular silicon strip sensors, illustrated on the right of Figure~\ref{fig:detector:velo_sensor}, which each provide measurements of radius, $r$, or azimuthal angle, $\phi$.
The $r$ sensor strips are concentric rings with a variable pitch that increases linearly from 38\mum at the inner edge to 102\mum at the outer edge.
The $\phi$ sensor strips are wedge-shaped and divided into two regions at $r=17.25\mm$ in order to reduce occupancy and avoid large strip pitches at the outer edge of the sensors.
The strips in the inner region have a pitch of 38\mum at the inner edge and 78\mum at the outer edge, while the strips in the outer region have a pitch of 39\mum at the inner edge and 97\mum at the outer edge.
To reduce ambiguities in the pattern recognition, the $\phi$ sensor strips are skewed 20\degrees from the radial direction in the inner region and 10\degrees in the outer region.

The modules have an inner radius of 8.2\mm, which is smaller than required by the beam during injection.
To avoid damage to the sensors, the two halves of the \velo are retractable to a distance of 30\mm from the centre of the beam.
The \velo is kept open during conditions when the \lhc beam is diffuse.
It is moved to the closed position when stable beam conditions are declared.

\subsection{Vertexing performance}
\label{sec:detector:veloperf}

%\begin{figure}[htb]
%  \centering
%  \includegraphics[width=0.6\textwidth]{body/detector/figs/VELO_per_osc}
%  \caption{.}
%  \label{fig:detector:VELO_per_osc}
%\end{figure}

%\begin{figure}[htb]
%  \centering
%  \includegraphics[width=0.49\textwidth]{body/detector/figs/VELO_per_decaytime_res_left}
%  \includegraphics[width=0.49\textwidth]{body/detector/figs/VELO_per_decaytime_res_right}
%  \caption{Decay time resolution as a function of momentum (left) and estimated decay time uncertainty (right) of fake prompt \decay{\Bs}{(\decay{\jpsi}{\mup\mun})(\decay{\phiz}{\Kp\Km})} candidates in 2011 and 2012 data. The superimposed histograms show the distributions of the $x$-axis variable.}
%  \label{fig:detector:VELO_per_decaytime}
%\end{figure}

The primary vertex resolution is measured in data by comparing two independent measurements of the position of the same vertex in the same event~\cite{LHCb-DP-2014-002}.
This is done by randomly dividing the tracks into two sets and reconstructing the primary vertex using each, then taking the difference in position between the two resulting vertices.
The width of the distribution of the difference between the vertex positions is $\sqrt{2}$ times the primary vertex resolution.
This resolution strongly depends on the number of tracks used to reconstruct the vertex, as shown on the left of Figure~\ref{fig:detector:VELO_per_res}.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.49\textwidth]{body/detector/figs/VELO_per_PV_res}
  \includegraphics[width=0.49\textwidth]{body/detector/figs/VELO_per_IP_res}
  \caption[\velo performance plots.]
          {Left: primary vertex resolution as a function of track multiplicity.
           % The superimposed histogram is the distribution of number of tracks per reconstructed primary vertex.
           Right: resolution of impact parameter in the $x$-direction as a function of the inverse of transverse momentum. \cite{LHCb-DP-2014-002}}
  \label{fig:detector:VELO_per_res}
\end{figure}

The impact parameter (IP) is the distance of closest approach of a particle's trajectory to the primary vertex.
%Decay products of long-lived heavy-quark hadrons tend to have larger impact parameters than particles produced at the primary vertex.
The resolution on this quantity is caused by multiple scattering, spatial resolution of the \velo sensor hits, and the distance of extrapolation from the interaction point to its first hit in a sensor.
It is customary to quote the resolution of the impact parameter projected onto two orthogonal axes, $\mathrm{IP}_x$ and $\mathrm{IP}_y$, which follow Gaussian distributions, unlike the $\mathrm{IP}$ resolution itself.
%TODO how is impact parameter resolution measured?
Figure~\ref{fig:detector:VELO_per_res} (right) shows the $\mathrm{IP}_x$ resolution as a function of the inverse of the transverse momentum.
This dependence is due to multiple scattering in the detector material and the spatial resolution of the \velo.
At high transverse momentum, typical for \PB decays, the $\mathrm{IP}_x$ resolution is about 13\mum.

\subsection{Tracker Turicensis}
\label{sec:detector:tt}

% TT
The Tracker Turicensis (\ttracker)~\cite{LHCb-TDR-009} is located immediately before the dipole magnet.
It is of particular importance for reconstructing tracks that originate outside of the \velo, such as those that come from \KS and \Lz decays.
The \ttracker consists of four layers of silicon microstrip sensors with a pitch of 183\mum between the strips.
The first and last layers are oriented vertically, and the second and third are rotated by $-5\degrees$ and $+5\degrees$ from the vertical.
The sensors are housed in a light-tight, electrically insulated and thermally insulated box.
The temperature inside the box is maintained below 5\degc, and nitrogen gas is continuously flushed through the box to prevent condensation.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.49\textwidth]{body/detector/figs/TT_x1_layer}
  \includegraphics[width=0.49\textwidth]{body/detector/figs/TT_u_layer}
  \caption[The first two layers of the \ttracker.]{The first two layers of the \ttracker, illustrating the vertical and 5\degrees orientations of the modules \cite{LHCb-TDR-009}.}
  \label{fig:detector:tt}
\end{figure}

Each layer of the \ttracker is composed of modules, which are columns of 14 silicon sensors.
Two half modules occupy the centre of each layer, above and below the beam pipe.
The first two layers are shown in Figure~\ref{fig:detector:tt}.
The sensors are read out in `sectors' of varying sizes (1 to 4) depending on their typical occupancy.
Alternate adjacent modules are staggered in $z$ and overlapped by a few millimetres to avoid gaps in the acceptance.
The readout boards, structural supports and cooling system are located at the ends of the modules, outside the acceptance of the detector.

\subsection{Inner Tracker}
\label{sec:detector:it}

% IT
The Inner Tracker (\intr)~\cite{LHCb-TDR-008} covers the high-occupancy inner part of the three tracking (T) stations after the magnet.
Each station has four boxes arranged around the beam pipe as shown in Figure~\ref{fig:detector:it}.
As with the \ttracker, each of the \intr stations consists of four layers of silicon microstrip sensors oriented $0\degrees$, $-5\degrees$, $+5\degrees$ and $0\degrees$ from the vertical.
The pitch of the strips is 196\mum.
The sensors are housed in light-tight boxes, four for each station, surrounding the beam pipe.
As with the \ttracker, the boxes are maintained below 5\degc, and nitrogen gas is continuously flushed through to prevent condensation.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.6\textwidth]{body/detector/figs/IT_layer}
  \caption{Positions of the \intr boxes around the beam pipe.}
  \label{fig:detector:it}
\end{figure}

The boxes either side of the beam pipe contain modules of two sensors each.
The ones above and below the beam pipe contain single-sensor modules.
The boxes are offset in $z$ and overlap to avoid gaps in the acceptance.
Unlike the \ttracker, the readout boards, cooling system and structural supports are inside the acceptance of the detector.

\subsection{Outer tracker}
\label{sec:detector:ot}

%Description of the detector
The Outer Tracker (\ot)~\cite{LHCb-TDR-006} is placed surrounding the \intr and consists of four modules per T station.
Each module consists of two staggered layers of straw tubes covering the rest of the acceptance not taken up by the \intr.
The modules have the same orientation as the strips in each layer of the \intr and \ttracker: the first and last are vertical, and the second and third are rotated by $-5\degrees$ and $+5\degrees$, respectively, from the vertical.
The position and orientation of the straw tubes and modules are shown in Figure~\ref{fig:detector:ot}.
The straw chambers are gas-tight conducting tubes with an internal diameter of 4.9\cm, filled with a mixture of $70\,\%$ argon to $30\,\%$ carbon dioxide.
The tubes are formed from two layers of thin foil.
The outer layer is a laminate of polyimide and aluminium, which provides gas-tightness and shielding.
The inner layer is carbon-doped polyimide and acts as the cathode.
A gold-plated tungsten anode wire runs through the middle of each tube.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.6\textwidth]{body/detector/figs/OT_2}
  \includegraphics[width=0.7\textwidth]{body/detector/figs/OT_1}
  \caption[The outer tracker.]{Top: a view of the \ot stations, with part of T2 retracted, showing the orientations of the straw tube modules.
           Note the cross-shaped gap where the \intr is located.
           Bottom: the packing of the straw tubes within one layer of the \ot. \cite{LHCb-TDR-006}}
  \label{fig:detector:ot}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=0.5\textwidth]{body/detector/figs/OT_drift_time}
  \caption[Outer tracker drift times.]{Drift time distribution for the inner-most \ot modules. \cite{LHCb-TDR-006}}
  \label{fig:detector:ot_drift}
\end{figure}

%Performance
The maximum drift time within the straw tubes is around 35\ns, which is lower than the bunch crossing frequency during Run 1.
However, to account for variations in time-of-flight, signal propagation and the readout electronics, a 75\ns window is read out.
Figure~\ref{fig:detector:ot_drift} shows the measured drift time distribution for three different bunch spacings.
The \lhc operated mostly with 50\ns and 75\ns bunch spacing during Run 1, with a short period in 2012 at 25\ns.

\subsection{Track reconstruction}
\label{sec:detector:trackreco}

Tracks are reconstructed from hits in the \velo, \ttracker and T detectors.
Tracks at \lhcb are classified by which subdetectors they pass through, as illustrated in Figure~\ref{fig:detector:tracktypes}.
The analyses presented in this thesis use only `long' tracks, defined as those which traverse all tracking stations.
The reconstruction of long tracks starts with a search for straight line trajectories among \velo hits.
These `\velo tracks' must have hits in at least three $r$ and three $\phi$ sensors.
Subsequently, two complementary algorithms add information from the \ttracker and T stations, named the `forward tracking' and `track matching' algorithms \cite{LHCb-DP-2014-002}.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.7\textwidth]{body/detector/figs/tracktypes.png}
  \caption[Track types at \lhcb.]{Track types at \lhcb, which are defined by the subdetectors they pass through. \velo tracks and T tracks may be combined to form long tracks.}
  \label{fig:detector:tracktypes}
\end{figure}

The forward tracking algorithm starts with a \velo track and combines it with a single hit in a T station to determine the track trajectory.
Additional T station hits are searched for in a window around this trajectory.
If the found hits satisfy certain quality cuts, the track is kept.

The track matching algorithm starts with a set of \velo tracks and a set of track segments reconstructed in the T stations.
The algorithm attempts to match tracks from the two sets by extrapolating tracks from both sets through the magnet.
Quantities such as position, gradient and number of hits are used to evaluate whether or not the \velo and T tracks are compatible.
The candidate tracks from both algorithms are combined, duplicates are removed, and any consistent hits in the \ttracker are added to improve the momentum measurement.

The final step of track reconstruction is to fit the tracks with a Kalman-filter \cite{Hulsbergen:2005pu}.
This takes into account multiple scattering and corrects for energy loss due to ionisation.
Track quality is determined from the \chisq per degree of freedom of the fit.

`Ghost' tracks are those which are reconstructed from combinations of hits that do not correspond to the trajectory of a charged particle \cite{LHCb-DP-2014-002}.
Most of these result from incorrect matching of \velo and T tracks.
The fraction of ghost tracks in minimum bias events ranges from 6.5\,\% to 20\,\% depending on the charged particle multiplicity.
A neural network classifier is used to identify and remove ghost tracks.
This uses the result of the track fit, track kinematics and the number of measured and expected hits in the tracking stations.
%TODO how well does this perform?

\subsection{Tracking performance}
\label{sec:detector:trackperf}

Tracking efficiency is measured using a `tag-and-probe' method with \decay{\jpsi}{\mup\mun} decays \cite{LHCb-DP-2014-002}.
One of the daughter muons (the `tag') is fully reconstructed, whereas the other (the `probe') is only partially reconstructed.
The $\mup\mun$ pairs must pass a \jpsi invariant mass cut.
The tracking efficiency is calculated as the proportion of probe muons which can be matched successfully to fully reconstructed long tracks.
Figure~\ref{fig:detector:track_eff} shows the tracking efficiency in 2011 and 2012 data as a function of momentum, pseudorapidity, track multiplicity and number of primary vertices.
The average efficiency is above 96\,\% in the momentum range $5<p<200\gevc$.
It is worst in high multiplicity events with $N_\mathrm{track}>200$. % TODO what fraction of events is this?

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.49\textwidth]{body/detector/figs/TrackEff_topleft}
  \includegraphics[width=0.49\textwidth]{body/detector/figs/TrackEff_topright}\\
  \includegraphics[width=0.49\textwidth]{body/detector/figs/TrackEff_bottomleft}
  \includegraphics[width=0.49\textwidth]{body/detector/figs/TrackEff_bottomright}
  \caption[Tracking efficiency.]{Tracking efficiency as a function of momentum (top left), pseudorapidity (top right), multiplicity (bottom left) and number of primary vertices (bottom right) in 2011 and 2012 data \cite{LHCb-DP-2014-002}.}
  \label{fig:detector:track_eff}
\end{figure}

The momentum resolution for long tracks is studied using \decay{\jpsi}{\mup\mun} decays \cite{LHCb-DP-2014-002}.
The mass resolution of the \jpsi meson is dominated by the momentum resolution of the muons.
Ignoring the muon mass and considering the case where both muons have similar momentum, the momentum resolution ($\delta p/p$) in this case is $$\left(\frac{\delta p}{p}\right)^2 = 2\left(\frac{\sigma_m}{m}\right)^2 - 2\left(\frac{p\sigma_\theta}{mc\theta}\right)^2,$$ where $m$ is invariant mass, $\sigma_m$ is the Gaussian width obtained from a fit to the invariant mass, $\theta$ is the opening angle between the muons, and $\sigma_\theta$ is the error on $\theta$, obtained from track fits of the muons.
The plot on the left of Figure~\ref{fig:detector:track_res} shows relative momentum resolution as a function of momentum.
The resolution ranges from 0.5\,\% for tracks below 20\gevc to 1.1\,\% for tracks above 160\gevc.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.49\textwidth]{body/detector/figs/TrackRes}
  \includegraphics[width=0.49\textwidth]{body/detector/figs/TrackMassRes}
  \caption[Momentum and mass resolution.]{Left: relative momentum resolution of muon tracks from \decay{\jpsi}{\mup\mun} decays as a function of track momentum.
           Right: relative mass resolution from several $\mup\mun$ resonances as a function of invariant mass.
           The solid curve is an empirical power-law fit through the points \cite{LHCb-DP-2014-002}.}
  \label{fig:detector:track_res}
\end{figure}

Fits are performed to the invariant mass distributions of six $\mup\mun$ resonances --- the \jpsi, \psitwos, \Y1S, \Y2S, \Y3S and $\PZ^0$ --- in order to obtain the relative mass resolution \cite{LHCb-DP-2014-002}.
The results are shown on the right of Figure~\ref{fig:detector:track_res}.
It can be seen that the relative mass resolution is about 0.5\,\% up to \order(10\gevcc).
