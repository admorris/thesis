\section{Event selection}
\label{sec:BsPhiKK:selection}

This section describes the selection used in the analysis.
The goal is to remove combinatorial background and candidates with misidentified hadrons without sacrificing signal efficiency or introducing bias in the observables used in the amplitude fit.
A more general overview of the event selection strategy is given in Chapter~\ref{ch:analysis}.

\subsection{Dataset}

This analysis uses the full Run 1 dataset, described in Section~\ref{sec:analysis:dataset}.
Simulated \BsPhiKK events, generated using phase space, are used to train multivariate algorithms (Section~\ref{sec:BsPhiKK:mva}) and model the acceptance function (Section~\ref{sec:BsphiKK:acceptance}).
Simulated \BsPhiPhi events are used to verify that the amplitude fit reproduces the input values for the helicity amplitudes.

Due to the wider invariant mass window on one of the $\Kp\Km$ pairs, compared to the \BsPhiPhi analysis in Chapter~\ref{ch:BsPhiPhi}, it is necessary to consider more potential peaking backgrounds in the data sample.
For this reason, samples of simulated \BdPhiKstar (with \decay{\Kstarz}{\Kp\pim}) and \LbPhipK events are produced, with the latter generated according to phase space.

\subsection{Trigger lines}

At the Level 0 trigger stage, candidates are required to pass either the \lone Hadron TOS or \lone Global TIS lines (see Section~\ref{sec:detector:trig}).
At the HLT1 stage, candidates are required to pass the `single-track all \lone' TOS line, which searches for a single detached high-momentum track without any confirmation of a \lone trigger decision~\cite{LHCb-PUB-2011-003}.
Events are subsequently required to pass either of the topological 3- or 4-body B decay TOS lines~\cite{LHCb-PUB-2011-002}.

\begin{table}[h]
  \begin{center}
    \caption[Trigger efficiencies.]{Efficiencies of the trigger lines on signal simulation samples after stripping.
             The column titled `\mass{\Kp\Km} cut' refers to the \BsPhiKK phase space simulation sample with a cut of $\mass{\Kp\Km}<1800\mevcc$.
             The number quoted at each row is the fraction kept after applying the previous step.}
    \label{tab:BsPhiKK:trigeff}
    \begin{tabular}{|l|r|r|r|}
    \hline
    & \multicolumn{3}{c|}{Fraction of events kept [$\%$]} \\
    \multicolumn{1}{|c|}{\raisebox{1.5ex}[1.5ex]{Trigger line}} & 
    \multicolumn{1}{c}{\BsPhiKK} &
    \multicolumn{1}{c}{\mass{\Kp\Km} cut} &
    \multicolumn{1}{c|}{\BsPhiPhi} \\ \hline
    \lone Hadron TOS          & $\TrigEffBsphiKKMCnocutHadron   $ & $\TrigEffBsphiKKMCmKKcutHadron   $ & $\TrigEffBsphiphiMCnocutHadron   $ \\
    \lone Global TIS                  & $\TrigEffBsphiKKMCnocutGlobal   $ & $\TrigEffBsphiKKMCmKKcutGlobal   $ & $\TrigEffBsphiphiMCnocutGlobal   $ \\
    Total L0                       & $\TrigEffBsphiKKMCnocutLzero    $ & $\TrigEffBsphiKKMCmKKcutLzero    $ & $\TrigEffBsphiphiMCnocutLzero    $ \\ \hline
    HLT1 Track All \lone TOS    & $\TrigEffBsphiKKMCnocutHltOne   $ & $\TrigEffBsphiKKMCmKKcutHltOne   $ & $\TrigEffBsphiphiMCnocutHltOne   $ \\ \hline
    HLT2 Topo 3-Body TOS & $\TrigEffBsphiKKMCnocutThreeBody$ & $\TrigEffBsphiKKMCmKKcutThreeBody$ & $\TrigEffBsphiphiMCnocutThreeBody$ \\
    HLT2 Topo 4-Body TOS & $\TrigEffBsphiKKMCnocutFourBody $ & $\TrigEffBsphiKKMCmKKcutFourBody $ & $\TrigEffBsphiphiMCnocutFourBody $ \\
    Total HLT2                     & $\TrigEffBsphiKKMCnocutHltTwo   $ & $\TrigEffBsphiKKMCmKKcutHltTwo   $ & $\TrigEffBsphiphiMCnocutHltTwo   $ \\ \hline
    Total                          & $\TrigEffBsphiKKMCnocuttotal    $ & $\TrigEffBsphiKKMCmKKcuttotal    $ & $\TrigEffBsphiphiMCnocuttotal    $ \\ \hline
    \end{tabular}
  \end{center}
\end{table}
\begin{figure}
  \centering
  \begin{overpic}[width=0.49\textwidth]{body/BsPhiKK/figs/BsphiKK_MCHadronTOS.pdf}
    \put(50,15){\lone Hadron TOS}
  \end{overpic}
  \begin{overpic}[width=0.49\textwidth]{body/BsPhiKK/figs/BsphiKK_MCGlobalTIS.pdf}
    \put(50,15){\lone Global TIS}
  \end{overpic}
  \begin{overpic}[width=0.49\textwidth]{body/BsPhiKK/figs/BsphiKK_MCLevel0Tot.pdf}
    \put(50,15){L0 Total}
  \end{overpic}
  \begin{overpic}[width=0.49\textwidth]{body/BsPhiKK/figs/BsphiKK_MCHlt1Total.pdf}
    \put(50,15){HLT1 Total}
  \end{overpic}
  \begin{overpic}[width=0.49\textwidth]{body/BsPhiKK/figs/BsphiKK_MCTopo3Body.pdf}
    \put(50,15){Topo 3-Body}
  \end{overpic}
  \begin{overpic}[width=0.49\textwidth]{body/BsPhiKK/figs/BsphiKK_MCTopo4Body.pdf}
    \put(50,15){Topo 4-Body}
  \end{overpic}
  \begin{overpic}[width=0.49\textwidth]{body/BsPhiKK/figs/BsphiKK_MCHlt2Total.pdf}
    \put(50,15){HLT2 Total}
  \end{overpic}
  \begin{overpic}[width=0.49\textwidth]{body/BsPhiKK/figs/BsphiKK_MCTrigTotal.pdf}
    \put(50,15){Trigger Total}
  \end{overpic}
  \caption{Trigger efficiency in bins of \mass{\Kp\Km} using the \BsPhiKK simulation sample.
  }
  \label{fig:BsPhiKK:binnedHlt2Totaltrigeff}
\end{figure}

The efficiency of each trigger line, and their combinations, is calculated using simulation samples after stripping has been applied.
Table \ref{tab:BsPhiKK:trigeff} summarises the efficiency of each line for the \BsPhiKK and \BsPhiPhi samples.
In order to verify that the chosen trigger lines introduce a smooth acceptance function, which can be accurately modelled by the method described in Section~\ref{sec:BsphiKK:acceptance}, the trigger efficiency is calculated in 20 bins of \mass{\Kp\Km} between 990\mevcc and 1800\mevcc.
These distributions are shown in Figure~\ref{fig:BsPhiKK:binnedHlt2Totaltrigeff}.
The choice of trigger lines for this analysis differs from those in Chapter~\ref{ch:BsPhiPhi} because the inclusive \phiz and topological two-body lines preferentially select \BsPhiPhi events, causing a sharp peak in the trigger efficiency distribution at low \mass{\Kp\Km}.
The trigger efficiency of the \BsPhiPhi simulation sample is not guaranteed to be the same as in Table~\ref{tab:BsPhiPhi:trigeff} because the efficiencies are calculated after the samples have passed two different stripping lines.

\input{body/BsPhiKK/cuts}
\input{body/BsPhiKK/vetoes}
\input{body/BsPhiKK/mva}


