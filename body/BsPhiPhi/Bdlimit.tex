\section{Search for the decay \BdPhiPhi}
\label{sec:BsPhiPhi:Bdlimit}
% Introduction
The suppressed decay mode \BdPhiPhi is searched for by fitting the $\phiz\phiz$ invariant mass distribution, with a selection that differs only by the choice of cut on the output of the BDT classifier: BDT$>0.14$ rather than BDT$>-0.025$, as detailed in Section~\ref{sec:BsPhiPhi:mva}.
From the result of this fit, shown in Figure~\ref{fig:BsPhiPhi:phiphi_wBdmass} and Table~\ref{tab:BsPhiPhi:Bdphiphifitres}, it is clear that there is no significant \BdPhiPhi signal.
Hence, a limit is set on the value of the \BdPhiPhi branching fraction.

\subsection{Efficiency}

% Efficiency
The trigger and reconstruction efficiencies cancel between the \BdPhiPhi and \BsPhiPhi modes.
However, the invariant mass cuts applied by the stripping line are under the $\pip\pim\pip\pim$ mass hypothesis and are tight enough to cause a difference in the selection efficiency of the two modes.
In order to obtain the selection efficiency relative to \BsPhiPhi as a function of \PhiPhi invariant mass, a set of six \BsPhiPhi generator-level event samples are generated with varied $\Kp\Km\Kp\Km$ masses in a range 5159\mevcc to 5366\mevcc.
For reference, the \Bd mass is $5279.58\pm0.17\mevcc$, and the \Bs mass is $5366.77\pm0.24$~\cite{PDG2016}.
For each sample, the ratio of the efficiency of the preselection cuts relative to that of the $\mass{\Bs}=5366\mevcc$ sample is calculated.
A fit is made to this distribution, and the ratio of selection efficiencies between \BdPhiPhi and \BsPhiPhi is taken to be the value at $\mass{\Bs}=5279\mevcc$, which is 0.86.

\subsection{Mass fit}

% Fit
Two fits are performed to the \PhiPhi invariant mass distribution using the re-optimised multivariate selection.
The first is the background-only model, $f_{b}$, which is identical to the one described in Section~\ref{sec:BsPhiPhi:phiphimassfit}.
The other is the signal plus background model, $f_{s+b}$, which differs by the addition of a component to account for \BdPhiPhi decays.
The same strategy is used with regards to fixing parameters to values found from fitting the simulation, as in Section~\ref{sec:BsPhiPhi:massfits}.
For the fit with the \BdPhiPhi component, the branching fraction of the decay is used as a free parameter, labelled $BR$ in Table~\ref{tab:BsPhiPhi:Bdphiphifitres}, to control the size of the \Bd peak.
The results of the fit are shown in Figure~\ref{fig:BsPhiPhi:phiphi_wBdmass} and Table~\ref{tab:BsPhiPhi:Bdphiphifitres}.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.8\textwidth]{body/BsPhiPhi/figs/Fig3.pdf}
  \caption[A fit to the \PhiPhi invariant mass with the tight BDT selection applied.]
  {
    A fit to the \PhiPhi invariant mass with the tight BDT selection applied.
    The total PDF is shown as a red solid line,
    \BsPhiPhi as a blue long-dashed line,
    \BdPhiPhi as a blue short-dashed line,
    and the combinatorial background as a purple dotted line.
  }
  \label{fig:BsPhiPhi:phiphi_wBdmass}
\end{figure}

\begin{table}[htb]
  \centering
  \caption{Results of the fits to \BsPhiPhi simulation and data using a model containing a \BdPhiPhi component.}
  \begin{tabular}{|c|r@{ $\pm$ }l|}
    \hline
    Simulation fit parameter & \multicolumn{2}{c|}{Result}\\
    \hline
    $\sigma_1$ & $ 13.8 $&$ 0.1 $\mevcc\\
    $\sigma_2$ & $ 28.1 $&$ 0.4 $\mevcc\\
    $\sigma_3$ & $ 41.0 $&$ 1.0 $\mevcc\\
    $f_1$      & $(84.7 $&$ 1.2)$\,\%\\
    $f_2$      & $(13.6 $&$ 1.2)$\,\%\\
    \hline
    Data fit parameter & \multicolumn{2}{c|}{Result}\\
    \hline
    $\mu$ & $5366.5 $&$ 0.3  $\mevcc\\
    $R_s$ & $  1.02 $&$ 0.02 $\\
    $N_S$ & $  1995 $&$ 49   $\\
    $BR$  & $  (1.1 $&$ 1.7)\times10^{-8}$\\
    $N_B$ & $    76 $&$ 12   $\\
    \hline
  \end{tabular}
  \label{tab:BsPhiPhi:Bdphiphifitres}
\end{table}

The fitted value of the \BdPhiPhi branching fraction is found to be $\BF(\BdPhiPhi)=(1.1\pm1.7)\times10^{-8}$.
The significance of this peak is calculated from a scan of the log-likelihood as a function of the \BdPhiPhi branching fraction.
Figure \ref{fig:BsPhiPhi:LLscan} shows the difference in negative log-likelihood, $-\Delta \ln(\mathcal{L})$, from the minimum value, as a function of $\BF(\BdPhiPhi)$.
The value of $-\Delta \ln(\mathcal{L})$ in the null hypothesis is found to be $0.87$.
Using Wilks' theorem~\cite{Wilks:1938dza}, this translates to a significance of $\sqrt{-2\Delta \ln(\mathcal{L})} = 1.3\sigma$.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.6\textwidth]{body/BsPhiPhi/figs/LLscan.pdf}
  \caption
  {
    A profile scan of minimised $-\Delta \ln(\mathcal{L})$ over $\BF(\BdPhiPhi)$.
  }
  \label{fig:BsPhiPhi:LLscan}
\end{figure}

\subsection{Limit on the \BdPhiPhi branching fraction}

% CLs method
The limit on the \BdPhiPhi branching fraction is set using the \CLs method~\cite{Read:2002hq} as implemented in the RooStats package~\cite{Moneta:2010pm}.
%The \CLs method is widely used, most notably at LHCb in placing limits of the branching fraction of \decay{\Bz}{\mup\mum} decays~\cite{Aaij:2013aka}. 
%TODO: give more details about CLs method
The \CLs variable is calculated as the ratio of the $p$-value of the signal plus background hypothesis to the $p$-value of the background only hypothesis.
A likelihood ratio test statistic, $R_{L}$, is defined as
\[
R_{L} = \frac{\mathcal{L}(f_{s+b}(x))}{\mathcal{L}(f_{b}(x))},
\]
where $\mathcal{L}(f_{s+b}(x))$ is the likelihood evaluated for the signal plus background model, $\mathcal{L}(f_{b}(x))$ is the likelihood evaluated for the background-only model, and $x$ is the set of parameters in the model.

At each point in a scan through the values of the \BdPhiPhi branching fraction, $R_L$ is calculated from fitting the data.
These fits also determines the values of the nuisance parameters ($\mu$, $R_s$, $N_S$ and $N_B$) to be used when generating toys.
The models $f_{s+b}$ and $f_b$ are each used to generate 50,000 toys.
The values of \CLsb and \CLb are calculated as the number of corresponding toys with a value of $-2\ln(R_L)$ above that found in data.
The value of \CLs is then calculated as
\[
\CLs = \frac{\CLsb}{\CLb}.
\]
\begin{figure}[h]
  \centering
  \includegraphics[width=0.8\textwidth]{body/BsPhiPhi/figs/Fig4.pdf}
  \caption[Results of the \CLs scan.]
  {
    Results of the \CLs scan with the observed \CLs values plotted as black points.
    The black dashed line is the expected distribution.
    The green and yellow bands mark the $1\sigma$ and $2\sigma$ confidence regions.
    The upper limit at $90\,\%$ $(95\,\%)$ confidence level is where the observed \CLs line intercepts the red solid (dashed) horizontal line.
  }
\label{fig:BsPhiPhi:clsfinal}
\end{figure}

% Result
Figure~\ref{fig:BsPhiPhi:clsfinal} shows the results of the \CLs scan.
The upper limit at $90\,\%$ ($95\,\%$) confidence level is taken as where the observed \CLs distribution falls below 0.1 (0.05).
The upper limits on the \BdPhiPhi branching fraction are measured to be
\[
\begin{array}{lccr}
\BF(\BdPhiPhi) &<& 2.8 \times 10^{-8} & (90\,\%\;\mathrm{CL}),\\
\BF(\BdPhiPhi) &<& 3.4 \times 10^{-8} & (95\,\%\;\mathrm{CL}).
\end{array}
\]
%TODO systematics as nuisance parameters
The limit at $90\,\%$ confidence level is a factor of seven improvement on the previous result from \babar: $\BF(\BdPhiPhi) < 2.0 \times 10^{-7}$~\cite{Aubert:2008fq}.
It is close to the higher of the theoretical predictions, which lie in the range $(0.1 \; \mathrm{to} \; 3.0) \times 10^{-8}$ \cite{Beneke:2006hg,Lu:2005be,BarShalom:2002sv}.

