\section{Event selection}
\label{sec:BsPhiPhi:selection}

This section describes the process of selecting data and simulation events to be used in this analysis.
The goal of the selection is to remove combinatorial and misidentified background events without sacrificing signal efficiency.
A more general overview of the event selection strategy is given in Chapter~\ref{ch:analysis}.

\subsection{Dataset}

The analysis uses the full Run 1 dataset, described in Section~\ref{sec:analysis:dataset}.
Simulated \BsPhiPhi, \BdPhiPhi and \BdPhiKstar events are used to obtain the parameters of the invariant mass distributions of the signal and normalisation decay modes, as well as the ratio of the selection efficiencies between these modes.

A potentially important background for the signal channel arises from the decay \LbPhipK, where the \proton is misidentified as a \Kp.
In order to study this, a sample of simulated \LbPhipK events is generated with the \proton and \Km produced according to phase space, \ie completely non-resonant three-body \LbPhipK decays.

Similarly, two important backgrounds for the normalisation channel are the decays \LbPhipK, where the \proton is misidentified as a \pip, and \decay{\Lb}{\phiz \proton \pim}, where the \proton is misidentified as a \Kp.
In order to study these, a mixed sample of \LbPhipK and \decay{\Lb}{\phiz \proton \pim} decays, hereby referred to as \LbPhiph, is generated according to a phase space distribution, as the resonant structures of these decays are unknown.

\subsection{Trigger lines}

The candidate events used in this analysis are required to pass a set of specific trigger lines.
More information on the specifics of each line can be found in Section~\ref{sec:analysis:triggers}.
Table~\ref{tab:BsPhiPhi:trigeff} shows the efficiency of each of these lines on the signal and normalisation simulation samples.

\begin{table}[h]
\caption[Trigger efficiency of the simulation samples.]{The trigger efficiency for the signal and normalisation simulation samples. These efficiencies are calculated using truth-matched simulation events that pass the stripping selection.
The total \lone efficiency is the efficiency of events to pass either of the chosen \lone trigger lines.
The \hltone efficiency is calculated after applying the \lone trigger requirements.
Similarly, the \hlttwo efficiencies are calculated after applying the \lone and \hltone trigger requirements.
The total efficiency is calculated requiring the candidate to pass at least one line from each layer.}
\begin{center}
\begin{tabular}{|l|c|c|} \hline
    & \multicolumn{2}{c|}{Fraction of events kept [$\%$]} \\
    \multicolumn{1}{|c}{\raisebox{1.5ex}[1.5ex]{Trigger line}} &
    \multicolumn{1}{|c}{\BsPhiPhi} &
    \multicolumn{1}{c|}{\BdPhiKstar} \\ \hline
\lone Hadron TOS             & $16.34 \pm 0.08$ & $23.21 \pm 0.08$ \\
\lone Global TIS             & $25.55 \pm 0.10$ & $30.53 \pm 0.09$ \\
Total \lone                  & $36.06 \pm 0.10$ & $44.79 \pm 0.09$ \\ \hline
\hltone Track All \lone TOS  & $78.52 \pm 0.15$ & $80.88 \pm 0.11$ \\ \hline
\hlttwo Topo 2-Body TOS      & $62.19 \pm 0.20$ & $69.29 \pm 0.14$ \\
\hlttwo Topo 3-Body TOS      & $84.85 \pm 0.15$ & $86.37 \pm 0.11$ \\
\hlttwo Topo 4-Body TOS      & $76.45 \pm 0.17$ & $72.81 \pm 0.14$ \\
\hlttwo Inclusive \phiz TOS  & $90.89 \pm 0.12$ & $66.99 \pm 0.14$ \\
Total \hlttwo                & $98.21 \pm 0.05$ & $94.99 \pm 0.07$ \\ \hline
Total                        & $27.81 \pm 0.10$ & $34.41 \pm 0.09$ \\ \hline
\end{tabular}
\label{tab:BsPhiPhi:trigeff}
\end{center}
\end{table}

\subsection{Cut-based selection}
\label{sec:BsPhiPhi:cuts}

% Stripping
The selection of \BsPhiPhi and \BdPhiKstar candidates starts from a stripping line designed to select the decay mode \decay{\Bs}{(\decay{\rhoz}{\pip\pim})(\decay{\rhoz}{\pip\pim})}.
More information about the stripping selection can be found in Section~\ref{sec:analysis:stripping}.
Due to the wide invariant mass windows and absence of PID requirements, this line is well suited to selecting similar quasi-two-body $B$ meson decay modes with four charged particles in the final state.

\begin{figure}[htb]
  \setlength{\unitlength}{1mm}
  \centering
  \begin{picture}(140,50)
    \put(0,0){
      \includegraphics[width=70mm]{body/BsPhiPhi/figs/GhostProb}
      \includegraphics[width=70mm]{body/BsPhiPhi/figs/TrackPT}
    }
    \put(47,25){\small Kept}
    \put(47,22){$\longleftarrow$}
    \put(94,25){\small Kept}
    \put(94,22){$\longrightarrow$}
  \end{picture}
  \caption[Track ghost probability and \pt distributions.]{
  Left: distribution of track ghost probability in selected data events (black points) and simulation (orange histogram). 
  Right: distribution of track \pt of kaons from generator-level \BsPhiPhi events without cuts.
  The distributions are normalised to unit area.
  The red dashed lines indicate the value of the applied cuts.
  }
  \label{fig:BsPhiPhi:trackdists}
\end{figure}

The stripping selection proceeds as follows.
Well-reconstructed tracks that traverse the entire length of the detector are chosen.
Here, the quality of the track reconstruction is judged by the \chisq per degree of freedom (\chisqndf) of the track fit, which is required to be less than 4.
In addition, the ghost probability for each track, defined in Sections~\ref{sec:detector:trackreco} and \ref{sec:analysis:trackvariables}, is required to be less than 0.8.
The distribution of this variable is shown in Figure~\ref{fig:BsPhiPhi:trackdists} (left).
Tracks originating from the primary vertex are removed by requiring each track to have an impact parameter \chisq greater than 16.
Each track is required to have a transverse momentum, \pt, of at least 500\mevc.
The distribution of this variable is shown in Figure~\ref{fig:BsPhiPhi:trackdists} (right).
Track pairs are combined, under the \pip\pim mass hypothesis, to form `\rhoz' candidates,
which are required to have $\mass{\pip\pim}<1100\mevcc$, $\ptot>1000\mevc$ and $\pt>900\mevc$.
The \chisqndf of the vertex fit for each \rhoz candidate is required to be less than 16.
Pairs of \rhoz candidates are combined to form \Bd candidates,
which are required to have an invariant mass between 4500 and 5700\mevcc.
Each \Bd candidate is required to have a vertex fit \chisqndf less than 12 and an impact parameter \chisq greater than 20.
The angle between the reconstructed momentum vector of the \Bd candidate and the displacement vector from the primary vertex to the decay vertex is required to be less than 0.045 radians. % COS(DIRA)>0.999

\begin{figure}[htb]
  \setlength{\unitlength}{1mm}
  \centering
  \begin{picture}(140,50)
    \put(0,0){
      \includegraphics[width=70mm]{body/BsPhiPhi/figs/kaonPID}
      \includegraphics[width=70mm]{body/BsPhiPhi/figs/pionPID}
    }
    \put(15,25){\small Kept}
    \put(15,22){$\longrightarrow$}
    \put(96,25){\small Kept}
    \put(96,22){$\longrightarrow$}
  \end{picture}
  \caption[Distributions of the PID variables in data and simulation.]{Distributions of the PID variables $\prob{\PK}(1-\prob{\Ppi})$ (left) and $\prob{\Ppi}$ (right) in data with loose selection (black points) and simulation (orange histogram), where the simulated particles are true kaons and pions, respectively. The red dashed lines indicate the value of the applied cuts. The distributions are normalised to unit area.}
  \label{fig:BsPhiPhi:PIDdists}
\end{figure}

% Offline cuts % efficiencies of PID cuts
The next step is a tighter cut-based selection specific to each decay mode.
Kaons and pions are identified using the neural-net hadron probability variables, \prob{\Ph}, which are described in Section~\ref{sec:analysis:pid}.
Kaon candidates are required to have ${\prob{\PK}(1-\prob{\Ppi})>0.025}$ and pion candidates to have ${\prob{\Ppi}>0.2}$.
The pion candidate in the \BdPhiKstar selection is chosen to be the particle with the largest value of \prob{\Ppi}.
Figure~\ref{fig:BsPhiPhi:PIDdists} shows the distributions of these variables for the stripped data and simulation.
To ensure the two samples are mutually exclusive, if the pion candidate has ${\prob{\Ppi}>\prob{\PK}}$, it is kept in the \BdPhiKstar sample and rejected from the \BsPhiPhi sample.
Pairs of particles identified as \Kp\Km and \Kp\pim are combined to form \phiz and \Kstarz candidates.
The invariant mass of each \phiz candidate is required to be within 15\mevcc of the nominal value~\cite{PDG2016}.
Similarly, the mass of each \Kstarz candidate must be within 150\mevcc of the nominal value~\cite{PDG2016}.
These mass windows are chosen to match those used in the \lhcb analyses of \BsPhiPhi~\cite{LHCb-PAPER-2014-026} and \BdPhiKstar~\cite{LHCb-PAPER-2014-005}, respectively, so that previously measured S-wave fractions can be used without the need to repeat the angular fits (see Section~\ref{sec:BsPhiPhi:swave}).
The invariant mass distributions of the signal and normalisation samples after the cut-based selection are shown in Figure~\ref{fig:BsPhiPhi:invariantmasses}.
It can be seen that the \BsPhiPhi selection already results in a small combinatorial background.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.49\textwidth]{body/BsPhiPhi/figs/phiphi_mass_nomva.pdf}
  \includegraphics[width=0.49\textwidth]{body/BsPhiPhi/figs/phikst_mass_nomva.pdf}
  \caption[Invariant mass distributions of the data using samples with the cut-based selection applied.]{Invariant mass distributions of the data using samples with the cut-based selection applied. Left: \BsPhiPhi. Right: \BdPhiKstar. The red dotted lines indicate the \Bd and \Bs masses.}
  \label{fig:BsPhiPhi:invariantmasses}
\end{figure}

% Vetoes
%TODO this bit

\subsection{Multivariate selection}
\label{sec:BsPhiPhi:mva}
%Training
This analysis uses a Boosted Decision Tree classifier (BDT, see Section~\ref{sec:analysis:mva}) developed for the analysis of the \BsPhipipi decay published in Ref.~\cite{LHCb-PAPER-2016-028} and detailed in Ref.~\cite{Haofei}.
This is trained to distinguish four-body hadronic \bquark hadron decays from combinatorial background without using PID information.
The training variables are the $\chisq_{\rm{IP}}$, $\chisq_{\rm{Vtx}}$, flight distance, lifetime, \pt and $\eta$ of \Bs meson, the $\eta$ and the minimum \pt of the $\Kp\Km$ and $\pip\pim$ pairs and the minimum \pt and minimum $\chisq_{\rm{IP}}$ of the tracks.
The signal sample used to train the BDT consists of simulated \BsPhipipi events, and the background sample consists of the upper sideband of data with the cut-based preselection for \BsPhipipi applied.

The BDT output in simulation is compared to data using the \sPlot technique~\cite{Pivk:2004ty}.
This is a method of obtaining the distributions of variables for separate components (\eg signal and background) within a data sample.
A set of weights --- one per component --- is assigned to each event in the sample based on a maximum likelihood fit to a `discriminating' variable, for which the shapes of the components are known.
In this case, these `s-weights' are obtained from fits to the \mass{\Kp\Km\Kp\Km} and \mass{\Kp\Km\Kp\pim} distributions of the data samples, shown in Figure~\ref{fig:BsPhiPhi:invariantmasses}, using the models described in Section~\ref{sec:BsPhiPhi:massfits}.
The s-weights associated with a certain component can then be used to plot the distributions of other variables for that component only, without {\it a priori} knowledge of its shape.
Figure~\ref{fig:BsPhiPhi:bdtcomp} shows the BDT output in the simulation compared to the data, weighted with the s-weights associated with the signal.
It can be seen that there is some disagreement between data and simulation, therefore care must be taken when quantifying the efficiency of the cut on the MVA output.

%TODO data/MC comparison of all variables here or in appendix

\begin{figure}[h]
  \centering
  \includegraphics[width=0.49\textwidth]{body/BsPhiPhi/figs/c_bdt.png}
  \includegraphics[width=0.49\textwidth]{body/BsPhiPhi/figs/c_bdt_Bd.png}
  \caption[Comparison of the BDT output between simulation and data.]{Comparison of the BDT output between the simulation (solid orange) and s-weighted data (black points) using samples with the cut-based selection applied. Left: \BsPhiPhi. Right: \BdPhiKstar.}
  \label{fig:BsPhiPhi:bdtcomp}
\end{figure}

%Optimisation
The choice of cut on the BDT output is optimised separately for the \BsPhiPhi branching fraction measurement and the search for the decay \BdPhiPhi.
For the branching fraction measurement, the cut on the BDT output is chosen to maximise the `signal significance' figure of merit for the \BdPhiKstar decay mode and then applied to both the signal and normalisation samples.
The figure of merit is defined as
\begin{equation}
FOM = \frac{S}{\sqrt{S+B}},
\label{eq:BsPhiPhi:phikstFOM}
\end{equation}
where $S$ is the number of signal candidates, and $B$ is the number of background events in a region $\pm 40 \mevcc$ around the \Bd mass, corresponding to a $\sim \pm 2\sigma$ window.
It is chosen to optimise for the \BdPhiKstar signal, rather than \BsPhiPhi, since the combinatorial background is larger, as can be seen in Figure~\ref{fig:BsPhiPhi:invariantmasses}.
The number of \BdPhiKstar candidates in the data before any cut is applied on the BDT output, $S_0$, is determined to be $6728 \pm 97$ by fitting the \PhiKstar invariant mass after the cut-based selection, using the model described in Section~\ref{sec:BsPhiPhi:phikstarmassfit}.
For a given cut on the BDT output, the number of signal events is $S = S_0 \cdot \varepsilon_S$, where the efficiency to pass the BDT cut, $\varepsilon_S$, is determined using the \BdPhiKstar Monte Carlo sample.
The number of background candidates, $B$, is obtained by counting the number of candidates in the sideband region $5600 < \mass{\PhiKstar} < 5800 \mevcc$ that pass the cut on the BDT output and scaling this number by the relative size of the signal and sideband windows.
The signal region is 80\mevcc wide, giving a scaling factor of 0.4.
This procedure assumes that the combinatorial background is flat in invariant mass, which is seen to be the case in Figure~\ref{fig:BsPhiPhi:sideband}.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.5\textwidth]{body/BsPhiPhi/figs/sideband_phikst.pdf}
  \caption{The \mass{\PhiKstar} distribution of the sideband sample used in optimising the `signal significance' figure of merit.}
  \label{fig:BsPhiPhi:sideband}
\end{figure}

The figure of merit is plotted as a function of the value of the cut on the BDT output on the left of Figure~\ref{fig:BsPhiPhi:bdt_response}.
The optimal value of the cut is BDT$>-0.025$.
The effect of this cut on the data can be found by comparing the yields of fits to the data samples using the models described in Sections~\ref{sec:BsPhiPhi:massfits} before and after the cut.
The signal yield for the \BsPhiPhi (\BdPhiKstar) mode is $2341 \pm 50$ ($6728 \pm 97$) before the cut and $2309 \pm 49$ ($6680 \pm 86$) after.
The efficiency of this cut on the \BsPhiPhi (\BdPhiKstar) simulation sample is found to be $98.65\pm 0.05\,\%$ ($98.35\pm 0.04\,\%$).
For comparison, the efficiency of this cut on the \BsPhipipi training sample is $97.6\,\%$.

The efficiency of the BDT cut on the \BdPhiKstar mode disagrees between data and simulation by a factor of $0.65\,\%$.
This is assigned as a systematic uncertainty in Section~\ref{sec:BsPhiPhi:externalsystematics}.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.49\textwidth]{body/BsPhiPhi/figs/phikst_bdt_response.pdf}
  \includegraphics[width=0.49\textwidth]{body/BsPhiPhi/figs/phiphi_bdt_response.pdf}
  \caption[Optimal cuts for the different figures of merit.]{The (left) signal significance figure of merit for \BdPhiKstar and (right) Punzi figure of merit for \BdPhiPhi plotted as a function of the value of the cut on the BDT output. The red dotted lines mark the maxima of the distributions, which correspond to the optimal cut values. The distributions are normalised to the number of events in data.}
  \label{fig:BsPhiPhi:bdt_response}
\end{figure}

In the search for \BdPhiPhi, since there is no visible signal in Figure~\ref{fig:BsPhiPhi:invariantmasses} and no precise prediction for the branching fraction, the Punzi figure of merit \cite{Punzi:2003bu} is used.
The aim of this figure of merit is to find a region of parameter space (in this case, simply the cut on the BDT output) that maximises the chance of observing a signal above a chosen significance level.
This does not require an initial signal yield as input, making it suitable for searches for decay modes where the expected signal yield is unknown.
It is defined as
\begin{equation}
  FOM = \frac{\varepsilon_{S}}{\frac{n}{2}+\sqrt{B}},
  \label{eq:BsPhiPhi:phiphiFOM}
\end{equation}
where $\varepsilon_{S}$ is the signal efficiency, $B$ is the number of background events, and $n$ is the desired significance in terms of standard deviations.
A value of $n=3$ is chosen in order to optimise for a possible $3\sigma$ result, which is the conventional threshold for claiming `evidence' of a signal.
Since a fully simulated \BdPhiPhi sample was not available, the signal sample used to optimise this figure of merit consists of simulated \BsPhiPhi events in the window $\left|\mass{\PhiPhi}-\mass{\Bs}\right|<40$\mevcc,
which relies on the assumption that the MVA cut efficiency the \BsPhiPhi and \BdPhiPhi modes are the same.
The signal efficiency in Equation~\ref{eq:BsPhiPhi:phiphiFOM} is the proportion of events in the window that passes a given cut on the BDT output.
The background sample is the upper mass sideband of the data ($5800\mevcc < \mass{\Kp\Km\Kp\Km} < 6000\mevcc$) after the cut-based selection.
As with the other figure of merit, the number of background events is found by counting the number of events in this window and multiplying this by a factor of 0.4.
The plot on the right of Figure~\ref{fig:BsPhiPhi:bdt_response} shows this FOM as a function of BDT cut.
The optimal cut value is BDT$>0.14$.
The jagged shape of the curve is due to the low background statistics: the initial number of events in the upper mass sideband is 184, and zero background events remain at the value of the optimal cut.
The efficiency of this cut is $(86.4 \pm 0.2)\,\%$ on the \BdPhiPhi simulation sample and $(88.3 \pm 0.1)\,\%$ on the \BsPhiPhi simulation sample.

