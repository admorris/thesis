\section{Systematic uncertainties}
\label{sec:BsPhiPhi:systematics}

The sources of systematic uncertainties on the \BsPhiPhi branching fraction measurement are discussed in this section.

\subsection{Choice of fit model}
\label{sec:BsPhiPhi:fitmodelsystematic}

The uncertainty on the \BsPhiPhi branching fraction due to the choice of fit model is estimated by fitting the invariant mass distributions with different functions for the signal and background components.
The largest deviation in the ratio of yields is taken as an estimate of the systematic uncertainty.
This is done separately for the signal, combinatorial background and misidentified background components.
The assigned uncertainty is the sum in quadrature of the three.

The functions considered for the signal components are the sum of two or three Gaussian functions, a Student's T function\footnote{See Appendix~\ref{sec:misc:studentt} for the explicit form of this function.}, and the sum of a Crystal Ball function with one or two Gaussian functions.
The largest deviation is found using the Student's T function for \BsPhiPhi and the sum of a Crystal Ball with two Gaussian functions for \BdPhiKstar, which gives a relative uncertainty of 0.59\,\%.

In the case of the background model, the largest deviation is found using the linear function for both the signal and normalisation modes, which gives a relative uncertainty of 0.16\,\%.
The effect of removing the \LbPhiph peaking background component from the \PhiKstar invariant mass model is also tested and found to give a relative uncertainty of 0.03\,\%.
The total relative uncertainty from the choice of fit model on the ratio of yields is 0.61\,\%.

\subsection{Particle identification efficiency}
\label{sec:BsPhiPhi:pidsystematic}

An uncertainty on the PID efficiencies arises from the calibration method.
This is quantified by varying the binning scheme for the calibration sample.
The default binning scheme has $21 \times 4 \times 4$ bins, corresponding to the three variables: \pt, $\eta$ and track multiplicity.
The number of bins is increased to $31 \times 6 \times 6$, while retaining the same range for each variable.
The efficiency tables for kaons and pions are regenerated with this finer binning, and the efficiency of the PID cuts is recalculated using the method described in Section~\ref{sec:BsPhiPhi:pideff}.
This causes a change in the PID efficiency ratio of 0.3\%, compared to the value calculated with the default scheme.
This change is taken as the systematic uncertainty on the PID efficiency.

\subsection{Hardware-level hadron trigger}
\label{sec:BsPhiPhi:hadrontriggersystematic}

The \lone hadron trigger is not reproduced perfectly in the simulation.
A data-driven recalibration of this trigger efficiency is performed using the large sample of \decay{\Dz}{\Kpm\pimp} decays collected during 2011 and 2012.
The calibration is then applied to the \BsPhiPhi and \BdPhiKstar simulation samples in place of the default trigger emulation.
This sample consists of binned distributions of the \lone hadron trigger efficiency for charged pions and kaons as functions of charge, transverse momentum and magnet polarity.

\begin{table}[htb]
  \centering
  \caption{Comparison between trigger efficiencies using the simulation and the data-driven resimulation methods for the \lone hadron trigger.}
  \begin{tabular}{|c|c|c|c|}
    \hline
    Total Trigger Efficiency & \BdPhiKstar [\%] & \BsPhiPhi [\%] & Ratio \\
    \hline
    Original simulation & $34.4 \pm 0.1$ & $27.8 \pm 0.1$ & $1.237 \pm 0.006$ \\
    Data-driven method & $29.2 \pm 0.1$ & $23.8 \pm 0.1$ & $1.227 \pm 0.007$ \\ % σε = sqrt((1-ε)ε/N) where N_ϕK* = 291884 and N_ϕϕ = 210372
    \hline
  \end{tabular}
  \label{tab:BsPhiPhi:l0eff}
\end{table}

The resimulation proceeds as follows.
On an event-by-event basis, four random numbers between 0 and 1 are generated and assigned to each particle and compared with the value of the trigger efficiency as found from the calibration sample.
If the random number is less than the efficiency for any of the particles, the event is deemed to have passed the trigger.
The systematic uncertainty is the relative change in the ratio of total trigger efficiencies when using the data-driven resimulation of the \lone hadron trigger, compared to the `default' method of using the efficiencies from the original simulation.
The values are given in Table~\ref{tab:BsPhiPhi:l0eff}, and the resulting systematic uncertainty is 1.1\,\%.

\subsection{Hadronic interaction with the detector}
\label{sec:BsPhiPhi:hadroninteractionsystematic}

%Introduction
Hadrons passing through the detector can interact strongly with the detector material.
Those which are absorbed before the end of the tracking system will not pass the `long track' requirement of the stripping line.
The loss of particles as a function of distance, $z$, in the direction of the beam is approximated as
\[
  \exp(-z/\lambda_{I}),
\]
where $\lambda_{I}$ is the interaction length, \ie the mean distance that the particle travels through a material before interacting.
The pion and kaon hadronic interaction lengths differ by up to $\sim 30\,\%$ in the momentum range between 5 and 100\gevc~\cite{Miglioranzi:1322668}.
The hadronic interaction lengths are not perfectly reproduced in the simulation, which leads to a systematic uncertainty on the ratio of selection efficiencies.

%Method
A sample of simulated \decay{\Bz}{\jpsi \Kstar} events is used to categorise the interaction type of kaons and pions that have end-vertices before the end of the tracking system ($z=9.41$~m).
The fractions of kaons and pions that interact hadronically with the detector in this region are shown in Table~\ref{tab:BsPhiPhi:hadinteraction_raw}.
This information is used to calculate $\lambda_{I}$ for each hadron species.

\begin{table}
  \centering
  \caption{Fraction of kaons and pions from \decay{\Bz}{\jpsi \Kstar} decays which have a hadronic interaction end-vertex before the end of the tracking system and the inferred interaction lengths.}
  \label{tab:BsPhiPhi:hadinteraction_raw}
  \begin{tabular}{|c|c|c|}
    \hline
    Particle & Fraction [\%] & $\lambda_{I}$ [m] \\
    \hline
    \pip & $14.7 \pm 0.6$ & $59.2 \pm 2.5$ \\
    \pim & $15.4 \pm 0.6$ & $56.3 \pm 2.3$ \\
    \Kp  & $11.5 \pm 0.5$ & $77.0 \pm 3.4$ \\
    \Km  & $13.7 \pm 0.5$ & $63.9 \pm 2.4$ \\
    \hline
  \end{tabular}
\end{table}
 
The simulation is considered to reproduce the hadronic interaction length to an accuracy of $10\,\%$~\cite{LHCb-PAPER-2010-001}, hence the relative uncertainty on the number of remaining particles is
\[
  \sigma = 1 - \frac{\exp(-z/\lambda_{I})}{\exp(-z/1.1\lambda_{I})}.
\]
Table~\ref{tab:BsPhiPhi:hadinteraction_error} lists the uncertainties obtained using this formula.
Since the final states differ by the exchange of a kaon with a pion, the effect on the ratio of selection efficiencies largely cancels.
The residual uncertainty is calculated as the difference between the charge-averaged pion and kaon uncertainties.
A relative systematic uncertainty of $0.26\,\%$ is assigned to the result.

\begin{table}
  \centering
  \caption{Uncertainties on the number of kaons and pions passing the `long track' cut due to the miss-modelling of hadronic interactions.}
  \label{tab:BsPhiPhi:hadinteraction_error}
  \begin{tabular}{|c|c|}
  \hline
    Particle & Uncertainty [\%] \\
    \hline
    \pip & 1.44 \\
    \pim & 1.51 \\
    \Kp  & 1.10 \\
    \Km  & 1.33 \\
    \hline
  \end{tabular}
\end{table}

\subsection{Tracking efficiency}
\label{sec:BsPhiPhi:trackingsystematic}

%Introduction
There is an approximate $2\,\%$ discrepancy in the efficiency of track reconstruction between data and simulation~\cite{LHCb-DP-2013-002}.
Since the final states of the signal and normalisation modes each have four tracks, this uncertainty largely cancels.
However, the presence of the low-momentum pion in the decay \BdPhiKstar leads to a residual uncertainty.

%Method
A look-up table of tracking efficiency ratios, and their uncertainties, binned in $\eta$ and \pt, is provided by the \lhcb tracking group.
The table is illustrated as a two-dimensional histogram in Figure~\ref{fig:BsPhiPhi:trackeff}.
A number of pseudo-experiments are performed, in which each efficiency ratio is varied randomly within its uncertainty.
The events in the \BdPhiKstar and \BsPhiPhi simulation samples are weighted by the product of the tracking efficiency ratios for each track.
The ratio of selection efficiencies is then calculated from the sum of the weights of each sample.
A distribution is obtained by performing 500 such pseudo-experiments.
%Result
A bias of $0.5\,\%$ is found and assigned as a systematic uncertainty.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.6\textwidth]{body/BsPhiPhi/figs/Track_Eff}
  \caption{The look-up table of tracking efficiency ratios, binned in $\eta$ and \pt.}
  \label{fig:BsPhiPhi:trackeff}
\end{figure}

\subsection{S-wave subtraction and angular acceptance}
\label{sec:BsPhiPhi:angaccsystematic}

The subtraction of the $\Kp\Km$ and $\Kpm\pimp$ S-wave contributions from the \BdPhiKstar yield is done using the results from a prior \lhcb angular analysis of the decay~\cite{LHCb-PAPER-2014-005}.
It is assumed that the ratio of detection efficiencies for S-wave events and P-wave events is the same in this analysis.
If this assumption is invalid, the S-wave fraction will differ from the values found in the angular analysis.

This effect is quantified using the acceptance calculated in the angular analysis.
Three event categories are considered: those that are true \BdPhiKstar decays; events where the $\Kp\Km$ pair is in an S-wave configuration, but the $\Kpm\pimp$ pair is in a P-wave configuration and events with P-wave $\Kp\Km$ and S-wave $\Kpm\pimp$.
The contribution from events where both the track pairs are in an S-wave configuration is considered to be negligible.
These categories differ from one another in their distributions of helicity angles and two-body invariant masses.
In the \BdPhiKstar angular analysis, the acceptance is calculated as a function of the three helicity angles and $\Kpm\pimp$ invariant mass, since there was found to be no dependence on the $\Kp\Km$ invariant mass.
%This is done separately for different outputs from the hardware hadron trigger: trigger-on-signal (TOS) and non-TOS.
%One-dimensional projections of the angular acceptance for events in each trigger category are shown in Figure~\ref{fig:BsPhiPhi:PhiKst_acceptance}.
%
%\begin{figure}[h]
%  \centering
%  \includegraphics[width=0.49\textwidth]{body/BsPhiPhi/figs/FromDean_acceptance_TOS.pdf}
%  \includegraphics[width=0.49\textwidth]{body/BsPhiPhi/figs/FromDean_acceptance_TIS.pdf}
%  \caption{One-dimensional projections of the acceptance for \BdPhiKstar as a function of the helicity angles and the $K\pi$ mass.
%  Here $\cos{\theta_{1(2)}}$ is the angle between the $K^+$ direction in the $K^{*0}$ ($\phi$) rest frame and the $K^{*0}$ ($\phi$) in the $B^0$ rest frame, and $\Phi$ is the angle between the $K^*$ and $\phi$ decay planes.
%  The black data points are obtained from fully simulated \BdPhiKstar data, and the blue curves are a visualisation of the acceptance using Legendre polynomials.
%  The upper (lower) set of four plots shows the acceptance for events that are TOS (not TOS) on the L0Hadron trigger. Reproduced from~\cite{PhiKst_ANA}.}
%  \label{fig:BsPhiPhi:PhiKst_acceptance}
%\end{figure}

Toy datasets are generated for each of the event categories, using the PDF from the angular analysis with parameters fixed to the fit results.
Weights are assigned to each toy event using the four-dimensional angular acceptance.
The ratio of the average efficiencies for each category is taken as the ratio of the sum of these per-event weights.
The weights for the two S-wave categories are each summed in the ratio given by the relevant S-wave fractions found in the angular analysis: $12.2\,\%$ for $\Kp\Km$ and $14.3\,\%$ for $\Kp\pim$.
This is then divided by the sum of the P-wave weights, giving the efficiency of S-wave events relative to that of P-wave events.

The difference in efficiency between the S-wave and P-wave events is multiplied by the total S-wave fraction to give a relative systematic uncertainty of $1.1\,\%$ on the branching fraction result.
A similar angular acceptance uncertainty is not necessary for the \BsPhiPhi mode, since the S-wave fraction is measured with an angular analysis \cite{LHCb-PAPER-2014-026} to be at the level of $1\,\%$, and hence any effect is negligible.

\subsection{Other uncertainties and summary}
\label{sec:BsPhiPhi:externalsystematics}

%Branching fraction analyses of \Bs decays may incur a systematic uncertainty due to the finite size of the width difference \DGs, if the effective lifetime in the considered final state is not equal to the lifetime assumed in the MC used to evaluate the efficiencies~\cite{deBruyn_Bs}.
%The MC used in this analysis has a lifetime which is (the inverse of) the average of the decay widths, \GL and \GH, of the light and heavy mass eigenstates.
%This is what the effective lifetime of \BsPhiPhi should be, in the absence of large New Physics effects in this decay.
%Numerically, the MC lifetime is 1.510 ps. Meanwhile the current PDG value for the average \Bs lifetime is 1.512 ps~\cite{PDG2016}.
%Since these two values are very close to one another, any possible systematic effect should be very small, and so is neglected.

%Further systematic checks of the influence of the choice of MVA
%classifier and the stability of the result between 2011 and 2012 are
%described in Appendix~\ref{app:checks}.

As mentioned in Section~\ref{sec:BsPhiPhi:mva}, the efficiency of the BDT cut on the \BdPhiKstar mode disagrees between data and simulation.
This is accounted-for by assigning a systematic uncertainty of $0.7\,\%$ on the ratio of selection efficiencies.

In the calculation of the \BsPhiPhi branching fraction, the branching fractions of \BdPhiKstar~\cite{Aubert:2008zza,Prim:2013nmy}, \PhiToKK and \KstarToKpi~\cite{PDG2016}, as well as the ratio of fragmentation fractions~\cite{LHCb-PAPER-2011-018,LHCb-PAPER-2012-037, LHCb-CONF-2013-011}, are used as external inputs.
The uncertainties on these quantities are treated as sources of systematic uncertainty on the \BsPhiPhi branching fraction result.
A summary of the systematic uncertainties presented in this section is shown in Table~\ref{tab:BsPhiPhi:BRsyst}.
The total relative systematic uncertainty on the branching fraction result is taken as the sum in quadrature of the individual uncertainties.
The dominant uncertainties come from the S-wave fractions, the ratio of fragmentation fractions and the normalisation channel branching fraction.
The latter two uncertainties are quoted separately in the result shown in Section~\ref{sec:BsPhiPhi:BsBFresult}, in order to assist calculating the \BsPhiPhi branching fraction again, should more precise measurements become available.

\begin{table}[h]
  \centering
  \caption[Summary of systematic uncertainties for the \BsPhiPhi branching fraction.]
  {
    Summary of systematic uncertainties for the \BsPhiPhi branching fraction measurement.
    The S-wave uncertainty is discussed in Section~\ref{sec:BsPhiPhi:swave}.
    The uncertainties due to the detection efficiency are discussed in Section~\ref{sec:BsPhiPhi:efficiencies}.
    Uncertainties on external inputs are discussed in Section~\ref{sec:BsPhiPhi:BsBFresult}.
  }
  \begin{tabular}{|c|c|c|}
    \hline
    Systematic & Source & Relative error [\%] \\
    \hline
    Fit model & Vary fit model in data                  & 0.6\\
    \BdPhiKstar BDT efficiency & Yields from data fit   & 0.7\\
    Selection efficiency & Simulation statistics        & 0.7\\
    Generator-level efficiency & Simulation statistics  & 0.3\\
    Hadronic interaction & Tracking prescription        & 0.3\\
    Tracking efficiency & Tracking prescription         & 0.5 \\
    \lone hadron trigger & Calibration samples          & 1.1 \\
    PID efficiency & Data-driven calculation            & 0.3 \\
    S-wave fractions & \lhcb angular analysis           & 3.1 \\
    Angular acceptance & \BdPhiKstar acceptance         & 1.1 \\
    $\BF(\PhiToKK)$ & PDG                               & 1.0 \\
%    $\BF(\KstarToKpi)$ & Isospin                        & $-$ \\
    \hline
    Sum of above in quadrature &                        & 3.9 \\
    \hline
    $f_s/f_d$ & \lhcb measurement                       & 5.8 \\
    $\BF(\BdPhiKstar)$ & PDG                            & 6.4 \\
    \hline
  \end{tabular}
  \label{tab:BsPhiPhi:BRsyst}
\end{table}

