\chapter{Alternative angular acceptance}
\label{ch:altacc}

As mentioned in Section~\ref{sec:BsphiKK:acceptance}, the angular acceptance is different between TIS and TOS trigger categories.
Since these categories are not mutually exclusive, there are two ways to split the events: TOS and not-TOS or TIS and not-TIS.
The former was chosen because it splits the data more evenly.
The effect of splitting by TIS and not-TIS is explored in this section.

The coefficients of the acceptance function, defined in Equation~\ref{eq:BsPhiKK:angacc}, are given in Table~\ref{tab:altacceptance}.
The 1D projections of the acceptance function are shown in Figure~\ref{fig:BsPhiKK:1DacceptanceTIS} for TIS events and Figure~\ref{fig:BsPhiKK:1DacceptancenotTIS} for not-TIS events.

A fit to data is performed with the best fit model (Model II) using this alternative trigger category splitting and these angular acceptance coefficients.
The fit projections are shown in Figure~\ref{fig:BsPhiKK:projectionsTIS}, and the deviation of each parameter from the default fit, in terms of the statistical error on that parameter ($\sigma$), is given in Table~\ref{tab:BsPhiKK:TISresults}.
Since none of the parameters deviate by as much as $\pm 1 \sigma$, the results are very consistent with the default fit.

\begin{table}[h]
  \centering
  \caption[Legendre moment coefficients.]{Values of the Legendre moment coefficients found using the \BsPhiKK phase-space simulation sample split by different trigger conditions.}
  \begin{tabular}{|l|r|r|}
    \hline
    $c^{ijkl}$ &                  TIS &              not-TIS \\
    \hline
    $c^{0000}$ & $ 0.0664 \pm 0.0002$ & $ 0.0638 \pm 0.0003$ \\
    $c^{0002}$ & $-0.0025 \pm 0.0005$ & ---                  \\
    $c^{0200}$ & $-0.0220 \pm 0.0010$ & ---                  \\
    $c^{0400}$ & $-0.0070 \pm 0.0014$ & $-0.0148 \pm 0.0018$ \\
    $c^{1000}$ & $-0.0144 \pm 0.0010$ & ---                  \\
    $c^{1200}$ & $-0.0140 \pm 0.0021$ & $-0.0133 \pm 0.0026$ \\
    \hline
  \end{tabular}
  \label{tab:altacceptance}
\end{table}

\begin{figure}
  \centering
  \includegraphics[width=\accfigwidth\textwidth]{body/BsPhiKK/figs/acceptance_mKK_proj_TIS}
  \includegraphics[width=\accfigwidth\textwidth]{body/BsPhiKK/figs/acceptance_phi_proj_TIS}\\
  \includegraphics[width=\accfigwidth\textwidth]{body/BsPhiKK/figs/acceptance_ctheta_1_proj_TIS}
  \includegraphics[width=\accfigwidth\textwidth]{body/BsPhiKK/figs/acceptance_ctheta_2_proj_TIS}
  \caption[1D projections of the acceptance function for TIS events]{1D projections of the acceptance function calculated for TIS events, overlaid onto the \BsPhiKK simulation sample.}
  \label{fig:BsPhiKK:1DacceptanceTIS}
\end{figure}
\begin{figure}
  \centering
  \includegraphics[width=\accfigwidth\textwidth]{body/BsPhiKK/figs/acceptance_mKK_proj_notTIS}
  \includegraphics[width=\accfigwidth\textwidth]{body/BsPhiKK/figs/acceptance_phi_proj_notTIS}\\
  \includegraphics[width=\accfigwidth\textwidth]{body/BsPhiKK/figs/acceptance_ctheta_1_proj_notTIS}
  \includegraphics[width=\accfigwidth\textwidth]{body/BsPhiKK/figs/acceptance_ctheta_2_proj_notTIS}
  \caption[1D projections of the acceptance function for non-TIS events]{1D projections of the acceptance function calculated for not-TIS events, overlaid onto the \BsPhiKK simulation sample.}
  \label{fig:BsPhiKK:1DacceptancenotTIS}
\end{figure}

\begin{table}
  \centering
  \caption[Deviations of the fit results from using the alternative trigger category splitting.]{Deviations of the fit results (in multiples of their statistical error) from using the alternative trigger category splitting.}
  \label{tab:BsPhiKK:TISresults}
  \begin{tabular}{|l|l|}
    \hline
    Fit parameter & $\sigma$ from default\\
    \hline
    \multicolumn{2}{|c|}{Breit-Wigner parameters}\\
    \hline
    $\phi(1020)$ $m$ [\mevcc]      & $\phantom{-}0.000 $ \\
    $\phi(1020)$ $\Gamma$ [\mev]   & $\phantom{-}0.006 $ \\
    \ftwop{1525} $m$ [\mevcc]      & $\phantom{-}0.030 $ \\
    \ftwop{1525} $\Gamma$ [\mev]   & $\phantom{-}0.006 $ \\
    \hline
    \multicolumn{2}{|c|}{Fit fractions}\\
    \hline
    $f_0(980)$         & $ -0.10 $ \\
    $\phi(1020)$       & $ -0.48 $ \\
    $f_0(1500)$        & $\phantom{-}0.14 $ \\
    $f_2^\prime(1525)$ & $\phantom{-}0.54 $ \\
    $\phi(1680)$       & $\phantom{-}0.03 $ \\
    nonresonant       & $\phantom{-}0.09 $ \\
    interference       & $ -0.07 $ \\
    \hline
    \multicolumn{2}{|c|}{Magnitudes}\\
    \hline
    $\phiz(1020)$ $|A_+|$    & $ -0.25  $ \\
    $\phiz(1020)$ $|A_0|$    & $\phantom{-}0.19  $ \\
    \ftwop{1525} $|A_+|$     & $ -0.13  $ \\
    \ftwop{1525} $|A_0|$     & $\phantom{-}0.20  $ \\
    $\phiz(1680)$ $|A_+|$    & $ -0.005 $ \\
    $\phiz(1680)$ $|A_0|$    & $\phantom{-}0.029 $ \\
    \hline
    \multicolumn{2}{|c|}{Phases [~rad~]}\\
    \hline
    nonresonant $\delta_0$  & $\phantom{-}0.03  $ \\
    \fzero{980} $\delta_0$   & $ -0.16  $ \\
    $\phiz(1020)$ $\delta_+$ & $\phantom{-}0.03  $ \\
    \fzero{1500} $\delta_0$  & $ -0.18  $ \\
    \ftwop{1525} $\delta_+$  & $\phantom{-}0.009 $ \\
    \ftwop{1525} $\delta_0$  & $ -0.05  $ \\
    \ftwop{1525} $\delta_-$  & $\phantom{-}0.004 $ \\
    $\phiz(1680)$ $\delta_+$ & $ -0.03  $ \\
    $\phiz(1680)$ $\delta_0$ & $\phantom{-}0.002 $ \\
    $\phiz(1680)$ $\delta_-$ & $ -0.007 $ \\
    \hline
  \end{tabular}
\end{table}

\begin{landscape}
\begin{figure}
  \centering
  \includegraphics[width=0.6\textwidth]{body/BsPhiKK/figs/TIS/Overlay_mKK_All_Data_PDF_0_pull.pdf}
  \includegraphics[width=0.6\textwidth]{body/BsPhiKK/figs/TIS/Overlay_phi_All_Data_PDF_0_pull.pdf}
  \includegraphics[width=0.6\textwidth]{body/BsPhiKK/figs/TIS/Overlay_ctheta_1_All_Data_PDF_0_pull.pdf}
  \includegraphics[width=0.6\textwidth]{body/BsPhiKK/figs/TIS/Overlay_ctheta_2_All_Data_PDF_0_pull.pdf}
  \caption[Fit projections of Model II using the alternative trigger category splitting.]{Fit projections of Model II using the alternative trigger category splitting in \mass{\Kp\Km} and the helicity angles, overlaid onto the data (black points).
                             The total PDF is a solid black line,
                             the $\phiz(1020)$ is a purple long-dashed line,
                             the \fzero{980} is a green long-dashed line,
                             the \ftwop{1525} is a brown long-dashed line,
                             the nonresonant component is a light blue short-dashed line,
                             the interference is a black dot-dashed line,
                             the \fzero{1500} and $\phiz(1680)$ are thin solid black lines,
                             and the background is a red histogram.}
  \label{fig:BsPhiKK:projectionsTIS}
\end{figure}
\end{landscape}

