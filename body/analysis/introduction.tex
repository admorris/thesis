\section{Introduction}
\label{sec:analysis:introduction}
This chapter outlines the event selection strategy common to both analyses presented in this thesis.
The selection of charmless \bquark hadron decays at \lhcb occurs in several steps.
First, to reduce combinatorial background, a cut-based selection imposes requirements on track quality, vertex quality,  kinematic and geometric variables.
To reduce backgrounds from misidentified particles, cuts are made on particle identification variables. Specific decay modes are vetoed by removing events, or imposing stricter requirements, in invariant mass windows under the appropriate mass hypothesis.
Finally, a multivariate algorithm is trained to discriminate between signal and combinatorial background.

\subsection{Kinematic variables}
%Transverse momentum
Transverse momentum, \pt, is the component of momentum perpendicular to the beam axis.
It is defined in Cartesian coordinates, with the beam axis corresponding to the $z$ axis, as
$$\pt = \sqrt{p_x^2 + p_y^2}.$$
Due to the large mass of \bquark hadrons, their decay products tend to have large transverse momentum.

%Pseudorapidity
Pseudorapidity is a measure of the polar angle, $\theta$, relative to the beam axis.
It is defined as $$\eta = -\ln \tan\left(\frac{\theta}{2}\right).$$

% Invariant mass
The invariant mass of multiple bodies (\eg the decay products of a \PB meson) can be found from the sums of their energy and momenta.
Cutting on invariant mass is useful for selecting specific parent or intermediate particles in a decay.

\subsection{Track and vertex quality variables}
\label{sec:analysis:trackvariables}

% track χ²
The final step of the track reconstruction, described in Section~\ref{sec:detector:trackreco}, is a Kalman-filter based fit \cite{Hulsbergen:2005pu}, which accounts for multiple scattering and corrects for energy losses due to ionisation.
The \chisq of this fit, known as the track fit \chisq, can be used as a measure of track quality.
% Ghost probability
As described in Section~\ref{sec:detector:trackreco}, the probability that a given track is a ghost is determined by an artificial neural network.
The output of this can be used to reduce the number background events.

% vertex χ²
A secondary vertex is reconstructed from a fit to several long tracks.
The \chisq of this fit can be used as a measure of vertex quality.
Signal decay candidates, where the tracks really do originate from the secondary vertex, tend to have a better vertex \chisq than combinatorial background events.

\subsection{Geometric variables}

Several geometric variables can be used to discriminate between signal and background.
The first two variables described in this section take advantage of the displaced secondary vertices typical of \PB decays to identify signal candidates.
The second two are measures of compatibility of a reconstructed particle with particular primary or secondary vertices.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.4\textwidth]{body/analysis/figs/IP}
  \caption[An illustration of impact parameter.]{Illustration of the impact parameter (length of the dotted line) of a track (solid line) in relation to the primary vertex (PV). The figure is drawn in the plane containing the track and the primary vertex.}
  \label{fig:analysis:IP}
\end{figure}

% impact parameter χ²
The impact parameter (IP) of a particle is the distance of closest approach of its extrapolated trajectory to the primary vertex.
This is illustrated as the length of the dotted line in Figure~\ref{fig:analysis:IP}.
The quantity $\chisq_\mathrm{IP}$ is commonly used in \lhcb analyses as a measure of significant displacement from the primary vertex.
It is defined as the difference in \chisq of the primary vertex reconstructed with and without the particle in question.
Particles originating from the primary vertex should have a small $\chisq_\mathrm{IP}$.

% flight distance χ²
The distance between a particle's creation and decay vertices, or flight distance (FD), is a variable used to identify displaced secondary vertices.
The quantity $\chisq_\mathrm{FD}$ can be used as a measure of the significance of the flight distance of a particle.
It is defined as the difference in \chisq of the creation vertex reconstructed with and without the particle in question.
Long-lived particles, such as \PB mesons, have a large $\chisq_\mathrm{FD}$.

% distance of closest approach χ²
The distance of closest approach (DOCA) between two tracks can be used as a measure of how compatible they are with originating from the same decay vertex.
The quantity $\chisq_\mathrm{DOCA}$ is the \chisq obtained from a vertex fit of the two tracks.
It is used in the selection of \decay{\phiz}{\Kp\Km} candidates.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.4\textwidth]{body/analysis/figs/DIRA}
  \caption[An illustration of the `DIRA' angle.]{The DIRA as defined as the angle between the $B$ momentum vector ($p_B$) and the displacement vector (red dotted line) from the primary vertex (PV) to the secondary vertex (SV).}
  \label{fig:analysis:DIRA}
\end{figure}

%DIRA
The `direction angle' (DIRA) is the angle between the momentum vector of a \PB candidate and the 3D displacement vector between the associated primary and secondary vertices.
This is illustrated in Figure~\ref{fig:analysis:DIRA}.
It is a measure of consistency of the \PB candidate with the reconstructed vertices.
The cosine of the DIRA is used to separate signal from combinatorial background; it should be close to one for true signal events.

\subsection{Particle identification variables}
\label{sec:analysis:pid}

% ProbNN
%https://indico.cern.ch/event/226062/contributions/475644/attachments/371741/517276/ANNPIDRetuning-Reco14-06052013.pdf
Artificial Neural Networks are used to assign a probability that a given track is a kaon (\prob{\PK}), a pion (\prob{\Ppi}) or a proton (\prob{\proton}) \cite{LHCb-DP-2014-002}.
The input variables used to train the Neural Network contain information from the \rich detectors, combined with information from the tracking system, muon detectors and the calorimeters.

% IsMuon 
%IsMuon definition from LHCb-PUB-2009-013: given a reconstructed track in the LHCb tracking system, hits in the Muon stations are searched around the track extrapolation in some Field of Interest (FOI). A boolean decision (IsMuon) is applied to tracks which satisfy the requirement to have at least one hit in FOI in a number of stations which depends on the momentum of the track. The stations required by the IsMuon decision are shown in Table 3 as a function of the track momentum.
A Boolean variable called `IsMuon' is used to remove kaons that decay in flight.
This variable is built by searching for hits in the muon detector corresponding to a reconstructed track in the vertex detector.
If there is at least one corresponding hit in a certain number of muon detector stations, then `IsMuon' is set to be true \cite{LHCb-PUB-2009-013}.

