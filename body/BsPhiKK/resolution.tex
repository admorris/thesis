\section{Resolution on \mass{\Kp\Km}}
\label{sec:BsphiKK:resolution}
%Introduction
When fitting the lineshapes of resonances with natural widths much larger than the mass resolution, it is sufficient to fit for the width, ignoring resolution effects.
Conversely, when fitting to resonances that are much narrower than the mass resolution, it is often sufficient to ignore the natural width and fit a Gaussian function with a width equal to the resolution.
In the case of resonances whose widths are of a similar order of magnitude to the mass resolution, as is the case with \decay{\phiz}{\Kp\Km} at \lhcb, then both the natural width and mass resolution must be accounted for.
This is done by convolving the lineshape with a Gaussian function.

%Method
The resolution is calculated using the simulation by taking the difference between the generated and simulated values of the $\Kp\Km$ invariant mass, $\Delta\mass{\Kp\Km}$, for each event in the \BsPhiKK simulation sample.
To account for the change of the resolution with \mass{\Kp\Km}, it is assumed that resolution scales with the square root of the energy release of the decay.
Hence, the function
\begin{equation}
  \sigma(m) = \sqrt{\sigma_0(m-2\mass{\Kp})}
  \label{eq:BsphiKK:resolution}
\end{equation}
is fitted to the distribution of $\Delta\mass{\Kp\Km}$ plotted in nine bins of \mass{\Kp\Km}.
This is shown in Figure~\ref{fig:BsphiKK:resolution}.
The fitted value of $\sigma_0$ is found to be $$\sigma_0 = (1.61 \pm 0.05)\times 10^{-5}\gevcc.$$


\begin{figure}[htb]
  \centering
  \includegraphics[width=0.6\textwidth]{body/BsPhiKK/figs/resolution}
  \caption[The mass-dependence of the $\mass{\Kp\Km}$ resolution.]{The mass-dependence of the $\mass{\Kp\Km}$ resolution as calculated using \BsPhiKK simulation. The red dotted line shows a fit using the function given in Equation~\ref{eq:BsphiKK:resolution}. The resolution is forced to be zero at threshold.}
  \label{fig:BsphiKK:resolution}
\end{figure}

The convolution can be expressed as the following integral
\[
  \int_{-\infty}^{+\infty} P_\mathrm{G} (m-x|0,\sigma(m)) \Gamma(m,\theta_1,\theta_2,\Phi) dx,
\]
where $P_\mathrm{G} (m-x|0,\sigma(m))$ is a Gaussian function with a mean of zero and a mass-dependent width given in Equation~\ref{eq:BsphiKK:resolution}, and $\Gamma(m,\theta_1,\theta_2,\Phi)$ is the time-integrated decay rate given in Equation~\ref{eq:theory:integrateddecayrate}.
In the amplitude fit, this integral is done numerically with 20 samples over a range $[-3\sigma(m),+3\sigma(m)]$.
The range of the integral and number of samples are chosen as a compromise between precision and computing time.

