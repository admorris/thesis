\subsection{Cut-based selection}
\label{sec:BsPhiKK:cuts}

The \decay{\Bs}{\phiz\rhoz} stripping line (Section~\ref{sec:analysis:stripping}) used in this analysis is designed to select the decay mode \decay{\Bs}{\phiz\pip\pim} without particle identification or invariant mass requirements on the pions, making it suitable for studies of \BsPhiKK decays.
One track pair is combined to form a \PhiToKK candidate, and the other a \RhoToPiPi candidate.
These are then combined to form a \Bs candidate.

In the stripping, charged particles are required to have a ghost probability less than 0.5 (later tightened to 0.3) and $\pt > 500\mevc$.
The quality of the track reconstruction is ensured by requiring that the track fit \chisqndf is less than 3.
The kaon tracks are required to have a log-likelihood of being a kaon greater than that of being a pion: $\Delta \ln(\mathcal{L}_{\PK-\Ppi})>0$, which is later imposed on all tracks.
In order to exclude tracks originating from the primary vertex, the $\chisq_{\rm{IP}}$ of the kaon candidates is required to be greater than 16, which is also later imposed on the pion candidates.
The \phiz and \rhoz candidates are both required to have $p_T>900\mevc$ and $p>1000\mevc$.
The tracks that form the \phiz candidate are required to have a distance of closest approach chi-squared, $\chisq_{\rm{DOCA}}$, less than 30.
Prompt \phiz mesons are removed by requiring $\chisq_{\rm{IP}}>10$, later tightened to 16, and a loose mass window of $|\mass{\Kp\Km}-\mass{\phiz}|<25\mevcc$ is applied, where \mass{\phiz} comes from Ref.~\cite{PDG2016}.
The vertex quality of the \phiz meson candidates is ensured by requiring $\chisq_{\rm{Vtx}}<25$.
To ensure equal treatment of the daughter mesons, the same $\chisq_{\rm{IP}}$ and $\chisq_{\rm{Vtx}}$ cuts are later applied to the \rhoz candidate.
The \rhoz candidate is also required to have $\mass{\pip\pim}<4000\mevcc$.
Finally, the \Bs candidate is required to lie in the mass range $4800<\mass{\Kp\Km \pip\pim}<5600\mevcc$ and to have a vertex $\chisq$ per degree-of-freedom, $\chisq_{\rm{Vtx}}/\rm{ndf}$, less than 9 and $\chisq_{\rm{IP}}<20$ to ensure that it originates from the primary vertex.

After the stripping step, the candidates are reconstructed under the $\Kp\Km\Kp\Km$ mass hypothesis.
In order to remove kaons that have decayed in flight, particles that have matching muon information are rejected.
To suppress background from misidentified hadrons, cuts are made on the probability, \prob{\Ph}, for the particle to be a certain hadron, $h$, as described in Section~\ref{sec:analysis:introduction}.
Kaon candidates are required to have $\prob{\PK}\times(1-\prob{\pi})>0.025$, in order to suppress misidentified pions, and $\prob{\PK}\times(1-\prob{\proton})>0.01$ in order to suppress misidentified protons.
These two cuts retain $(\SelEffBsphiKKMCnocutKpiPID)\%$ and $(\SelEffBsphiKKMCnocutKpPIDtwodp\pm0.02)\,\%$ of the \BsPhiKK simulation events, respectively.

The invariant mass cut on the candidate $\PhiToKK$ decays is tightened to $|\mass{\Kp\Km}-\mass{\phiz}|<15\mevcc$ to agree with previous LHCb analyses of \BsPhiPhi~\cite{LHCb-PAPER-2013-007, LHCb-PAPER-2012-004, LHCb-PAPER-2014-026, LHCb-PAPER-2015-028}.
To ensure significant displacement from the primary vertex, the \Bs candidates are also required to have a flight distance \chisq, $\chisq_{\rm{FD}}$, greater than 250.
Where events have multiple candidates, they are ordered in terms of $\chisq_{\rm{Vtx}}$.
The first candidate is kept, along with any others that do not share tracks.
The above cuts are summarised in Table \ref{tab:BsPhiKK:seleff}, along with the fraction of \BsPhiKK and \BsPhiPhi simulation events that satisfy each condition.

\begin{table}[h]
  \begin{center}
    \caption[Selection efficiencies.]{Efficiencies of the offline selection cuts on signal simulation events and upper sideband data events with $\mass{\PhiKK}>5500\mevcc$ after applying the stripping and trigger requirements.
             The simulation samples have additional cuts on the background category, as described in the text.}
    \label{tab:BsPhiKK:seleff}
    \begin{tabular}{|l|c|c|c|}
    \hline
    & \multicolumn{3}{c|}{Fraction of events kept [$\%$]} \\
    \multicolumn{1}{|c|}{\raisebox{1.5ex}[1.5ex]{Cut}}     & 
    \multicolumn{1}{c}{\BsPhiKK}  &
    \multicolumn{1}{c}{\BsPhiPhi} &
    \multicolumn{1}{c|}{Sideband} \\ \hline
    $\prob{}(\rm{ghost})<0.3$                         & \SelEffBsphiKKMCnocutGhostProb     & \SelEffBsphiphiMCnocutGhostProb     & \SelEffBsphiKKsidebandnocutGhostProb     \\
    Remove decay-in-flight \Kp                        & \SelEffBsphiKKMCnocutisMuon        & \SelEffBsphiphiMCnocutisMuon        & \SelEffBsphiKKsidebandnocutisMuon        \\
    \Kp $p_T>500\mevc$                                & \SelEffBsphiKKMCnocutPT            & \SelEffBsphiphiMCnocutPT            & \SelEffBsphiKKsidebandnocutPT            \\
    $\mass{\phiz}\pm15\mevcc$            & \SelEffBsphiKKMCnocutPhiWindow     & \SelEffBsphiphiMCnocutPhiWindow     & \SelEffBsphiKKsidebandnocutPhiWindow     \\
    $\chisq_{\rm{FD}}>250$                        & \SelEffBsphiKKMCnocutBsFDchisq     & \SelEffBsphiphiMCnocutBsFDchisq     & \SelEffBsphiKKsidebandnocutBsFDchisq     \\
%    $\chisq_{\rm{IP}}<20$                         & \SelEffBsphiKKMCnocutBsIPchisq     & \SelEffBsphiphiMCnocutBsIPchisq     & \SelEffBsphiKKsidebandnocutBsIPchisq     \\
    $\prob{\PK}\times(1-\prob{\pi})>0.025$   & \SelEffBsphiKKMCnocutKpiPID        & \SelEffBsphiphiMCnocutKpiPID        & \SelEffBsphiKKsidebandnocutKpiPID        \\
    $\prob{\PK}\times(1-\prob{\proton})>0.01$ & \SelEffBsphiKKMCnocutKpPID         & \SelEffBsphiphiMCnocutKpPID         & \SelEffBsphiKKsidebandnocutKpPID         \\ \hline
    Overall cut efficiency               & \SelEffBsphiKKMCnocuttotal         & \SelEffBsphiphiMCnocuttotal         & \SelEffBsphiKKsidebandnocuttotal         \\ \hline
    \end{tabular}
  \end{center}
\end{table}

The simulation samples are subject to additional background category requirements~\cite{Gligorov:1035682}, which use the `truth' information from the generator.
For the signal simulation samples, events are required to correspond to true signal or signal plus radiative photons, which allows for proper modelling of the radiative tail in the \mass{\Kp\Km \Kp\Km} distribution.
For the background simulation samples, the events are required to correspond to misidentified background, which removes any combinatorial component.

\FloatBarrier
