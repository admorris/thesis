\subsection{Multivariate selection}
\label{sec:BsPhiKK:mva}

After the triggers, stripping, offline cuts and mass vetoes, multivariate analysis (MVA) algorithms are trained to further reduce the combinatorial background.
Four different MVA algorithms from the TMVA package~\cite{Höcker:1019880} were tested in order to find the best background rejection and signal efficiency.
These are three types of Boosted Decision Tree with adaptive boosting (BDT), gradient boosting (BDTG), decorrelation and adaptive boosting (BDTD) respectively, and a Multilayer Perceptron (MLP), a type of artificial neural network.

Seven discriminating variables are used to train the MVA algorithms: the minimum values of $p_{T}$ and $\ln\chisq_{\rm{IP}}$ out of the kaons and the $p_T$, $\eta$, $\ln\chisq_{\rm{IP}}$, $\ln\chisq_{\rm{FD}}$ and $\ln\chisq_{\rm{Vtx}}$ for the \Bs candidate.
Figure~\ref{fig:BsPhiKK:mvaVars} shows the distributions of these variables in the simulation and data weighted using the \sPlot technique~\cite{Pivk:2004ty} (s-weighted data).
The \sPlot technique is described in Section~\ref{sec:BsPhiPhi:mva}.
It can be seen that the agreement is reasonable: the discriminating variables need not agree perfectly between data and simulation in order for the classifier to distinguish signal from background.

\def\mvafigwidth{0.45}
\begin{figure}
  \begin{center}
    \subfloat[\Bs $\ln\chisq_{\rm{FD}}$]{\includegraphics[width=\mvafigwidth\textwidth]{body/BsPhiKK/figs/mvaVar_B_s0_ln_FDCHI2}}
    \subfloat[\Bs $\ln\chisq_{\rm{IP}}$]{\includegraphics[width=\mvafigwidth\textwidth]{body/BsPhiKK/figs/mvaVar_B_s0_ln_IPCHI2}}\\
    \subfloat[\Bs $\ln\chisq_{\rm{Vtx}}$]{\includegraphics[width=\mvafigwidth\textwidth]{body/BsPhiKK/figs/mvaVar_B_s0_ln_EVCHI2}}
    \subfloat[\Bs \pt]{\includegraphics[width=\mvafigwidth\textwidth]{body/BsPhiKK/figs/mvaVar_B_s0_PT_fiveGeV}}\\
    \subfloat[\Bs $\eta$]{\includegraphics[width=\mvafigwidth\textwidth]{body/BsPhiKK/figs/mvaVar_B_s0_Eta}}
    \subfloat[min. \Kpm \pt]{\includegraphics[width=\mvafigwidth\textwidth]{body/BsPhiKK/figs/mvaVar_minK_PT_GeV}}\\
    \subfloat[min. \Kpm $\ln\chisq_{\rm{IP}}$]{\includegraphics[width=\mvafigwidth\textwidth]{body/BsPhiKK/figs/mvaVar_minK_ln_IPCHI2}}
    \caption[Comparison of the discriminating variables between simulation and data.]{Comparison of the discriminating variables between \BsPhiKK simulation (yellow histogram) and s-weighted data (black points). The distributions are normalised to unit area.}
    \label{fig:BsPhiKK:mvaVars}
  \end{center}
\end{figure}

The decay \BsPhiPhi is expected to be the dominant contribution to the \BsPhiKK final state in the region $\mass{\Kp\Km}<1800\mevcc$.
This decay mode is already well studied~\cite{Acosta:2005eu,Aaltonen:2011rs,LHCb-PAPER-2012-004,LHCb-PAPER-2013-007,LHCb-PAPER-2014-026,LHCb-PAPER-2015-028}.
Hence this analysis focuses on the other resonant components, such as \BsPhiftwop.
The signal and background samples used in the training and optimisation have a cut of $1050<\mass{\Kp\Km}<1800\mevcc$.
The background sample consists of the upper sideband data in the range $\mass{\Kp\Km \Kp\Km} > 5500\mevcc$.
Figure \ref{fig:BsPhiKK:variables_id} shows the distributions of the discriminating variables in the signal and background training samples.
It can be seen from the differences between the signal and background distributions that each of the discriminating variables has some separation power.
Figure \ref{fig:BsPhiKK:CorrelationMatrices} shows the correlation matrices of the discriminating variables in the signal and background training samples.
The \pt and $\eta$ variables are (anti)correlated to some extent, as are the minimum kaon $\ln\chisq_{\rm{IP}}$ and the \Bs $\ln\chisq_{\rm{FD}}$.

\begin{figure}
  \begin{center}
    \subfloat[\Bs $\ln\chisq_{\rm{FD}}$]{\includegraphics[width=\mvafigwidth\textwidth]{body/BsPhiKK/figs/variables_id_c0}}
    \subfloat[\Bs $\ln\chisq_{\rm{IP}}$]{\includegraphics[width=\mvafigwidth\textwidth]{body/BsPhiKK/figs/variables_id_c1}}\\
    \subfloat[\Bs $\ln\chisq_{\rm{Vtx}}$]{\includegraphics[width=\mvafigwidth\textwidth]{body/BsPhiKK/figs/variables_id_c2}}
    \subfloat[\Bs \pt]{\includegraphics[width=\mvafigwidth\textwidth]{body/BsPhiKK/figs/variables_id_c3}}\\
    \subfloat[\Bs $\eta$]{\includegraphics[width=\mvafigwidth\textwidth]{body/BsPhiKK/figs/variables_id_c4}}
    \subfloat[min. \Kpm \pt]{\includegraphics[width=\mvafigwidth\textwidth]{body/BsPhiKK/figs/variables_id_c5}}\\
    \subfloat[min. \Kpm $\ln\chisq_{\rm{IP}}$]{\includegraphics[width=\mvafigwidth\textwidth]{body/BsPhiKK/figs/variables_id_c6}}
    \caption[Comparison of the discriminating variables in the signal and background samples used to train the MVA.]{Comparison of the discriminating variables in the signal (blue) and background (red) samples used to train the MVA. The distributions are normalised to unit area.}
    \label{fig:BsPhiKK:variables_id}
  \end{center}
\end{figure}

\begin{figure}
  \begin{center}
    \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/CorrelationMatrixS}
    \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/CorrelationMatrixB}
    \caption[Correlation matrices of the discriminating variables in the signal and background samples.]{Correlation matrices of the discriminating variables in the signal (left) and background (right) samples.}
    \label{fig:BsPhiKK:CorrelationMatrices}
  \end{center}
\end{figure}

Figure~\ref{fig:BsPhiKK:rejBvsS} shows the background rejection versus signal efficiency (ROC curve) of the different MVA algorithm types trained.
The MLP algorithm gives the best background rejection for all values of signal efficiency.
Figure~\ref{fig:BsPhiKK:overtrain} shows the distribution of the MVA outputs for the MLP algorithm, with the training samples overlaid on top of the test samples.
The probabilities returned by the Kolmogorov--Smirnov test~\cite{smirnov1948,ajams2013112} are reasonable and suggest no overtraining.
A comparison of the distributions of the MLP output in simulation and s-weighted data is shown in Figure~\ref{fig:BsPhiKK:mvaoutput}, which shows good agreement despite the imperfect agreement of the training variables shown in Figure~\ref{fig:BsPhiKK:mvaVars}.
Supplementary plots from the MVA training are presented in Appendix~\ref{sec:mvaplots}.

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.7\textwidth]{body/BsPhiKK/figs/rejBvsS}
    \caption{ROC curves for the different MVA types.}
    \label{fig:BsPhiKK:rejBvsS}
  \end{center}
\end{figure}

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.7\textwidth]{body/BsPhiKK/figs/overtrain_MLP}
    \caption[Distribution of the MVA output for the signal and background samples.]{Distribution of the MVA output for the signal and background samples. The test samples are plotted as histograms, and the training samples are plotted as points.}
    \label{fig:BsPhiKK:overtrain}
  \end{center}
\end{figure}

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.7\textwidth]{body/BsPhiKK/figs/mvaOutput_mlp}
    \caption[Comparison of MLP output between simulation and data.]{Comparison of MLP output between \BsPhiKK simulation (yellow histogram) and s-weighted data (black points). The distributions are normalised to unit area.}
    \label{fig:BsPhiKK:mvaoutput}
  \end{center}
\end{figure}
\FloatBarrier

In order to choose the optimal cut on the MVA output, the signal significance figure of merit, defined as
\[
  FOM = \frac{S}{\sqrt{S+B}},
\]
is maximised, where $S = S_{0}\cdot\varepsilon_{S}$ is the product of the initial signal yield, $S_{0}$, in data and the cut efficiency, $\varepsilon_{S}$, on the simulation sample; similarly, $B = B_{0}\cdot\varepsilon_{B}$ is the product of the initial background yield, $B_{0}$ in data and the cut efficiency, $\varepsilon_{B}$ on the sideband sample.

\begin{figure}[h]
  \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/initialmassfit10501800_MC}
  \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/initialmassfit10501800}
  \caption[Fits to the \mass{\Kp\Km \Kp\Km} distributions of the simulation and data with $1.05<\mass{\Kp\Km}<1.80\gevcc$.]{Fits to the \mass{\Kp\Km \Kp\Km} distributions of the \BsPhiKK simulation (left) and data (right) with $1.05<\mass{\Kp\Km}<1.80\gevcc$.
           The \Bs signal component is the blue long-dashed line, and the combinatorial background component is the purple dotted line.
           Vertical black dotted lines denote a window of $\pm 3\sigma$ around the mean of the \Bs peak.}
  \label{fig:BsPhiKK:initialmassfit10501800}
\end{figure}

To determine these initial yields, fits are performed to the \mass{\Kp\Km \Kp\Km} distribution of \BsPhiKK simulation and data with the cut-based selection applied and an additional cut of $1.05<\mass{\Kp\Km}<1.80\gevcc$.
The \Bs signal component is modelled as a sum of a Crystal Ball function plus two Gaussian functions with a shared mean.
The Crystal Ball tail parameter $n$ is fixed to 1.
The shape parameters of the signal component in the fit to data are fixed to the results of the fit to simulation.
The combinatorial background component is modelled as an exponential function.
These fits are shown in Figure \ref{fig:BsPhiKK:initialmassfit10501800}.
The values of $S_{0}$ and $B_{0}$ are found by integrating the signal and background PDFs in a window $\pm 3 \sigma$ around the \Bs mass, which is marked by black vertical dashed lines.
This gives $S_{0}=\initialmassfitABSignalNthreesigma$ and $B_{0}=\initialmassfitABCombinatorialNthreesigma$.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.6\textwidth]{body/BsPhiKK/figs/BsphiKK_MC_mlpvsmKK}
  \caption[Efficiency of the optimal cut on the MLP output as a function of \mass{\Kp\Km}.]{Efficiency of the optimal cut on the MLP output as a function of \mass{\Kp\Km} using the \BsPhiKK simulation. Note that the vertical axis starts from $80\,\%$. A fit using a constant function is superimposed.}
  \label{fig:BsPhiKK:mlpeffvsmKK}
\end{figure}

The MLP algorithm gives the highest value of significance and is chosen to be used for the rest of this analysis.
The efficiency and significance curves for all four algorithms are shown in Figure~\ref{fig:mvaplots:mvaeffs} in Appendix~\ref{sec:mvaplots}.
The optimal cut on the MLP output is at a value of \MLPoptcuttwodp.
The efficiencies of this cut on the \BsPhiKK and \BsPhiPhi simulation samples are $(\BsphiKKMCmvacuteff)\,\%$ and $(\BsphiphiMCmvacuteff)\,\%$, respectively.
Using the yields of fits made to the data sample with $|\mass{\Kp\Km}-\mass{\phiz}|<15\mevcc$, the efficiency of this cut on the signal is determined to be $(95.6 \pm 2.9)\,\%$, which is in good agreement with the simulation.
The rejection rate on the sideband sample is $(\sidebandmvacutrej)\,\%$.
As a further check, the efficiency of this cut as a function of \mass{\Kp\Km} for the \BsPhiKK simulation sample is shown in Figure~\ref{fig:BsPhiKK:mlpeffvsmKK}.
It can be seen that the cut does not bias the distribution.

\FloatBarrier

