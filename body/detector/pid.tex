\section{Particle identification}
\label{sec:detector:pid}

In this section, the particle identification (PID) system of \lhcb is described.
Ring-imaging Cherenkov detectors are employed to identify long-lived charged hadrons: namely pions, kaons and protons.
The calorimetry system is used to identify and measure the energy of photons, electrons and hadrons.
The muon detector system is used to identify and measure the momentum of muons.

\subsection{Ring imaging Cherenkov detectors}
\label{sec:detector:rich}

%Introduction
\lhcb contains two \rich detectors~\cite{LHCb-TDR-003} for the purpose of hadron identification.
These detectors are optimised for distinguishing between protons, charged pions and charged kaons.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.5\textwidth]{body/detector/figs/RICH1_schematic}
  \includegraphics[width=0.45\textwidth]{body/detector/figs/RICH2_schematic}
  \caption[The \rich detectors.]{Left: side view of \richone, illustrating the path of Cherenkov light.
           Right: top-down view of \richtwo. \cite{LHCb-TDR-003}}
  \label{fig:detector:riches}
\end{figure}

% Principles of RICH detectors
Cherenkov radiation~\cite{Cherenkov} is emitted when a charged particle passes through a dielectric medium, known as a `radiator', with a speed, $\beta$, greater than $1/n$, where $n$ is the refractive index of the material.
The light is emitted at a constant polar angle from the trajectory of the particle.
This angle, known as the Cherenkov angle, $\theta_c$, is a function of $n$ and $\beta$:
\begin{equation}
\cos\theta_c = \frac{1}{n\beta}.
\end{equation}

A \rich detector employs spherical mirrors to focus the light into rings.
The radius of a Cherenkov ring is the product of $\theta_c$ and the focal length of the mirror.
Measurement of the radius of these rings gives information on the velocity of the associated particle.
When the rings are matched to tracks, which provide momentum information, it is possible to infer the mass of the particle.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.45\textwidth]{body/detector/figs/RICH_radiator_angles}
  \includegraphics[width=0.5\textwidth]{body/detector/figs/RICH_radiator_angles_data}
  \caption[Cherenkov angle as a function of momentum.]{Left: Cherenkov angle as a function of momentum for various charged particle species in each of the \rich radiators.
           Right: reconstructed Cherenkov angle from isolated tracks in \cfourften as a function of track momentum, using data from \richone \cite{LHCb-DP-2012-003}.}
  \label{fig:detector:rich_radiator_angles}
\end{figure}

%Stuff common to the LHCb RICH detectors
The \lhcb \rich detectors, shown in Figure~\ref{fig:detector:riches}, have gas-tight volumes filled with fluorocarbon gas radiators.
There are two detectors, separated by the \ttracker, \intr/\ot tracking stations and the magnet.
They have different angular coverages and cover different momentum ranges.
Figure~\ref{fig:detector:rich_radiator_angles} shows the relation between Cherenkov angle and momentum for various particle species in each of the radiators used in the \lhcb \rich detectors.

%RICH1
The \richone detector, shown on the left of Figure~\ref{fig:detector:riches}, is positioned between the \velo and the \ttracker.
During Run 1, it used both silica aerogel and \cfourften gas as Cherenkov radiators.
The aerogel was removed prior to Run 2.
\richone has an angular coverage from 25\mrad to 250\mrad in the vertical plane and 300\mrad in the horizontal.
It is designed to cover the low-momentum range from 2--60\gevc.

%RICH2
The \richtwo detector, shown on the right of Figure~\ref{fig:detector:riches}, is located after the third tracking station and before the first \muondetector plane.
It uses \cffour gas as its Cherenkov radiator.
The angular coverage extends from 15\mrad to 100\mrad in the vertical plane and 120\mrad in the horizontal.
This allows the detector to cover the momentum range from 15--100\gevc.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.45\textwidth]{body/detector/figs/RICH_HPD}
  \caption[A hybrid photon detector.]{Schematic of an \hpd \cite{LHCb-TDR-003}.}
  \label{fig:detector:hpd}
\end{figure}

Hybrid Photon Detectors (\hpd{}s) \cite{Eisenhardt:2008zz} are used to detect the Cherenkov light.
A diagram of an \hpd can be seen in Figure~\ref{fig:detector:hpd}.
\hpd{}s are vacuum phototubes with silicon pixel sensors placed at the anode end.
The entrance window is a spherically shaped piece of quartz, with the inner surface coated with a multi-alkali photocathode.
When Cherenkov photons with a wavelength in the range 200--600\nm strike the photocathode, photoelectrons are released and accelerated by electrostatic fields to the pixel sensor, which produces an average of one electron--hole pair per 3.6\ev of deposited energy.
The operating voltage of the \hpd{}s is {$-20$\,kV}, corresponding to around 5000 electron--hole pairs per photoelectron.
The \rich detectors are enclosed in iron boxes to mitigate the effects of the field produced by the magnet.
Furthermore, the \hpd{}s are housed in individual cylinders of mu-metal\footnote{A nickel--iron alloy with high magnetic susceptibility.} within an external box of iron.
The magnetic shielding allows the \hpd{}s to operate up to a magnetic field strength of {50\,mT}, well above the maximum field strength during operation ({30\,mT} at \richone).

%\subsection{Hadron identification performance}

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.6\textwidth]{body/detector/figs/RICH_kaon_performance_data}
  \caption[\rich performance.]{Kaon identification efficiency (red) and pion-as-kaon misidentification rate (black) as a function of track momentum for two different cuts on $\Delta\log\mathcal{L}\left(\PK-\Ppi\right)$ \cite{LHCb-DP-2012-003}.}
  \label{fig:detector:richkaon}
\end{figure}

The performance of the \rich detectors is measured using control samples of \decay{\KS}{\pip\pim}, \decay{\Lz}{\proton\pim} and \decay{\Dstarp}{\decay{\Dz(}{\Km\pip)}\pip} decays, which were selected using only kinematic requirements \cite{LHCb-DP-2012-003}.
With these, the pion, proton and kaon identification efficiencies and misidentification rates were calculated for a range of track momenta.
Figure~\ref{fig:detector:richkaon} shows the kaon identification efficiency and \decay{\pi}{\PK} misidentification rate for two different PID requirements.
In the momentum range between 2\gevc and 100\gevc, for a requirement that the likelihood of the track being a kaon is larger than it being a pion, the kaon identification efficiency is 95\,\% with a pion misidentification rate of 10\,\%.

\subsection{Calorimeters}
\label{sec:detector:calo}

% General description
The calorimetry system~\cite{LHCb-TDR-002} is located between the first and second muon detector stations.
Its purpose is to discriminate between electrons, photons and hadrons, as well as provide measurements of their energy and position.
It also plays an important role in providing transverse energy measurements for the hardware-level trigger described in Section~\ref{sec:detector:lzero}.
\lhcb adopts the conventional approach of having an electromagnetic calorimeter (\ecal) followed by a hadronic calorimeter (\hcal).
Two additional detectors, the Scintillator Pad Detector (\spd) and Preshower Detector (\presh) are placed in front of the \ecal in order to help reduce the background for the hardware-level electron trigger caused by the large numbers of pions.
In order to fully contain showers from high energy photons, the thickness of the \ecal is 25 radiation lengths, on top of the 2.5 of the \spd/\presh.
The \hcal thickness is 5.6 interaction lengths as a trade-off between shower containment and available space.
The calorimeters have an angular coverage from 25\mrad to 250\mrad in the vertical plane and 300\mrad in the horizontal, matching that of the \richone.
Since the hit density close to the beam pipe is about two orders of magnitude larger than at the edge of the acceptance, each of the subdetectors in the calorimetry system has variable segmentation, shown in Figure~\ref{fig:detector:calo_segmentation}, with smaller cells located in the inner region.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.49\textwidth]{body/detector/figs/ECAL_segmentation}
  \includegraphics[width=0.49\textwidth]{body/detector/figs/HCAL_segmentation}
  \caption[Segmentation of the calorimeters.]{Segmentation of (left) the \spd, \presh and \ecal and (right) the \hcal \cite{LHCb-TDR-002}.}
  \label{fig:detector:calo_segmentation}
\end{figure}

% Principle
Both the \ecal and \hcal are sampling calorimeters, constructed from alternating layers of absorber and detector material.
The absorber material is lead in the \ecal and iron in the \hcal.
The detector material is a plastic scintillator made from doped polystyrene.
Particles interact with the absorber material, generating showers of secondary particles, which in turn induce scintillation light in the detector material.
This light is transmitted through wavelength-shifting (WLS) fibres to photomultipliers (PMTs).
The \spd/\presh cells are read out using multi-anode photomultipliers (MaPMTs) mounted outside the acceptance of the detector inside magnetic shielding.
The \ecal and \hcal cells are read out by individual PMTs mounted directly on the detector modules.

\begin{figure}[htb]
  \centering
  \includegraphics[width=\textwidth]{body/detector/figs/SPDPS_modules}
  \caption[The inner and outer \spd/\presh pads.]{Diagrams of (left) inner and (right) outer \spd/\presh pads, showing the routing of the WLS fibres \cite{LHCb-TDR-002}.}
  \label{fig:detector:spdps_modules}
\end{figure}

% SPD/PS
The \spd/\presh detector consists of two near-identical planes of rectangular scintillator pads, with a 15\mm layer of lead sandwiched between them.
The scintillator pads are illustrated in Figure~\ref{fig:detector:spdps_modules}.
The segmentation of the detector in $\theta$ and $\phi$ is chosen to exactly match that of the \ecal.
The \spd detects charged particles and is used in conjunction with the \ecal to discriminate between electrons and neutral particles such as \piz.
The \presh, located after the layer of lead, helps distinguish between electrons and charged hadrons, such as \pipm, as the latter deposit less energy than the former.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.8\textwidth]{body/detector/figs/ECAL_module}
  \caption[An inner \ecal module.]{An inner \ecal module \cite{LHCb-TDR-002}. The nine fibre bundles can be seen on the left.}
  \label{fig:detector:ecal_module}
\end{figure}

% ECAL
The \ecal consists of modules built from alternating layers of lead, scintillator tiles and reflecting TYVEK paper\footnote{TYVEK is a brand of synthetic paper manufactured by DuPont.}.
An \ecal module is shown in Figure~\ref{fig:detector:ecal_module}.
Modules in the inner region are divided into 9 cells; those in the middle region are divided into 4 cells, and the outer modules are a single cell.
Light is collected from the scintillator tiles by WLS fibres, which traverse the entire module.
The fibres from each cell are bundled together at the back end of each module and feed light into one PMT per bundle.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.8\textwidth]{body/detector/figs/HCAL_module_zoom}
  \caption[An \hcal module.]{Cut-away view of several outer \hcal submodules assembled within a full module \cite{LHCb-TDR-002}.}
  \label{fig:detector:hcal_module}
\end{figure}

% HCAL
The \hcal is made from layers of iron absorber and scintillating tiles.
Unlike most calorimeters, the tiles run parallel to the beam axis.
The tiles are interspersed laterally by 10\mm of iron, whereas the length of the tiles and iron spacers corresponds to the hadronic interaction length in steel.
A module is composed of 216 identical periods with a thickness of 20\mm.
Part of an \hcal module is shown in Figure~\ref{fig:detector:hcal_module}.
One period consists of an absorber structure, made from laminated steel plates, with gaps where the scintillator tiles are inserted.
As with the \ecal, scintillator light is collected by WLS fibres and delivered to PMTs at the back of each module.
Cells are defined as groups of tiles whose fibres are bundled together.
The cells in the inner region are a quarter of the size of those in the outer region.


% Herschel?

\subsection{Muon detectors}
\label{sec:detector:muon}

The muon detection system (\muondetector)~\cite{LHCb-TDR-004}, illustrated in Figure~\ref{fig:detector:muon}, consists of five stations (M1--M5) of detectors.
Station M1 is located between \richtwo and the calorimeters and is used to improve the measurement of transverse momentum for the trigger.
The stations M2 to M5 are located immediately after the \hcal.
Iron absorbers with a thickness of 80\cm are placed between each detector station after M2.
The minimum momentum for a muon to traverse the entire \lhcb detector to M5 is approximately 6\gevc.
The total absorber thickness is about 20 interaction lengths.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.4\textwidth]{body/detector/figs/MUON_schematic}
  \includegraphics[width=0.5\textwidth]{body/detector/figs/MUON_segmentation_2}
  \caption[The muon stations.]{Left: side view of the muon system.
           Right: segmentation of the muon stations. \cite{LHCb-TDR-004}}
  \label{fig:detector:muon}
\end{figure}

The detectors provide spatial point measurements for charged tracks.
The first three stations have good horizontal spatial resolution, translating to a transverse momentum resolution of $20\,\%$. The transverse momentum is determined from the slope of the muon track in the horizontal plane, using knowledge of the \pt kick of the magnet and assuming that the particle comes from the primary vertex.
The other two stations have poor spatial resolution, and are used to identify penetrating particles.

The \muondetector stations are segmented into pads, each providing binary hit information to the trigger and DAQ.
Since the hit density is much higher in the inner region, the stations are divided into four concentric regions from inside to out, with the inner regions being more finely segmented.
The segmentation is finer in the horizontal (bending) direction than in the vertical, as illustrated on the right of Figure~\ref{fig:detector:muon}, since this is more important for measuring transverse momentum.
The stations are scaled transversely in proportion to their distance from the beam crossing point.
For all except the inner-most region of M1, the pads are composed of multi-wire proportional counters (\mwpc{}s).
The remaining pads are triple gas electron multipliers (\gem{}s).

