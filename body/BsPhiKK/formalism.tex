\section{Amplitude formalism}
\label{sec:BsPhiKK:formalism}

The amplitude of the \BsPhiKK decay in the absence of $\Bs-\Bsb$ mixing (or at $t=0$) can be expressed as
\begin{equation}
\label{eq:amplitude}
  A(m, \Phi, \theta_1, \theta_2) = \sum\limits_{\text{resonances}} \left(\frac{p}{m_B}\right)^{L_B} \left(\frac{q}{m_0}\right)^{J} B_{L_B}^\prime(p) B_{J}^\prime(q) T(m) F(\Phi, \theta_1, \theta_2) \sqrt{pq}
\end{equation}
where $m$ is the invariant mass of the $\Kp\Km$ pair, $m_0$ and $J$ are the mass and spin of the $\Kp\Km$ resonance, $m_B$ is the pole mass of the \Bs meson, $L_B$ is the minimum angular momentum between the \phiz and $\Kp\Km$ resonance, $p$ is the momentum of the $\Kp\Km$ resonance in the \Bs decay frame, $q$ is the momentum of a kaon in the $\Kp\Km$ resonance decay frame, $T$ is the resonance lineshape (see Section~\ref{sec:BsPhiKK:lineshapes}), $F$ is the angular distribution (see Section~\ref{sec:theory:kinematics}), $\Phi$, $\theta_1$ and $\theta_2$ are the helicity angles, and the functions $B^\prime$ are the Blatt--Weisskopf barrier factors \cite{Blatt-Weisskopf-1979,VonHippel:1972fg}.
The first three $B^\prime$ functions are:
\begin{eqnarray}
B_{0}^\prime(p) = 1, \\
B_{1}^\prime(p) = \sqrt{\frac{1+(p_0 r)^2}{1+(p r)^2}}, \\
B_{2}^\prime(p) = \sqrt{\frac{9+3(p_0 r)^2+(p_0 r)^4}{9+3(p r)^2+(p r)^4}},
\end{eqnarray}
where $r$ is a parameter that characterises the interaction radius of the hadrons in the decay and $p_0$ is the momentum calculated using the pole mass of the appropriate resonance.
The values of these radii in the amplitude fit to data are chosen to be $r=1.0\invgevc$ for the \Bs meson and $r=3.0\invgevc$ for the $\Kp\Km$ resonance.
These values are also used in EvtGen~\cite{Lange:2001uf} when generating the simulation samples.
These are later varied to obtain a systematic uncertainty.

As discussed in Section~\ref{sec:theory:mixing}, the amplitude in Equation~\ref{eq:amplitude}, $A(0)$ and its \CP conjugate, $\overline{A}(0)$, are converted to a time-integrated decay rate using \cite{Datta:2007yk}
\begin{equation}
  \begin{array}{rcl}
    \int^{\infty}_{0} \Gamma(t) + \overline{\Gamma}(t) dt &=& \frac{1}{2}\left(\left|A(0)\right|^2+\left|\overline{A}(0)\right|^2\right)\left(\frac{1}{\Gamma_L}+\frac{1}{\Gamma_H}\right)\\
    &+&\Re\left(\overline{A}(0)A(0)^*\right) \left(\frac{1}{\Gamma_L}-\frac{1}{\Gamma_H}\right),
  \end{array}
  \tag{\ref{eq:theory:integrateddecayrate}}
\end{equation}
which assumes equal numbers of \Bs and \Bsb mesons.
The values of $\Gamma_H$ and $\Gamma_L$ are calculated from the PDG average values of the \Bs mass eigenstate lifetimes: $\tau_H = 1.661 \pm 0.032 \ps$ and $\tau_L = 1.045 \pm 0.025 \ps$~\cite{PDG2016}.

Since the helicity angles $\theta_1$ and $\theta_2$ are calculated relative to the \Kp mesons in each event, the \CP conjugate amplitude $\overline{A}(0)$ is calculated as in Equation~\ref{eq:amplitude}, but with the transformation $(\cos\theta_1, \cos\theta_2, \Phi) \to (-\cos\theta_1, -\cos\theta_2, -\Phi)$.
The decay rate is convolved with a Gaussian function with a mass-dependent width to account for the \mass{\Kp\Km} resolution, as discussed in Section~\ref{sec:BsphiKK:resolution}, and then multiplied by the acceptance function, discussed in Section~\ref{sec:BsphiKK:acceptance}, to give the final PDF to be fitted to the data.

\subsection{Resonance lineshapes}
\label{sec:BsPhiKK:lineshapes}

The $\Kp\Km$ resonances which are significantly displaced from threshold are modelled using relativistic Breit--Wigner distributions~\cite{Breit:1936zzb}, given as:
\[
	T (m|m_0, \Gamma_0) \propto \frac{1}{m_0^2 - m^2 - im_0\Gamma(m)},
\]
where $\Gamma(m)$ is the mass-dependent width:
\[
	\Gamma(m) = \Gamma_0 \left(\frac{q}{q_0}\right)^{2J+1} \left(\frac{m_0}{m}\right) B^\prime_J(q)^2,
\]
where $\Gamma_0$ is the natural linewidth of the resonance, and the other symbols retain the same meanings as in Equation~\ref{eq:amplitude}.

For \decay{\fzero{980}}{\Kp\Km}, which is close to threshold, a Flatt\'e lineshape~\cite{Flatte:1976xu} is used:
\[
	T(m|m_0,g_{\pip\pim},g_{\Kp\Km}) \propto \frac{1}{m_0^2 - m^2 - im_0\left(\Gamma_{\pip\pim}(m) + \Gamma_{\Kp\Km}(m)\right)},
\]
where the mass-dependent widths for the $\pip\pim$ and $\Kp\Km$ channels are
\[
	\Gamma_{\pip\pim}(m) = g_{\pip\pim}\sqrt{\frac{m^2}{4} - m^2_{\pi^+}}
\]
and
\[
	\Gamma_{\Kp\Km}(m) =
	\begin{cases}
	g_{\Kp\Km} \sqrt{\frac{m^2}{4} - m^2_{\Kp}}
	&m > 2m_{\Kp}
	\\\\
	i g_{\Kp\Km} \sqrt{m^2_{\Kp} - \frac{m^2}{4}}
	&m < 2m_{\Kp}
	\end{cases},
\]
where $g_{\pip\pim}$ and $g_{\Kp\Km}$ are the couplings to the $\pip\pim$ and $\Kp\Km$ channels.
The \fzero{980} Flatt\'e couplings are taken from Ref.~\cite{LHCb-PAPER-2012-005} to be $g_{\pip\pim} = 199 \pm 30$\mev and $g_{\Kp\Km} = (3.0 \pm 0.3)\times g_{\pip\pim}$.

