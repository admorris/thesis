\section{Fits to data}
\label{sec:BsPhiKK:datafits}

Several amplitude fits are performed to the data in the invariant mass window $|\mass{\Kp\Km\Kp\Km}-\mass{\Bs}|<2\sigma_\text{eff}$, where $\sigma_\text{eff}$ is defined in Section~\ref{sec:BsPhiKK:massfits}.
The background model is described in Section~\ref{sec:BsPhiKK:background}.
The signal is modelled using the amplitude formalism described in Section~\ref{sec:BsPhiKK:formalism} with the choice of resonances built using an iterative method.
If a fit with an additional spin-1 or 2 resonance fails to converge, results in amplitudes close to zero or has a non-positive-definite covariance matrix, then it is repeated with a reduced number of free parameters by fixing the magnitudes and/or phases of the helicity amplitudes.

The figure of merit used to evaluate fit performance is the Bayesian information criterion (BIC)~\cite{schwarz1978}, which penalises the maximised likelihood, $\mathcal{L}$, using the number of free parameters, $p$, and the sample size, $n$: $\text{BIC} = p\ln(n) - 2\ln(\mathcal{L}).$
A smaller value of the BIC indicates a more suitable model.
For comparison, a similar quantity, called the Akaike information criterion ($\text{AIC} = 2p - 2\ln(\mathcal{L})$), as well as the negative log-likelihood ($-\ln(\mathcal{L})$) were calculated for each fit and found to give similar results.

The `minimal' model from Section~\ref{sec:BsPhiKK:resonances}, containing components for the \fzero{980}, $\phiz(1020)$, \ftwop{1525} and nonresonant $\Kp\Km$ pairs, is first fitted to the data. 
This fit is then compared with models that include an extra \fzero{1370}, \fzero{1500}, \ftwo{1640}, $\phiz(1680)$, \fzero{1710} or \ftwo{1750} resonance to determine which, if any, best describes the data.
The model including a $\phiz(1680)$ component is the only one to decrease the BIC, by 48 units.
The decrease in negative log-likelihood compared to the minimal model is 46 units, indicating that this component is required.

Next, fits are performed using the model from the previous step with the smallest BIC (minimal + $\phiz(1680)$) with an additional \fzero{1370}, \fzero{1500}, \ftwo{1640}, \fzero{1710} or \ftwo{1750} component.
The best model was the one including the \fzero{1500}, which decreased the BIC by 7.5 and the negative log-likelihood by 12 units.
The second best model from this step was the inclusion of the \fzero{1710}, which increased the BIC by 0.1 and decreased the negative log-likelihood by 8.2 units.

Finally, the addition of a \ftwo{1640}, \fzero{1710} or \ftwo{1750} were tried.
None of these were found to decrease the value of the BIC.
Therefore, the best fit model is chosen to be the one with components for the \fzero{980}, $\phiz(1020)$, \fzero{1500}, \ftwop{1525}, $\phiz(1680)$ and nonresonant \BsPhiKK decays.

As systematic variations, the three models with the best values of BIC are considered.
Hereafter, these are referred to as Models I, II and III.
Model I contains the fewest components.
Model II is formed by adding a \fzero{1500} component to Model I.
Likewise, Model III is formed by adding a \fzero{1710} component to Model I.
The fit results of the three best models are shown in Table~\ref{tab:BsPhiKK:results}, and their performance in terms of $-\ln(\mathcal{L})$, AIC and BIC are shown in Table~\ref{tab:BsPhiKK:NLLs}.
The projections of the best-fit model (Model II) are shown in Figure~\ref{fig:BsPhiKK:projections}.
The performance of all of the models tried in the first two steps, as well as the projections of Models I and III, are given in Appendix~\ref{ch:likelihoods}.

\begin{table}[h]
  \centering
  \caption[Fit results using the best fit models.]{Fit results using the three best fit models.}
  \label{tab:BsPhiKK:results}
  \begin{tabular}{|l|c|c|c|}
    \hline
    \multirow{2}{*}{Fit parameter} & \multicolumn{3}{c|}{Result with statistical uncertainty} \\
    & \multicolumn{1}{c}{Model II} & \multicolumn{1}{c}{Model III} & \multicolumn{1}{c|}{Model I} \\
    \hline
    \multicolumn{4}{|c|}{Breit--Wigner parameters}\\
    \hline
    $\phi(1020)$ $m$ [\mevcc]      & $1019.52 \pm 0.06$           & $1019.52 \pm 0.04$            & $1019.52 \pm 0.06$             \\
    $\phi(1020)$ $\Gamma$ [\mev]   & $\phantom{100}4.27 \pm 0.03$ & $\phantom{100}4.267 \pm 0.021$ & $\phantom{100}4.267 \pm 0.030$ \\
    \ftwop{1525} $m$ [\mevcc]      & $1522 \pm 4$                 & $1517.7 \pm 2.4$              & $1518 \pm 4$                   \\
    \ftwop{1525} $\Gamma$ [\mev]   & $\phantom{10}75 \pm 5$       & $\phantom{10}75.8 \pm 2.7$    & $\phantom{10}77 \pm 5$         \\
    \hline
    \multicolumn{4}{|c|}{Fit fractions}\\
    \hline
    $f_0(980)$         & $( 3.0 \pm 0.5)\%$            & $\phantom{-}( 6.3 \pm 0.7 )\%$            & $( 5.0 \pm 0.7 )\%$            \\
    $\phi(1020)$       & $(74.0 \pm 0.8)\%\phantom{1}$ & $\phantom{-}(73.7 \pm 0.8 )\%\phantom{1}$ & $(73.7 \pm 0.9 )\%\phantom{1}$ \\
    $f_0(1500)$        & $( 1.0 \pm 0.4)\%$            &  ---                                      &  ---                           \\
    $f_2^\prime(1525)$ & $( 6.0 \pm 0.6)\%$            & $\phantom{-}( 7.0 \pm 0.7 )\%$            & $( 6.9 \pm 0.6 )\%$            \\
    $\phi(1680)$       & $( 4.1 \pm 0.9)\%$            & $\phantom{-}( 3.9 \pm 0.7 )\%$            & $( 3.5 \pm 0.9 )\%$            \\
    $f_0(1710)$        & ---                           & $\phantom{-}( 2.6 \pm 0.7 )\%$            &  ---                           \\
    nonresonant       & $( 8.9 \pm 1.1)\%$            & $\phantom{-}( 7.4 \pm 1.2 )\%$            & $( 9.1 \pm 1.0 )\%$            \\
    interference       & $( 3.2 \pm 0.9)\%$            &            $(-0.6 \pm 0.9 )\%$            & $( 1.9 \pm 1.0 )\%$            \\
    \hline
    \multicolumn{4}{|c|}{Magnitudes}\\
    \hline
    $\phiz(1020)$ $|A_+|$    & $0.798 \pm 0.009$ & $0.798 \pm 0.009$ & $0.798 \pm 0.009$ \\
    $\phiz(1020)$ $|A_0|$    & $0.603 \pm 0.009$ & $0.603 \pm 0.009$ & $0.603 \pm 0.009$ \\
    \ftwop{1525} $|A_+|$     & $0.30 \pm 0.08$   & $0.33 \pm 0.03$   & $0.28 \pm 0.07$   \\
    \ftwop{1525} $|A_0|$     & $0.93 \pm 0.03$   & $0.915 \pm 0.012$ & $0.930 \pm 0.026$ \\
    $\phiz(1680)$ $|A_+|$    & $0.81 \pm 0.06$   & $0.87 \pm 0.03$   & $0.81 \pm 0.06$   \\
    $\phiz(1680)$ $|A_0|$    & $0.09 \pm 0.13$   & $0.16 \pm 0.08$   & $0.13 \pm 0.15$   \\
    \hline
    \multicolumn{4}{|c|}{Phases [~rad~]}\\
    \hline
    nonresonant $\delta_0$  & $\phantom{-}1.40 \pm 0.10$ & $\phantom{-}1.40 \pm 0.06$ & $\phantom{-}1.40 \pm 0.10$ \\
    \fzero{980} $\delta_0$   & $-1.62 \pm 0.23$           & $-1.97 \pm 0.07$           & $-1.75 \pm 0.19$           \\
    $\phiz(1020)$ $\delta_+$ & $\phantom{-}2.71 \pm 0.07$ & $\phantom{-}2.71 \pm 0.04$ & $\phantom{-}2.72 \pm 0.07$ \\
    \fzero{1500} $\delta_0$  & $-0.2 \pm 0.3$             &  ---                       &  ---                       \\
    \fzero{1710} $\delta_0$  & ---                        & $\phantom{-}2.52 \pm 0.08$ &  ---                       \\
    \ftwop{1525} $\delta_+$  & $\phantom{-}2.5 \pm 0.4$   & $\phantom{-}2.15 \pm 0.19$ & $\phantom{-}2.0 \pm 0.3$   \\
    \ftwop{1525} $\delta_0$  & $-0.06 \pm 0.26$           & $-0.17 \pm 0.09$           & $-0.36 \pm 0.22$           \\
    \ftwop{1525} $\delta_-$  & $\phantom{-}1.9 \pm 0.4$   & $\phantom{-}1.56 \pm 0.20$ & $\phantom{-}1.5 \pm 0.4$   \\
    $\phiz(1680)$ $\delta_+$ & $-0.40 \pm 0.24$           & $-0.75 \pm 0.09$           & $-0.63 \pm 0.23$           \\
    $\phiz(1680)$ $\delta_0$ & $-2.0 \pm 1.1$             & $-2.5 \pm 0.5$             & $-2.4 \pm 0.9$             \\
    $\phiz(1680)$ $\delta_-$ & $\phantom{-}2.97 \pm 0.32$ & $\phantom{-}2.57 \pm 0.17$ & $\phantom{-}2.6 \pm 0.3$   \\
    \hline
  \end{tabular}
\end{table}
\begin{landscape}
\begin{figure}
  \centering
  \includegraphics[width=0.6\textwidth]{body/BsPhiKK/figs/ModelII/Overlay_mKK_All_Data_PDF_0_pull.pdf}
  \includegraphics[width=0.6\textwidth]{body/BsPhiKK/figs/ModelII/Overlay_phi_All_Data_PDF_0_pull.pdf}
  \includegraphics[width=0.6\textwidth]{body/BsPhiKK/figs/ModelII/Overlay_ctheta_1_All_Data_PDF_0_pull.pdf}
  \includegraphics[width=0.6\textwidth]{body/BsPhiKK/figs/ModelII/Overlay_ctheta_2_All_Data_PDF_0_pull.pdf}
  \caption[Fit projections of Model II.]{Fit projections of Model II in \mass{\Kp\Km} and the helicity angles, overlaid onto the data (black points).
                             The total PDF is a solid black line,
                             the $\phiz(1020)$ is a purple long-dashed line,
                             the \fzero{980} is a green long-dashed line,
                             the \ftwop{1525} is a brown long-dashed line,
                             the nonresonant component is a light blue short-dashed line,
                             the interference is a black dot-dashed line,
                             the \fzero{1500} and $\phiz(1680)$ are thin solid black lines,
                             and the background is a red histogram.}
  \label{fig:BsPhiKK:projections}
\end{figure}
\end{landscape}

\begin{table}
\caption{Comparison of the $-\ln(\mathcal{L})$, AIC and BIC between the three best models.}
\label{tab:BsPhiKK:NLLs}
\begin{tabular}{|c|l|r|r|r|}
  \hline
  Model & Resonance content                                      & $-\ln(\mathcal{L})$     & AIC     & BIC     \\
  \hline
  I   & $\fzero{980} + \phiz(1020) + \ftwop{1525} + \phiz(1680)$ &  3836.7 &  7719.4 &  7864.6 \\
  II  & $\text{Model I} + \fzero{1500}$                          &  3824.7 &  7699.4 &  7857.1 \\
  III & $\text{Model I} + \fzero{1710}$                          &  3828.5 &  7707.0 &  7864.7 \\
  \hline
\end{tabular}
\end{table}

Fit fractions are calculated from the signal PDF, without background components or detector effects, as the integral of one component divided by the integral of the total PDF.
The sum of fit fractions can differ from unity due to interference.
This difference is quoted in Table~\ref{tab:BsPhiKK:results} as the `interference' fit fraction.
The statistical uncertainties on the fit fractions are obtained by generating and fitting $\sim 100$ random toy samples, each the same size as the dataset and with starting values set to the results of the fit to data, and taking the standard deviation of the results.

\begin{table}[h]
  \centering
  \caption{Change in negative log-likelihood caused by removing each component from Model II.}
  \begin{tabular}{|l|r|r|r|}
    \hline
    Component removed & $-\ln(\mathcal{L})$ & $\Delta \ln(\mathcal{L})$ & $n_\sigma$\\
    \hline
    None                  & 3824.7 & ---  & --- \\
    Nonresonant $\Kp\Km$ & 3827.9 &  3.2 &  2.1\\
    \fzero{980}           & 3855.9 & 31.2 &  7.6\\
    \fzero{1500}          & 3836.7 & 12.0 &  4.5\\
    \ftwop{1525}          & 3923.1 & 98.4 & 12.9\\
    $\phiz(1680)$         & 3879.1 & 54.4 &  9.4\\
    \hline
  \end{tabular}
  \label{tab:BsPhiKK:significances}
\end{table}

The statistical significance of each component in Model II, with the exception of the $\phiz(1020)$, is estimated from the change in $-\ln(\mathcal{L})$ caused by removing that component and repeating the fit to data.
The results are shown in Table~\ref{tab:BsPhiKK:significances}.
Wilks' theorem~\cite{Wilks:1938dza} allows for an estimate of the statistical significance, expressed as an equivalent number of Gaussian standard deviations, $n_\sigma$, as
\begin{equation}
n_\sigma = \sqrt{2} \text{erfc}^{-1}\left(\text{prob}\left(-2\Delta \ln(\mathcal{L}),\Delta\text{ndf}\right)\right),
\end{equation}
where the `erfc' is the cumulative error function, `prob' calculates the probability of \chisq for a given number of degrees of freedom (ndf), and $\Delta\text{ndf}=2$ for the spin-0 components, 6 for the $\phiz(1680)$ (3 complex amplitudes) and 8 for the \fzero{1525} (3 amplitudes, plus mass and width).
This is valid in the limit of an infinitely large sample size, and the calculation of $-\Delta \ln(\mathcal{L})$ neglects systematic uncertainties.
Although this approximation may lead to an over-estimate of the statistical significance, the decays \BsPhiftwop and \decay{\Bs}{\phiz\phiz(1680)} are significant enough to claim observation.

%A 3D \chisq is calculated by binning the data in $5 \times 5 \times 5$ bins in \mass{\Kp\Km}, $\cos\theta_1$ and $\cos\theta_2$ and summing the difference between the number of observed and expected events.
%This is done for each of the toy studies to build a distribution, shown in Figure~\ref{fig:BsPhiKK:chisq}.
%A \chisq function is fitted to this distribution to obtain the effective number of free parameters, which is found to be $130.9 \pm 1.5$.
%The 3D \chisq from the fit to data is 175.
%The probability of exceeding this value by chance is calculated, using the fitted number of effective degrees of freedom, to be 0.5\%.

%\begin{figure}
%  \centering
%  \includegraphics[width=0.5\textwidth]{body/BsPhiKK/figs/chisq}
%  \caption[Distribution of 3D \chisq values from toys.]{Distribution of 3D \chisq values from toys. The vertical line is the \chisq of the fit to data. A fit using a \chisq distribution is overlaid.}
%  \label{fig:BsPhiKK:chisq}
%\end{figure}

\begin{table}[h]
  \centering
  \caption[S-wave fractions in different windows around the $\phiz(1020)$ mass.]{S-wave fractions calculated from Model II in different windows around the $\phiz(1020)$ mass. The uncertainties are statistical.}
  \label{tab:BsPhiKK:swaves}
  \begin{tabular} {|l|c|}
    \hline
    Window & S-wave fraction \\
    \hline
    $\pm 10$\mevcc & $(0.29 \pm 0.11)\%$ \\
    $\pm 15$\mevcc & $(0.45 \pm 0.13)\%$ \\
    $\pm 20$\mevcc & $(0.60 \pm 0.15)\%$ \\
    $\pm 25$\mevcc & $(0.76 \pm 0.18)\%$ \\
    \hline
  \end{tabular}
\end{table}

The polarisation parameters of the \BsPhiPhi decay are measured to be $\Fzero = (35.3 \pm 1.0)\%,$ $\Fperp = (31.9 \pm 0.6)\%,$ and $\deltapara = 2.71 \pm 0.05,$ where the uncertainties are statistical.
In the \lhcb Run 1 angular analysis of \BsPhiPhi~\cite{LHCb-PAPER-2014-026}, the polarisation parameters were measured to be $\Fzero = (36.4 \pm 1.2)\%,$ $\Fperp = (30.5 \pm 1.3)\%,$ and $\deltapara = 2.54 \pm 0.07.$
The agreement between these results is good, considering the differences in the selection and that the angular analysis did not fit the $\Kp\Km$ invariant mass.
The parameter \deltapara disagrees the most of the three, which could be due to interference from resonant states that were not included in the angular analysis, or it could be due to a bias in the fit model as suggested by the fit to simulation in Section~\ref{sec:BsPhiKK:MCfits}.

The S-wave fraction in $\pm 10$, $\pm 15$, $\pm 20$ and $\pm 25$\mevcc windows around the $\phiz(1020)$ pole mass are obtained by calculating the \BsPhiPhi fit fraction within the mass window and assuming that what remains is S-wave.
The results are shown in Table~\ref{tab:BsPhiKK:swaves}.

\FloatBarrier

\subsection{Angular moments}
\label{sec:BsPhiKK:moments}

The distributions of angular moments depend on the spins of the resonant components and the interference between them.
The angular moments $\langle P_l \rangle$ are defined as the \mass{\Kp\Km} distribution weighted by the associated Legendre polynomial function $P_l(\cos\theta_2)$,
\begin{equation}
  \langle P_l \rangle = \int^1_{-1} d\Gamma(m,\theta_2) P_l(\cos\theta_2) d\cos\theta_2.
\end{equation}
If there is no contribution from resonances with spin $>2$, the moments with $l>4$ should be zero.
Each of the distributions can be interpreted as the following:
$\langle P_0 \rangle$ is the distribution of events,
$\langle P_1 \rangle$ is the sum of the interference between $S$- and $P$-wave and $P$- and $D$-wave,
$\langle P_2 \rangle$ is the sum of $P$-wave, $D$-wave and the interference between $S$- and $D$-wave,
$\langle P_3 \rangle$ is the interference between $P$- and $D$-wave,
$\langle P_4 \rangle$ is the $D$-wave, and
$\langle P_5 \rangle$ is the $F$-wave.
The averaging over \Bs and \Bsb causes a cancellation in the interference terms involving longitudinal $P$-wave and transverse $D$-wave, which causes $\langle P_1 \rangle$ and $\langle P_3 \rangle$ to be zero.

\begin{figure}
  \centering
  \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/LegendreMoments_phirange_P0.pdf}
  \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/LegendreMoments_phirange_P1.pdf}
  \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/LegendreMoments_phirange_P2.pdf}
  \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/LegendreMoments_phirange_P3.pdf}
  \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/LegendreMoments_phirange_P4.pdf}
  \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/LegendreMoments_phirange_P5.pdf}
  \caption[Legendre moment distributions in the low invariant mass range.]{Legendre moment distributions with $l<6$ in the invariant mass range $\mass{\Kp\Km}<1.05\gevcc$. The black points are data, and the blue histograms are toy events generated from the PDF.}
  \label{fig:BsPhiKK:lowmoments}
\end{figure}
\begin{figure}
  \centering
  \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/LegendreMoments_highmass_P0.pdf}
  \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/LegendreMoments_highmass_P1.pdf}
  \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/LegendreMoments_highmass_P2.pdf}
  \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/LegendreMoments_highmass_P3.pdf}
  \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/LegendreMoments_highmass_P4.pdf}
  \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/LegendreMoments_highmass_P5.pdf}
  \caption[Legendre moment distributions in the high invariant mass range.]{Legendre moment distributions with $l<6$ in the invariant mass range $1.05<\mass{\Kp\Km}<1.8\gevcc$. The black points are data, and the blue histograms are toy events generated from the PDF.}
  \label{fig:BsPhiKK:highmoments}
\end{figure}

The angular moments of the data and Model II are shown for $\mass{\Kp\Km}<1.05\gevcc$ in Figure~\ref{fig:BsPhiKK:lowmoments} and for $1.05<\mass{\Kp\Km}<1.8\gevcc$ in Figure~\ref{fig:BsPhiKK:highmoments}.
It can be seen from these plots that the model describes the first six angular moments of the data well.
The dip in the $\langle P_2 \rangle$ distribution around 1.63\gevcc was found to be best accommodated by models that include the $\phiz(1680)$.

