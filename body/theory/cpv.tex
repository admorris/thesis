\section{\CP violation}
\label{sec:theory:CPV}

% Symmetries in physics
The invariance of a system under a discrete or continuous transformation is referred to as a symmetry.
According to Noether's theorem~\cite{noether}, continuous symmetries lead to conserved quantities, \eg time translation symmetry leads to the conservation of energy.
Discrete symmetries are not covered by Noether's theorem and therefore do not necessarily correspond to conservation laws.
Important discrete symmetries in the Standard Model are charge conjugation ($C$), parity ($P$) and time-reversal ($T$).

% Charge symmetry
Under the charge conjugation operator $C$, a particle $\psi$ is mapped to its antiparticle $\overline{\psi}$.
% Parity Symmetry
The parity operator $P$ reverses the sign of Cartesian spatial coordinates: $(x,y,z)\to(-x,-y,-z).$
% T symmetry
The time-reversal operator $T$ reverses the sign of the time coordinate $t\to-t.$
The discrete symmetries $C$, $P$ and $T$ are the conditions that an observable remains invariant under the associated transformation.

% CP symmetry
\CP symmetry is the invariance under the combined operation of charge conjugation and parity:
\begin{equation}
  \CP \ket{\psi(x,y,z)} = \ket{\overline{\psi}(-x,-y,-z)}.
\end{equation}
% Parity violation
The electromagnetic and strong interactions conserve the symmetries \PC, \PP and \CP.
The weak interaction is known to violate $P$ symmetry, as it couples to left-handed fermions but not right-handed ones \cite{Wu:1957my}.
Similarly, it violates $C$ symmetry, as it couples to left-handed fermions but not left-handed antifermions.
%% Cronin & Fitch experiment
\CP symmetry was thought to be conserved in the weak interaction until the observation of the \CP-violating decay \decay{\KL}{\pip\pim} by Cronin and Fitch in 1963 \cite{Christenson:1964fg}.

In the context of the matter--antimatter asymmetry of the observable universe, \CP violation is needed to allow baryon-number-violating processes to occur more frequently in one direction than the other.
This is the second Sakharov condition for baryogenesis~\cite{Sakharov:1967dj}.
%There is one source of \CP violation in the Standard Model, which takes the form of a complex phase in the CKM matrix, which parameterises quark mixing: see Section~\ref{sec:theory:CPVSM}.

There are three types of \CP violation: \CP violation in decay (direct \CP violation), \CP violation in mixing (indirect \CP violation) and \CP violation in the interference between mixing and decay~\cite{Branco:1999fs}.

%\subsection{Direct \CP violation}
%\label{sec:theory:CPVD}
%% Introduction
Direct \CP violation is an asymmetry in the rate of a decay and its \CP-conjugate process.
Consider some particle $\ket{\psi}$ that decays weakly to some final state $\ket{f}$.
The amplitudes of this decay and its \CP-conjugate process are
\begin{equation}
A_f = \bra{f}H\ket{\psi} \;\;\;\mathrm{and}\;\;\;
\overline{A}_{\overline{f}} = \bra{\overline{f}}H\ket{\overline{\psi}},
\end{equation}
where $H$ is the weak-force Hamiltonian.
Since probability is the square of decay amplitude, \CP symmetry is violated if
\begin{equation}
\left|\frac{A_f}{\overline{A}_{\overline{f}}}\right|^2 \neq 1.
\label{eq:theory:CPVdec}
\end{equation}

%% Mechanism TODO: re-work this
Decay amplitudes are the sum of all possible contributing intermediate processes.
The individual contributing amplitudes are complex numbers which can be expressed as carrying two phases.
One is the \CP-conserving `strong' phase, $\delta$, whose sign is invariant under a \CP transformation.
The other is the \CP-violating weak phase, $\phi$, which changes sign under \CP.
The origin of the weak phase in the Standard Model is described in Section \ref{sec:theory:CPVSM}.
The decay amplitudes can be written as
\begin{equation}
A_f = \sum_k A_k = \sum_k |A_k|e^{i\delta_k}e^{i\phi_k},
\end{equation}
and
\begin{equation}
\overline{A}_{\overline{f}} = \sum_k \overline{A}_k = \sum_k |A_k|e^{i\delta_k}e^{-i\phi_k}.
\end{equation}
If there is only one contributing process, i.e. $A_f = A_1$, then the decay probabilities of \CP-conjugate processes must necessarily be the same:
\begin{equation}
|A_f|^2 = |A_1|^2e^{i\delta_1-i\delta_1}e^{i\phi_1-i\phi_1} = |A_1|^2,
\end{equation}
and
\begin{equation}
|\overline{A}_{\overline{f}}|^2 = |A_1|^2e^{i\delta_1-i\delta_1}e^{-i\phi_1+i\phi_1} = |A_1|^2.
\end{equation}
Therefore, direct \CP violation can only occur in decays with multiple possible intermediate states.
The simplest example is with two contributing processes:
\begin{equation}
\begin{array}{rcl}
A_f\;\;\, &=& |A_1|e^{i\delta_1}e^{i\phi_1} + |A_2|e^{i\delta_2}e^{i\phi_2},\\
|A_f|^2 &=& |A_1|^2 + |A_2|^2 + |A_1||A_2|e^{i(\Delta\delta)}e^{i(\Delta\phi)} + |A_1||A_2|e^{-i(\Delta\delta)}e^{-i(\Delta\phi)},\\
&=& |A_1|^2 + |A_2|^2 + 2|A_1||A_2|\left(
\cos(\Delta\delta)\cos(\Delta\phi) - \sin(\Delta\delta)\sin(\Delta\phi)
\right),\\
|\overline{A}_{\overline{f}}|^2&=& |A_1|^2 + |A_2|^2 + 2|A_1||A_2|\left(
\cos(\Delta\delta)\cos(\Delta\phi) + \sin(\Delta\delta)\sin(\Delta\phi)
\right),
\end{array}
\end{equation}
where $\Delta\delta = \delta_1-\delta_2$ and $\Delta\phi = \phi_1-\phi_2$.
The difference between the amplitudes is a function of both the strong and weak phases, but only arises as a result of the weak phase changing sign.
\begin{equation}
|A_f|^2-|\overline{A}_{\overline{f}}|^2 = -4|A_1||A_2|\sin(\Delta\delta)\sin(\Delta\phi).
\end{equation}

% Observable
Experimentally, direct \CP violation can be measured by the quantity $\mathcal{A}_{\CP}$, defined as
\begin{equation}
\mathcal{A}_{\CP} = \frac{|A_f|^2-|\overline{A}_{\overline{f}}|^2}{|A_f|^2+|\overline{A}_{\overline{f}}|^2} = \frac{\Gamma(\psi\to f)-\Gamma(\overline{\psi}\to\overline{f})}{\Gamma(\psi\to f)+\Gamma(\overline{\psi}\to\overline{f})},
\end{equation}
where $\Gamma(\psi\to f)$ is the partial decay width of the process $\psi\to f$.

%\subsection{Indirect \CP violation}
%\label{sec:theory:CPVM}
% Introduction
The second type of \CP violation occurs in neutral meson mixing.
Species of neutral meson which have distinct flavour eigenstates, $\psi^0$ and $\overline{\psi^0}$, can oscillate between particle and antiparticle without violating any conservation law.
Indirect \CP violation, or \CP violation in mixing, is where the rate of mixing is greater in one direction than the other.

% Mechanism
Since the \CP operator transforms one flavour eigenstate into the other, the flavour eigenstates are not eigenstates of \CP.
The \CP eigenstates of a neutral meson are orthogonal superpositions of the flavour eigenstates with equal amplitude:
\begin{equation}
\ket{\psi_-} = \frac{1}{\sqrt{2}} \ket{\psi^0} + \frac{1}{\sqrt{2}} \ket{\overline{\psi^0}} \;\;\;\mathrm{and}\;\;\;
\ket{\psi_+} = \frac{1}{\sqrt{2}} \ket{\psi^0} - \frac{1}{\sqrt{2}} \ket{\overline{\psi^0}},
\label{eq:theory:CPeigenstates}
\end{equation}
where the subscript of each of the \CP eigenstates denotes the \CP eigenvalue:
\begin{equation}
\CP \ket{\psi_+}=+\ket{\psi_+} \;\;\;\mathrm{and}\;\;\;
\CP \ket{\psi_-}=-\ket{\psi_-}.
\end{equation}
As discussed in Section~\ref{sec:theory:CPVSM}, the weak eigenstates of quarks are not mass eigenstates.
The weak eigenstates of neutral mesons, $\ket{\psi_1}$ and $\ket{\psi_2}$, may be defined as orthogonal superpositions of $\ket{\psi^0}$ and $\ket{\overline{\psi^0}}$:
\begin{equation}
\ket{\psi_1} = p\ket{\psi^0} + q\ket{\overline{\psi^0}} \;\;\;\mathrm{and}\;\;\;
\ket{\psi_2} = p\ket{\psi^0} - q\ket{\overline{\psi^0}},
\end{equation}
where $q$ and $p$ are complex mixing amplitudes.
% Observable
\CP symmetry is violated if the mixing amplitudes don't have the same magnitude \ie
\begin{equation}
\left|\frac{q}{p}\right|^2 \neq 1,
\label{eq:theory:CPVmix}
\end{equation}
which fulfils the condition of an asymmetry in the direction of oscillation.
% Oscillation

%\subsection{\CP violation in interference}
%\label{sec:theory:CPVI}
%% Introduction
The third type of \CP violation is in the interference between mixing and decay.
This occurs when a neutral particle $\psi^0$ and its antiparticle $\overline{\psi^0}$ can decay to the same final state $f$.
In this case, there are two paths for $\psi^0$ to decay to $f$: directly $\psi^0 \to f$ and through mixing $\psi^0 \to \overline{\psi^0} \to f$.
%% Mechanism
\CP violation arises from the difference in the weak phases of the two paths.
The first has a weak phase $\phi_D$, associated with the decay.
%The \CP-conjugate process $\overline{\psi^0} \to f$ has the opposite phase $-\phi_D$.
The mixing process $\psi^0 \to \overline{\psi^0}$ has a phase $\phi_M$.
The difference in phase between the paths is the residual phase $\phi_s = \phi_M - 2\phi_D$.

% Observable
This type of \CP violation can be quantified by defining $\lambda$ as the product of the two variables introduced in Equations~\ref{eq:theory:CPVdec} and \ref{eq:theory:CPVmix}:
\begin{equation}
\lambda \equiv \frac{q}{p}\frac{\overline{A}_f}{A_f} \;\;\;\mathrm{and}\;\;\;
\overline{\lambda} \equiv \frac{q}{p}\frac{\overline{A}_{\overline{f}}}{A_{\overline{f}}}.
\label{eq:theory:CPVint}
\end{equation}
If $f$ is a \CP eigenstate, then $\lambda = \overline{\lambda}$.
If $\lambda \neq 1$ then \CP symmetry is violated.
This manifests itself as an asymmetry in the time-dependent decay rates:
\begin{equation}
\mathcal{A}_{\CP}(t) = \frac{d\Gamma(\psi^0 \to f)/dt - d\Gamma(\overline{\psi^0} \to f)/dt}{d\Gamma(\psi^0 \to f)/dt + d\Gamma(\overline{\psi^0} \to f)/dt}.
\end{equation}

It is important to note that \CP may be violated through interference even if it is not individually violated in decay or mixing. In terms of $\lambda$, this would correspond to $|\lambda|=1$ and $\arg(\lambda)\neq0$.

