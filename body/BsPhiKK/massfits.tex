\section{Peaking background estimation}
\label{sec:BsPhiKK:peakbkgsize}

The decay modes \LbPhipK and \BdPhiKstar, where \KstarToKpi, are considered as possible peaking backgrounds that enter the data sample due to one hadron misidentified as a kaon.
When constructed under the \PhiKK hypothesis, the invariant mass distributions of these two modes have peaks near the \Bs mass.

In order to quantify the contribution of the \BdPhiKstar background to the data sample, the yield $N$ is estimated using:
\begin{equation}
\frac{N_{\BdPhiKstar}}{N_{\BsPhiPhi}} =
\frac{N_{\BdPhiKstar}^\text{sel}}{N_{\BsPhiPhi}^\text{sel}}
\frac{N_{\BsPhiPhi}^\text{gen}}{N_{\BdPhiKstar}^\text{gen}}
\frac{\BF(\BdPhiKstar)}{\BF(\BsPhiPhi)}
\frac{\BF(\KstarToKpi)}{\BF(\PhiToKK)}
\frac{f_d}{f_s},
\label{eq:BsPhiKK:Bdest}
\end{equation}

where $N^\text{gen}$ is the number of generated events, corrected for the generator-level efficiency, $N^\text{sel}$ is the number of simulation events that pass all the selection requirements given in Section~\ref{sec:BsPhiKK:selection}, and $f_s$ and $f_d$ are the fragmentation fractions for the \Bs and \Bd respectively.
The value of $f_s/f_d$ is taken from an average of LHCb results as $0.259 \pm 0.015$~\cite{LHCb-PAPER-2011-018,LHCb-PAPER-2012-037, LHCb-CONF-2013-011}.
For the purpose of this section, the number of \BsPhiPhi events is assumed to be the signal yield from the fit to data with a cut of $\mass{\Kp\Km}<1.05\gevcc$.
The values of the \PhiToKK, \KstarToKpi and \BdPhiKstar branching fractions are obtained in the same way as in Section~\ref{sec:BsPhiPhi:BsBFresult}.
The number of expected \BdPhiKstar events in the full \mass{\Kp\Km\Kp\Km} range of the selected data is found to be $23 \pm 3$.
This number is used to fix the size of the \BdPhiKstar component in the fit described in Section~\ref{sec:BsPhiKK:massfits}.

In Figure~\ref{fig:BsPhiKK:LbtophiKpveto}, there is a significant \Lb peak in the data reconstructed under the $\phiz\proton\Km$ hypothesis before the vetoes are applied.
The yield of this peak is determined from a fit to this distribution.
The \Lb peak is modelled as the sum of a Crystal Ball~\cite{Skwarnicki:1986xj} and a Gaussian function with a shared mean:
\begin{equation}
\label{eq:BsPhiKK:Lbpeakmodel}
P_\Lb(m) = f P_\text{CB}(m|\mu,\sigma_1,\alpha,n) +
(1-f) P_\text{G}(m|\mu,\sigma_2).
\end{equation}
The parameter $n$ is fixed to 1, and the parameters $f$, $\sigma_{\{1,2\}}$ and $\alpha$ are fixed to the results of a fit to the \LbPhipK simulation.
The fit to the \mass{\Kp\Km\proton\Km} distribution in data is performed in a window of $\pm 60 \mevcc$ around the \Lb mass using an exponential function to model the non-\Lb events.
This results in a yield of $1240 \pm 70$ events, which is multiplied by the efficiency of the vetoes described in Section~\ref{sec:BsPhiKK:vetoes} on the \LbPhipK simulation to give an expected yield of $88 \pm 7$ in the full \mass{\Kp\Km\Kp\Km} range of the selected data.
The fit is shown in Figure~\ref{figs:BsphiKK:Lbfit}.

\begin{figure}[h]
  \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/LbphiKp_fit_for_yieldS}
  \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/LbphiKp_fit_for_yield}
  \caption[Fits to the \mass{\Kp\Km\proton\Km} distributions in \LbPhipK simulation and data.]{Fits to the \mass{\Kp\Km\proton\Km} distributions in \LbPhipK simulation (left) and data (right) with the cuts described in Section~\ref{sec:BsPhiKK:cuts} applied.
  The `signal' \Lb peak shape is shown as a solid blue line in the simulation fit. The two components of the data fit are the \Lb `signal' and a `background' component (mostly misreconstructed \BsPhiKK events) modelled as an exponential curve.}
  \label{figs:BsphiKK:Lbfit}
\end{figure}

To verify this value of the \LbPhipK yield to within an order of magnitude, it is estimated using
\begin{equation}
\frac{N_{\LbPhipK}}{N_{\BsPhiPhi}} =
\frac{N_{\LbPhipK}^\text{sel}}{N_{\BsPhiPhi}^\text{sel}}
\frac{N_{\BsPhiPhi}^\text{gen}}{N_{\LbPhipK}^\text{gen}}
\frac{\BF(\LbPhipK)}{\BF(\BsPhiPhi)}
\frac{1}{\BF(\PhiToKK)}
\frac{f_{\Lb}}{f_s},
\label{eq:BsPhiKK:Lbest}
\end{equation}
where the symbols take the same meanings as in Equation~\ref{eq:BsPhiKK:Bdest}, under the assumption that
\begin{equation}
  \frac{\BF(\LbPhipK)}{\BF(\BsPhiPhi)} = \frac{\BF(\decay{\Lb}{\jpsi\proton\Km})}{\BF(\decay{\Bs}{\jpsi\phiz})},
\label{eq:BsPhiKK:Lbass}
\end{equation}
and the branching fractions for \decay{\Lb}{\jpsi\proton\Km} and \decay{\Bs}{\jpsi\phiz} are taken from the PDG~\cite{PDG2016}.
The value of $f_{\Lb}/f_{s}$ is calculated as 
\begin{equation}
\frac{f_\Lb}{f_s} = \frac{f_\Lb}{f_u+f_d} \frac{f_u+f_d}{f_s},
\end{equation}
where $f_s/(f_u+f_d) = 0.134\pm 0.011$ and $f_{\Lb}/(f_u+f_d) = (0.404 \pm 0.109) (1-(0.031 \pm 0.005)\frac{\pt}{1\gevc})$ \cite{LHCb-PAPER-2011-018}.
The value of \pt used to calculate this fragmentation fraction is the average \pt of the \Lb candidates in the fully selected \LbPhipK simulation sample: 10.4\gevc.
This results in an expected \LbPhipK yield of $\sim 41 \pm 14$, roughly half the size of the yield obtained from fitting the \mass{\Kp\Km\proton\Km} distribution.
Considering the assumption in Equation~\ref{eq:BsPhiKK:Lbass}, this estimate is in reasonable agreement with the value obtained from fitting the data.

\FloatBarrier

\section{Fit to the $\Kp\Km\Kp\Km$ invariant mass}
\label{sec:BsPhiKK:massfits}

A fit to the \PhiKK invariant mass distribution is performed in order to determine the number of signal and background candidates in the data used for the amplitude fit.
The invariant mass range $5150 < \mass{\Kp\Km\Kp\Km} < 5600$\mevcc is used so that the shape of the combinatorial background is properly modelled.

The \BsPhiKK component of the \mass{\Kp\Km\Kp\Km} spectrum is modelled using the sum of a Crystal Ball shape \cite{Skwarnicki:1986xj}, $P_\text{CB}$, and two Gaussian distributions, $P_\text{G}$, given by:
\begin{equation}
\label{eq:BsPhiKK:Bspeakmodel}
P_\Bs(m) = f_1 P_\text{CB}(m|\mu,\sigma_1,\alpha,n) +
(1-f_1) \big[ f_2 P_\text{G}(m|\mu,\sigma_2) +
(1-f_2) P_\text{G}(m|\mu,\sigma_3) \big],
\end{equation}
where $\mu$ is the shared mean of the three components, $\sigma_{\{1,2,3\}}$ are the Gaussian widths of each component, and  $\alpha$ and $n$ describe the position and shape of the power-law tail of the Crystal Ball distribution.
The parameters $f_{\{1,2\}}$ are factors that control the relative amplitude of each component.
They are required to be between 0 and 1.
The combinatorial background is modelled by an exponential function.

\begin{figure}[h]
  \includegraphics[width=0.32\textwidth]{body/BsPhiKK/figs/Bsmassfit_1800_mvacut_pkgbkgs_PB0}
  \includegraphics[width=0.32\textwidth]{body/BsPhiKK/figs/Bsmassfit_1800_mvacut_pkgbkgs_PB1}
  \includegraphics[width=0.32\textwidth]{body/BsPhiKK/figs/Bsmassfit_1800_mvacut_pkgbkgs_PB2}
  \caption[The \mass{\Kp\Km\Kp\Km} distributions in the simulation samples used to model the backgrounds.]{The \mass{\Kp\Km\Kp\Km} distributions used to model the \decay{\Bs}{\phiz f_1(1420)} (left), \BdPhiKstar (centre) and \LbPhipK (right) peaking background components. The convolved histogram shapes are shown as solid red lines on the left and right plots, overlaid onto smeared generator-level events. The PDF used to model the \BdPhiKstar background is shown as a red line on the centre plot.}
  \label{fig:BsphiKK:peaking}
\end{figure}

Three peaking background components are considered: misidentified \BdPhiKstar and \LbPhipK decays and partially reconstructed \decay{\Bs}{\phiz \Kp\Km \piz} decays.
The shape of the \BdPhiKstar component is modelled using a Crystal Ball plus a Gaussian function with a shared mean.
The parameters of this shape are fixed to the results of a fit to the fully selected \BdPhiKstar simulation sample, shown in the centre of Figure~\ref{fig:BsphiKK:peaking}.
Due to a lack of simulation events after the full selection, the \LbPhipK component is modelled using a histogram of generator-level \LbPhipK events, produced using the fast generation package RapidSim~\cite{Cowan:2016tnm}.
The background-specific cut, applied under the \mass{\Kp\Km\proton\Km} hypothesis, changes the shape of the \mass{\Kp\Km\Kp\Km} distribution.
This effect is simulated by categorising events as falling in either, both or neither of the \mass{\Kp\Km\proton\Km} and \mass{\Kp\Km\Kp\antiproton} windows.
The cut efficiency for each of these categories is found from the full \LbPhipK simulation and then used to weight the corresponding generator-level events.
This results in the distribution shown on the right of Figure~\ref{fig:BsphiKK:peaking}.
The partially reconstructed background component is modelled using a histogram of the \mass{\Kp\Km\Kp\Km} distribution in \decay{\Bs}{\phiz f_1(1420)(\decay{}{\Kp\Km\piz})} generator-level events, also produced using RapidSim.
This is shown on the left of Figure~\ref{fig:BsphiKK:peaking}.
The \decay{\Bs}{\phiz f_1(1420)} decay mode is chosen because it is predicted to have a branching fraction of $\left(16.1^{+9.9}_{-7.6}\right)\times 10^{-6}$~\cite{Liu:2016rqu}, comparable to that of \BsPhiPhi, and the decay mode \decay{f_1(1420)}{\Kp\Km\piz} is dominant~\cite{PDG2016}.
Both of the histogram PDFs are convolved with a Gaussian function with a width of 20\mevcc to account for the mass resolution.
The yield of the \BdPhiKstar component is fixed to the value found in Section~\ref{sec:BsPhiKK:peakbkgsize}, whereas the sizes of the \LbPhipK and partially reconstructed backgrounds are left free in the fit.

\begin{figure}[t]
\includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/Bsmassfit_1800_mvacut_pkgbkgs_MC}
\includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/Bsmassfit_1800_mvacut_pkgbkgs}
\caption[Fits to \mass{\Kp\Km\Kp\Km} using the fully selected phase-space simulation and data.]{Fits to \mass{\Kp\Km\Kp\Km} using the fully selected phase-space simulation (left) and data (right).
        The total fit is in red,
        the \Bs component is a blue long-dashed line,
        the partially reconstructed background is a solid green line,
        the \BdPhiKstar background is a dotted brown line,
        the \LbPhipK background is a dot-dashed black line,
        and the combinatorial background component is a purple dotted line.
        The pull distribution is shown underneath the fit.
        The black vertical dashed lines mark a region of $\pm 2 \sigma_\text{eff}$ around the fitted \Bs mass, where $\sigma_\text{eff}$ is the fitted width of the peak.}
\label{fig:BsMassFit_1800_mvacut}
\end{figure}

\begin{table}[h]
  \centering
  \caption[Results of the fit to \mass{\PhiKK} using the simulation and data.]{Results of the fit to \mass{\PhiKK} using the \BsPhiKK phase space simulation (top part) and data (bottom part).}
  \label{tab:BsPhiKK:Bsmassfitresults}
  \begin{tabular}{|c|rl|}
    \hline
    Parameter & Value & \\
    \hline
    $\alpha      $ & \BsmassfitmvacutpkgbkgsBSignalalpha  & \\
    $\sigma_1    $ & \BsmassfitmvacutpkgbkgsBSignalsigmaA &\mevcc \\
    $\sigma_2    $ & \BsmassfitmvacutpkgbkgsBSignalsigmaB &\mevcc \\
    $\sigma_3    $ & \BsmassfitmvacutpkgbkgsBSignalsigmaC &\mevcc \\
    $f_1         $ & \BsmassfitmvacutpkgbkgsBSignalfgausA &\\
    $f_2         $ & \BsmassfitmvacutpkgbkgsBSignalfgausB &\\
    \hline
    $\sigma_\text{eff}$ & \BsmassfitmvacutpkgbkgsBCombinatorialNresolution &\mevcc\\
    \hline
    $\mu         $ & \BsmassfitmvacutpkgbkgsBSignalmean &\mevcc \\
    $N_\text{sig}$ & \BsmassfitmvacutpkgbkgsBSignalN &\\
    $N_\text{bkg}$ & \BsmassfitmvacutpkgbkgsBCombinatorialN &\\
    $N_\text{part}$ & \BsmassfitmvacutpkgbkgsBpeakingN & \\
    $N_\text{\Lb}$ & \BsmassfitmvacutpkgbkgsBpeakingBN & \\
    \hline
  \end{tabular}
\end{table}

The parameters $f_{\{1,2\}}$, $\sigma_{\{1,2,3\}}$ and $\alpha$ in the \Bs signal model, given in Equation~\ref{eq:BsPhiKK:Bspeakmodel}, are fixed to the values found in a fit to the \BsPhiKK phase space simulation sample.
The Crystal Ball tail shape parameter $n$ is fixed to 1.
In the fit to data, the value of $\mu$, the yields of each component, and the slope of the exponential function that models the combinatorial background are left as free parameters.
The results of the fits to simulation and data are summarised in Table~\ref{tab:BsPhiKK:Bsmassfitresults} and Figure~\ref{fig:BsMassFit_1800_mvacut}.
From the pull distributions in Figure~\ref{fig:BsMassFit_1800_mvacut}, the fit describes the simulation and data well.
Using 50 bins in \mass{\Kp\Km\Kp\Km}, the \chisqndf of the fit to data is 0.90.
The yield of the \LbPhipK background is consistent with the value found in Section~\ref{sec:BsPhiKK:peakbkgsize}.

For the angular analysis, the signal region is defined as the window of \mass{\Kp\Km\Kp\Km} between $\mu-2\sigma_\text{eff}$ and $\mu+2\sigma_\text{eff}$, where $$\sigma_\text{eff} = f_1 \sigma_1 + (1-f_1)(f_2 \sigma_2 + (1-f_2) \sigma_3).$$
The yields of the signal, combinatorial background and peaking backgrounds are found by integrating the fitted PDFs over the signal region.
This results in \BsmassfitmvacutpkgbkgsBSignalNtwosigma signal events, \BsmassfitmvacutpkgbkgsBCombinatorialNtwosigma combinatorial background events, $\BsmassfitmvacutpkgbkgsBpeakingANtwosigmanodps \pm 1$ \BdPhiKstar events and $\BsmassfitmvacutpkgbkgsBpeakingBNtwosigma$ \LbPhipK events.
\FloatBarrier

