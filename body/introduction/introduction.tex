The origin of the asymmetry between the abundance of matter and antimatter in the universe is an unsolved problem in physics.
In the big bang, an equal amount of matter and antimatter should have been produced, yet our universe is dominated by matter~\cite{Canetti:2012zc}.
Three conditions~\cite{Sakharov:1967dj} are necessary to produce this imbalance: baryon/lepton number violation, \CP violation and a departure from thermal equilibrium.
Given the experimental constraints on baryon and lepton number violation, the amount of \CP violation in the Standard Model is many orders of magnitude too small to account for the observed matter--antimatter asymmetry~\cite{Cohen:1993nk,Riotto:1998bt}.
This motivates the search for sources of \CP violation beyond the Standard Model.
One of the main areas where this search is performed is `\PB physics': the study of hadrons containing \bquark quarks.

The primary goal of the \lhcb experiment is to look for new physics in \bquark hadron decays.
This programme of research builds on the studies of \Bd and \Bp mesons conducted at the \babar and \belle experiments~\cite{Bevan:2014iga}, which were $\ep\en$ collider experiments.
As a dedicated \PB-physics experiment at a hadron collider, \lhcb can also study \Bs mesons, \Bc mesons and \bquark baryons with unprecedented precision.

This thesis presents two studies of `charmless' \Bs meson decays, \ie fully hadronic decays which do not contain \cquark quarks.
Specifically, the decays \BsPhiPhi and \BsPhiKK are studied, which are examples of \decay{\bquark}{\squark\squarkbar\squark} transitions.
In the Standard Model, these decays are forbidden at tree-level and proceed via loop diagrams.
This enhances their sensitivity to new physics~\cite{Raidal:2002ph}.
Since the daughter particles of charmless \bquark hadron decays are considerably lighter than the parent particle, the energy released is typically large.
This permits the use of the perturbative QCD and QCD factorisation techniques for theoretical calculations of these decays~\cite{Collins:1989gx}.
QCD factorisation calculations rely on input from experiment for the long-range non-perturbative parts.
The results of these calculations have large uncertainties and are often less precise than the measured values.
In the absence of very large effects from new physics, studies of \CP violation in charmless \bquark hadron decays require more precise theoretical predictions with which to compare measurements.
The analyses presented in Chapters~\ref{ch:BsPhiPhi} and \ref{ch:BsPhiKK} provide measurements which may be used to improve the precision of these calculations.

Chapter~\ref{ch:theory} of this thesis gives an overview of the theoretical background and motivation behind the work presented in Chapters~\ref{ch:BsPhiPhi} and \ref{ch:BsPhiKK}.
The topics of the Standard Model of particle physics, \CP violation, hadronic \decay{\bquark}{\squark} transitions and QCD calculations of hadronic charmless \PB decays are covered.
Chapter~\ref{ch:detector} describes the \lhc and the \lhcb experiment, covering the detector, trigger and data processing software.
Chapter~\ref{ch:analysis} presents the general aspects of the event selection, common to both of the analyses presented in this thesis.
Chapter~\ref{ch:BsPhiPhi} details a measurement of the \BsPhiPhi branching fraction and a search for the decay \BdPhiPhi.
The author of this thesis performed the MVA optimisation, mass fits, calculation of the generator and selection efficiencies, the data-driven hadron trigger resimulation and the limit-setting on the \BdPhiPhi branching fraction.
Other contributors to this analysis are acknowledged on page~\pageref{ch:acknowledgements}.
This work is published in Ref.~\cite{LHCb-PAPER-2015-028}.
Chapter~\ref{ch:BsPhiKK} details an amplitude analysis of the \BsPhiKK decay, in which the decays \BsPhiftwop and \decay{\Bs}{\phiz\phiz(1680)} are observed for the first time.
The analysis includes a measurement of the branching fraction and polarisation amplitudes of the \BsPhiftwop mode.
The author performed the selection, peaking background estimation and mass fits, modelled the detector effects and background in the amplitude fit, developed the fitter using the RapidFit framework
%\footnote{RapidFit is a C++ framework for performing maximum log-likelihood fitting. It was developed by the Edinburgh \lhcb group and used in a number of angular fits and amplitude fits, \eg Refs.~\cite{LHCb-PAPER-2011-021,LHCb-PAPER-2014-026}.}
, performed numerous variations on the amplitude fits and calculated the systematic uncertainties.
The analysis is currently under review by the collaboration.
Finally, Chapter~\ref{ch:conclusion} summarises the results and discusses the prospects for the future of these decay modes.

The inclusion of charge-conjugate processes is implied throughout this thesis, unless the distinction is drawn between particle and antiparticle, such as in Sections~\ref{sec:theory:CPV} (\CP violation), \ref{sec:theory:mixing} ($\Bs-\Bsb$ mixing) and \ref{sec:theory:puzzle} (the polarisation puzzle).
Where symbols for light, flavourless mesons are quoted without mass numbers, the lowest-mass state is implied: \eg the symbol \phiz refers to the $\phiz(1020)$. All mentions of \Kstarz refer to the $\Kstar(892)^0$.

