\subsection{Angular distribution of $\PP \to \PV\PS,\ \PV\PV \text{ and } \PV\PT$ decays}
\label{sec:theory:kinematics}

When studying \CP violation in decays where the final state is not a \CP eigenstate, such as \BsPhiPhi, angular information is used to decompose the decay into different polarisations with definite \CP eigenvalues.
The angular distribution of the final-state particles depends on the spins of intermediate states.
Hence, angular information is also important to distinguish between different resonant components in an amplitude analysis.
This section considers sequential two-body decays of a pseudoscalar meson (\PP) to a final state containing four pseudoscalar mesons, \ie \decay{\PP}{\decay{\PX_1(}{\PP\PP)}\decay{\PX_2(}{\PP\PP)}}, where $\PX_1$ and $\PX_2$ may be scalar (\PS), vector (\PV) or tensor (\PT) mesons.

Helicity, $\lambda$, is the projection of angular momentum, $J$, onto the direction of a particle.
In a two-body decay of a pseudoscalar particle, the vector sum of the angular momenta of the daughter particles must be zero.
In the rest frame of the parent particle, the sum of the momentum vectors of the daughters must also be zero.
The helicities of the daughters in the decay frame must therefore be equal.
Since helicity is a discrete quantity, it takes integer values $-J_\mathrm{min} \leq \lambda \leq J_\mathrm{min},$ where $J_\mathrm{min}$ is the minimum angular momentum of either of the two daughter particles.
When one of the intermediate daughters is a scalar meson, such as in \BsPhifzero, there is only one possible polarisation: $\lambda=0$.
For the cases where both are vector mesons, as in \BsPhiPhi, or a vector and a tensor meson, as in \BsPhiftwop, there are three possible values of helicity, each with a different angular distribution and an associated polarisation amplitude: \Azero, \Aplus and \Aminus.
The polarisation corresponding to $\lambda=0$ is referred to as `longitudinal', while the $|\lambda|=1$ polarisations are `transverse'.

The longitudinally polarised final state of the decay \BsPhiPhi is a \CP eigenstate with an eigenvalue of $\CP = +1$.
The transverse polarisations \Aplus and \Aminus are themselves not \CP eigenstates, however the linear combination $\Apara \equiv \frac{1}{\sqrt{2}}\left(\Aplus+\Aminus\right)$ has $\CP = +1$, and $\Aperp \equiv \frac{1}{\sqrt{2}}\left(\Aplus-\Aminus\right)$ has $\CP = -1$.

\begin{figure}[h]
\setlength{\unitlength}{1.0mm}
  \centering
  \begin{picture}(140,60)
    \put(0,-1){\includegraphics*[width=140mm]{body/BsPhiKK/figs/angle_fig_norm2}}
%      \put(15 ,42){$\hat{n}_{V_1}$}
%      \put(128,41){$\hat{n}_{V_2}$}
      \put(80 ,22){\Bs}
      \put(83 ,37){$\Phi$}
      \put(115,30){$\theta_{2}$}
      \put(38 ,30){$\theta_{1}$}
      \put(42 ,16){\Km}
      \put(48 ,40){\Kp}
      \put(105,15){\Km}
      \put(117,42){\Kp}
      \put(60 ,30){\phiz}
      \put(95 ,30){\PX}
  \end{picture}
\caption[Decay angles for the \decay{\Bs}{\phiz(\to\Kp\Km)\PX(\to\Kp\Km)} decay.]{Decay angles for the \decay{\Bs}{\phiz(\to\Kp\Km)\PX(\to\Kp\Km)} decay. Each of the momentum vectors is constructed in the rest frame of its direct parent particle.}
\label{fig:angles}
\end{figure}

The helicity angles, as defined for \BsPhiKK, are illustrated in Figure~\ref{fig:angles}.
The angle $\Phi$ is between the decay planes of the daughters of the \Bs meson, as measured in the rest frame of the \Bs.
The angle $\theta_1$ is defined as the angle between the momentum vector of the \Kp in the \phiz rest frame and the momentum vector of the \phiz in the \Bs rest frame.
Similarly, $\theta_2$ is the angle between the momentum vector of the \Kp in the decay frame of the second daughter of the \Bs and the momentum vector of that daughter in the \Bs rest frame.

The helicity angle distribution of a \decay{\PP}{\decay{\PX_1(}{\PP\PP)}\decay{\PX_2(}{\PP\PP)}} decay can be generalised as \cite{Datta:2007yk}
\begin{equation}
  \label{eq:BsPhiKK:angdist}
  F(\Phi, \theta_1, \theta_2) = \sum\limits_{\lambda} \helamp{\lambda} Y_{J_{1}}^{-\lambda}(\pi-\theta_1,-\Phi)Y_{J_{2}}^{\lambda}(\theta_2,0),
\end{equation}
where $J_1$ and $J_2$ are the angular momenta of $\PX_1$ and $\PX_2$, respectively, and $Y_{J}^{\lambda}$ are the spherical harmonic functions.

\subsection{The polarisation puzzle}
\label{sec:theory:puzzle}

%Introduction
In a decay of a pseudoscalar meson to a pair of vector mesons (\decay{\PP}{\PV\PV}), the daughters are required by the conservation of angular momentum to have the same helicity.
This requirement excludes ten of the sixteen combinations of helicities of the final state valence quarks.
The remaining six combinations, of which four are longitudinally polarised, are subject to different suppression factors.

%Explanation
Since the weak interaction only couples to fermions with left-handed chirality, and antifermions with right-handed chirality, the outgoing quarks from a weak vertex should predominantly have negative helicity.
Likewise, outgoing antiquarks from a weak vertex should predominantly have positive helicity.
If a quark \Pq produced at a weak vertex in a \bquark decay undergoes a `helicity flip', this introduces a suppression factor of $\sim m_\quark/m_\bquark$~\cite{Kagan:2004uw}.
In \decay{\overline{\PB}}{\PV\PV} decays, one helicity flip is required for negative polarisation, and two are required for positive polarisation.
Longitudinal polarisation is possible without any helicity suppression factors.

\begin{table}[h]
  \centering
  \caption[The allowed combinations of helicities of the final-state valence quarks in the decay \decay{\Bsb}{\phiz\phiz}.]{The six allowed combinations of helicities of the final-state valence quarks in the decay \decay{\Bsb}{\phiz\phiz}.
           The antiquark labelled $\squarkbar_2$ is chosen to be the spectator quark.
           Red numbers indicate which quarks must undergo a helicity flip in a pure electroweak diagram.}
  \begin{tabular}{|l|r|r|r|r|r|r|}
    \hline
    $\squark_1$    helicity & $-\frac12$ & $-\frac12$ & $-\frac12$ & $\color{red}+\frac12$ & $\color{red}+\frac12$ & $\color{red}+\frac12$ \\
    $\squarkbar_1$ helicity & $\color{red}-\frac12$ & $+\frac12$ & $+\frac12$ & $\color{red}-\frac12$ & $\color{red}-\frac12$ & $+\frac12$ \\
    $\phiz_1$      helicity &       $-1$ &        $0$ &        $0$ &        $0$ &        $0$ &       $+1$ \\\hline
    $\squark_2$    helicity & $-\frac12$ & $-\frac12$ & $\color{red}+\frac12$ & $-\frac12$ & $\color{red}+\frac12$ & $\color{red}+\frac12$ \\
    $\squarkbar_2$ helicity & $-\frac12$ & $+\frac12$ & $-\frac12$ & $+\frac12$ & $-\frac12$ & $+\frac12$ \\
    $\phiz_2$      helicity &       $-1$ &        $0$ &        $0$ &        $0$ &        $0$ &       $+1$ \\\hline
    Helicity flips          &          1 &          0 &          1 &          2 &          3 &          2 \\\hline
  \end{tabular}
  \label{tab:theory:helicity}
\end{table}

%Results
The allowed helicity configurations and the number of required helicity flips for a purely electroweak \decay{\Bsb}{\phiz\phiz} diagram are summarised in Table~\ref{tab:theory:helicity}.
Note that one of the longitudinally polarised configurations is favoured, and both of the transverse polarisations are suppressed.
If, as in Figure~\ref{fig:theory:penguin}, the pair $\squark_2\squarkbar_1$ is created by the strong interaction, parity conservation requires that these quarks have opposite helicity, hence the configuration in the penultimate column of the table requires only one helicity flip, rather than three.

%Implications
For \decay{\Bb}{\PV\PV} decays with light particles in the final state, the expected hierarchy of the helicity amplitudes is therefore $|\helamp{0}| \gg |\helamp{-}| \gg |\helamp{+}|$ \ie the longitudinal fraction, $\Fzero \equiv |\helamp{0}|^2/\sum_i|\helamp{i}|^2$, is large.
For \decay{\B}{\PV\PV} decays, the expected hierarchy is $|\helamp{0}| \gg |\helamp{+}| \gg |\helamp{-}|$.

\begin{figure}
  \centering
  \includegraphics[width=0.9\textwidth]{body/theory/figs/polar}
  \includegraphics[width=0.9\textwidth]{body/theory/figs/polarbs}
  \caption[Measurements of the longitudinal polarisation fractions of \decay{\Bd}{\PV\PV} and \decay{\Bs}{\PV\PV} decays]{Measurements of the longitudinal polarisation fractions of \decay{\Bd}{\PV\PV} (top) and \decay{\Bs}{\PV\PV} (bottom) decays~\cite{HFAG2016s}.}
  \label{fig:theory:polar}
\end{figure}

The measured values of \Fzero in various \decay{\Bd}{\PV\PV} and \decay{\Bs}{\PV\PV} decay channels are shown in Figure~\ref{fig:theory:polar}.
Tree-level dominated decays, such as \decay{\Bd}{\rhop\rhom}, are indeed observed to be predominantly longitudinally polarised \cite{Aubert:2007nua}.
However, penguin-dominated decays, such as \BdPhiKstar, are found to have large transverse polarisations~\cite{Chen:2003jfa,Aubert:2003mm}.
This `polarisation puzzle' has generated much discussion as to whether this discrepancy is due to effects such as penguin-annihilation contributions~\cite{Kagan:2004uw}, final-state interactions~\cite{Cheng:2004ru,Colangelo:2004rd}, form-factor tuning~\cite{Li:2004mp} or physics beyond the Standard Model (see Ref.~\cite{Beneke:2006hg} for a comprehensive list).
In order to resolve this issue, further experimental study is required to reduce the uncertainties on the long-range form-factors and allow more precise theoretical calculations of the polarisation amplitudes.

