\section{Introduction}
\label{sec:BsPhiPhi:introduction}

This chapter details an analysis that is published in Ref.~\cite{LHCb-PAPER-2015-028}.

% Physics motivation for the mode
As discussed in Chapter~\ref{ch:theory}, the decay \BsPhiPhi is a flavour-changing neutral current transition.
It is therefore forbidden at tree-level in the Standard Model.
To leading order, this decay proceeds by a \decay{\bquarkbar}{\squarkbar\squark\squarkbar} strong penguin diagram.
As such, it is potentially sensitive to the effects of new particles entering the loop~\cite{Raidal:2002ph}.
% Theory predictions
The central values of predictions for the \BsPhiPhi branching fraction lie in the range $(1.3 \; \mathrm{to} \; 2.6) \times 10^{-5}$ \cite{Beneke:2006hg, Li:2003hea, Cheng:2009mu, Zou:2015iwa, Wang:2017hxe}.
The predictions are summarised in Table~\ref{tab:theory:BsBFpredictions} and detailed in Section~\ref{theory:sec:qcdprediction}.

% Experimental history of the mode
The first observation of the decay was made by the CDF collaboration at the Tevatron~\cite{Acosta:2005eu}.
Subsequently, CDF measured the branching fraction of this mode using \BsToJPsiPhi for normalisation~\cite{Aaltonen:2011rs}.
Using the current PDG value of the \BsToJPsiPhi branching fraction~\cite{PDG2016}, the CDF result is $${\BF(\BsPhiPhi)=(1.91\pm 0.26\stat\pm 0.16\syst)\times 10^{-5}}.$$
%The polarisation amplitudes have been measured by CDF~\cite{Aaltonen:2011rs} and LHCb~\cite{LHCb-PAPER-2012-004}.
%The \CP-violating phase of the \Bs-\Bsb mixing diagram cancels exactly with that of the \BsPhiPhi penguin diagram, therefore the overall phase of the decay, \phis, is expected to be close to zero~\cite{Raidal:2002ph}.
%Measurements of \phis in this channel have been performed by CDF~\cite{Aaltonen:2011rs} and LHCb~\cite{LHCb-PAPER-2013-007,LHCb-PAPER-2014-026}, which show agreement with the hypothesis of \CP-symmetry.

% Motivation for the branching fraction
The branching fraction of \BsPhiPhi is used for normalisation in studies of other charmless \bquark hadron decays.
Therefore, it is important to improve its precision.
The Run 1 LHCb dataset contains a significantly higher number of \BsPhiPhi candidates than were collected by CDF, so an appreciable reduction in the statistical uncertainty of the branching fraction is expected.
This chapter details a measurement of the \BsPhiPhi branching fraction relative to that of the \BdPhiKstar decay.
Throughout this chapter, \Kstarz refers specifically to the $\Kstar(892)^0$ meson.

The ratio of the \BsPhiPhi branching fraction relative to that of \BdPhiKstar is calculated using
\begin{equation}
  \label{eq:BsPhiPhi:phiphiBR}
  \frac{\BF(\BsPhiPhi)}{\BF(\BdPhiKstar)} = 
  \frac{N_{\BsPhiPhi}}{N_{\BdPhiKstar}}
  \frac{\varepsilon_{\BdPhiKstar}}{\varepsilon_{\BsPhiPhi}}
  \frac{\BF(\KstarToKpi)}{\BF(\PhiToKK)}
  \cdot
  \frac{1}{f_s/f_d},
\end{equation}
where the symbols \BF represent branching fractions, $N$ are yields obtained from fits to data (Section~\ref{sec:BsPhiPhi:massfits}), $\varepsilon$ are efficiencies obtained through a mix of simulation and data-driven techniques (Section~\ref{sec:BsPhiPhi:efficiencies}), and $f_s/f_d$ is the ratio of fragmentation fractions, \ie the ratio of probabilities that a \bquark quark hadronises with a \squark or a \dquark quark.

% Bd mode
The decay \BdPhiPhi is unobserved and highly suppressed in the Standard Model by the OZI rule~\cite{Okubo:1963fa, Zweig:1964jf, Iizuka:1966fk}.
Branching fraction predictions lie in the range $(0.2 \; \mathrm{to} \; 3.0) \times 10^{-8}$ \cite{Beneke:2006hg, BarShalom:2002sv, Lu:2005be}.
These predictions are summarised in Table~\ref{tab:theory:BdBFpredictions} and detailed in Section~\ref{sec:theory:bdtoss}.
However, this value may be enhanced in models of physics beyond the Standard Model~\cite{BarShalom:2002sv}.
The previous best upper limit for the branching fraction of the mode is $$\BF(\BdPhiPhi) < 2.0 \times 10^{-7} \text{ at } 90\,\% \text{ confidence level},$$ set by the BaBar collaboration~\cite{Aubert:2008fq}.
Section~\ref{sec:BsPhiPhi:Bdlimit} describes a search for this decay mode and the determination of a new upper limit on its branching fraction.

