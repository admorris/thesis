\section{The \lhcb detector}
\label{sec:detector:lhcb}

This section describes the geometry of the \lhcb detector, illustrated in Figure~\ref{fig:detector:lhcb}, as well as the purpose and locations of its subdetectors.
The subdetectors are described in further detail in Sections~\ref{sec:detector:tracking} and \ref{sec:detector:pid}.

\begin{figure}[htb]
  \includegraphics[width=\textwidth]{body/detector/figs/LHCb_detector}
  \caption[Side view of the \lhcb detector.]{Side view of the \lhcb detector in the y--z (non-bending) plane, showing the positions of the subdetectors.}
  \label{fig:detector:lhcb}
\end{figure}

\subsection{Coordinate system}
\label{sec:detector:coordinates}

A right-handed Cartesian coordinate system is adopted by \lhcb.
The origin of the coordinate system is located at the interaction point.
The $z$ axis is aligned with the direction of beam 1 and points in the `downstream' direction, towards the end of the detector.
The $y$ axis points vertically upwards.
The $x$ axis points horizontally towards the centre of the \lhc ring.

\subsection{Overview}
\label{sec:detector:overview}

% General overview of the detector
In high-energy hadron collisions, the majority of heavy quark pairs are produced at a small angle with respect to the beam axis.
The polar angle and pseudorapidity distributions of \bquark quarks in simulated \lhc collisions are shown in Figure~\ref{fig:detector:bbacc}.
The \lhcb detector~\cite{Alves:2008zz}, shown in Figure~\ref{fig:detector:lhcb}, is positioned almost entirely on one side of the crossing point in the direction of travel of beam 1, defined as the `forward' direction.
The polar angle coverage extends from 15\mrad to 300\mrad in the horizontal (bending) plane and to 250\mrad in the vertical (non-bending) plane.
This translates to a solid-angle coverage of approximately $4\,\%$ and a pseudorapidity range of $2 < \eta < 5$.
Roughly $25\,\%$ of the $\bquark\bquarkbar$ pairs produced in \lhc collisions are contained in this region. % TODO: cite


\begin{figure}[htb]
  \centering
  \includegraphics[width=0.49\textwidth]{body/detector/figs/bb_acceptance_eta}
  \includegraphics[width=0.49\textwidth]{body/detector/figs/bb_acceptance}
  \caption[Pseudorapidity and polar angle distributions of simulated \bquark quarks.]
          {Left: Pseudorapidity distribution of \bquark quarks produced in simulated $\proton\proton$ collisions.
                 The solid lines show the acceptance of \lhcb (red) compared to \atlas and \cms (yellow).
           Right: Polar angle distribution of simulated \bquark quarks.
                  The red region is the acceptance of \lhcb.}
  \label{fig:detector:bbacc}
\end{figure}

% Quick introduction and context for each of the subdetectors
The subdetectors of \lhcb can be grouped in two categories based on their function: namely tracking and particle identification.
The tracking system, detailed in Section~\ref{sec:detector:tracking}, consists of a retractable silicon-strip vertex locator~\cite{LHCb-TDR-005} around the interaction point; a silicon-strip tracking detector~\cite{LHCb-TDR-009} immediately upstream from the magnet~\cite{LHCb-TDR-001} and three tracking stations immediately downstream from the magnet, which consist of silicon strips in the inner part~\cite{LHCb-TDR-008} and straw tubes in the outer part~\cite{LHCb-TDR-006}.
The particle identification system, detailed in Section~\ref{sec:detector:pid}, consists of the ring-imaging Cherenkov (\rich) system, the calorimeter system and the muon detection system.
There are two \rich detectors~\cite{LHCb-TDR-003}, each designed to discriminate between pions, kaons and protons in different momentum ranges.
The first is located between the vertex locator and the magnet, while the other is located downstream from the third tracking station.
The calorimetry system~\cite{LHCb-TDR-002} sits downstream from the second \rich detector and consists of a scintillator pad and preshower detector, an electromagnetic calorimeter and a hadronic calorimeter.
The muon system~\cite{LHCb-TDR-004} consists of five stations (M1--M5) composed of multi-wire proportional counters in low occupancy regions and triple gas electron multipliers in high occupancy regions.
The M1 station is located between the second \rich detector and the calorimeter.
The M2 station is located immediately downstream from the calorimeter, and the rest of the stations follow, interspersed with iron absorbers.

%A new subdetector, the High Rapidity Shower Counters for \lhcb (\herschel)~\cite{Albrow:2014lta}, was added for Run 2 of the \lhc, which began in 2015.
%\herschel consists of five planes of scintillators, using the same design as the preshower subdetector, located up to 150\m from the crossing point; two stations are in the forward region and three in the backward region.
%The subdetector extends the pseudorapidity coverage up to $\eta=8$ and is used to veto hard interactions and disassociative events that may otherwise pass the trigger for central exclusive production.
\input{body/detector/tracking}
\input{body/detector/pid}
\input{body/detector/trigger}
%\input{body/detector/software}
%\input{body/detector/upgrade}

