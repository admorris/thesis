\chapter{The Standard Model}
\section{The electroweak interaction}
\label{sec:app:ew}

% Glashow's model SU(2)×U(1)
Before breaking electroweak symmetry, the interaction is mediated by four massless vector-boson fields: $W^1$, $W^2$ and $W^3$ with symmetry $SU(2)$ and coupling $g$, and $B$ with symmetry $U(1)$ and coupling $g^\prime$.
% - kinetic Lagrangian
The kinetic energy term of the electroweak Lagrangian is
\begin{equation}
  \mathcal{L}_G = -\frac{1}{4} \sum_{i=1}^3 W^i_{\mu\nu} W^{i\mu\nu} - \frac{1}{4} B_{\mu\nu}B^{\mu\nu},
  \label{eq:theory:LG}
\end{equation}
where $B_{\mu\nu}$ and $W^i_{\mu\nu}$ are the field strength tensors of the $B$ and $W^i$ fields, given by
\begin{equation}
  \begin{array}{rcl}
    W^i_{\mu\nu} &=& \partial_\mu W_\nu^i - \partial_\nu W_\mu^i - g \varepsilon_{ijk}W^j_\mu W^k_\nu, \\
    B_{\mu\nu}   &=& \partial_\mu B_\nu   - \partial_\nu B_\mu.
  \end{array}
\end{equation}

% - interaction Lagrangian
The interaction term of the Lagrangian is
\begin{equation}
%  \mathcal{L}_I = i\sum_j\overline{\psi}^j_L \slashed{D}^L \psi^j_L + i\sum_k\overline{\psi}^k_R \slashed{D}^R \psi^k_R
  \mathcal{L}_I = i\sum_{j=1}^3 \left(
                  \overline{\ell}_L^j \slashed{D}^L \ell_L^j + \overline{e}_R^j \slashed{D}^R e_R^j
                + \overline{q}_L^j \slashed{D}^L q_L^j + \overline{u}_R^j \slashed{D}^R u_R^j + \overline{d}_R^j \slashed{D}^R d_R^j,
                \right)
  \label{eq:theory:LI}
\end{equation}
where the index $j$ denotes the generations of fermions, $\ell^j_L$ and $q^j_L$ are left-handed lepton and quark doublets, and $e^j_R$, $u^j_R$ and $d^j_R$ are the right-handed singlets of charged leptons, up-type quarks and down-type quarks, respectively.\footnote{There are no right-handed neutrinos in the Standard Model, so there is no $\overline{\nu}_R^j \slashed{D}^R \nu_R^j$ term.}
The two covariant derivatives acting on left-handed and right-handed fermions are
\begin{equation}
    D^L_\mu = \partial_\mu + i\frac{g}{2}\sum_{i=1}^3\sigma^iW^i_\mu + i\frac{g^\prime}{2}B_\mu,
\end{equation}
\begin{equation}
    D^R_\mu = \partial_\mu + i\frac{g^\prime}{2}B_\mu,
\end{equation}
where $\sigma^i$ are the Pauli matrices.
% - left-handed and right-handed couplings
% - weak isospin doublets and hypercharge singlets

% Higgs mechanism
So far, this description of the electroweak interaction deals with massless fields, which do not correspond to the particles observed in nature.
Electromagnetism is mediated by a massless gauge boson, \Pgamma, while the weak interaction is mediated by three massive gauge bosons: $\PW^+$, $\PW^-$ and $\PZ^0$.
The charged fermions are also massive and carry both left- and right-handed chirality.\footnote{Neutrino oscillation implies they are also massive, but the problem of adding neutrino masses to the Standard Model will not be covered here.}

The Higgs mechanism provides a way to mix the massless $W^i$ and $B$ fields to produce the observed electroweak bosons and also grant masses to the charged fermions.
% - complex scalar doublet
It does this by introducing a doublet of complex scalar fields
\begin{equation}
  \phi =
  \begin{pmatrix}
  \varphi^+ \\
  \varphi^0
  \end{pmatrix}.
\end{equation}
In terms of real-valued scalar fields, $\varphi^+ = \varphi_1 + i\varphi_2$ and $\varphi^0 = \varphi_3 + i\varphi_4$.
The conjugate of this doublet, which will appear later when granting mass to the up-type quarks, is defined as
\begin{equation}
  \phi^c =
  \begin{pmatrix}
  \overline{\varphi}^0 \\
  -\varphi^-
  \end{pmatrix}.
\end{equation}

% - kinetic Lagrangian
The dynamics of this doublet is contained in the Lagrangian term
\begin{equation}
  \mathcal{L}_H = \left|D^H_\mu\phi\right|^2 - \mu^2\left|\phi\right|^2 + \lambda\left|\phi\right|^4,
  \label{eq:theory:LH}
\end{equation}
where $\mu$ and $\lambda$ are constants which describe the shape of the Higgs potential.
The covariant derivative is
\begin{equation}
  D^H_\mu = \partial_\mu + ig\sum_{i=1}^3 W^i_\mu + i\frac{g^\prime}{2}B_\mu.
\end{equation}

% - yukawa interaction Lagrangian
The charged fermions couple to the scalar doublet through the Yukawa interaction terms of the electroweak Lagrangian
\begin{equation}
  \mathcal{L}_Y = -\sum_{j=1}^3 \left(
                  y_e^j \overline{\ell}_L^j \phi   e_R^j
                + y_u^j \overline{ q  }_L^j \phi^c u_R^j
                + y_d^j \overline{ q  }_L^j \phi   d_R^j
                + h.c.
                \right),
  \label{eq:theory:LY}
\end{equation}
where $y^j_e$, $y^j_u$ and $y^j_d$ are the Yukawa couplings for the charged leptons, up-type quarks and down-type quarks for each generation $j$, and the term $h.c.$ represents the Hermitian conjugates of the three terms shown.

% - potential, vev and SSB
With a choice of $\lambda<0$ and $\mu^2<0$, the Higgs potential has minima at a value of $\phi$ given by
\begin{equation}
  \left|\phi\right|^2 = \frac{\mu^2}{2\lambda}.
\end{equation}
A vacuum state is chosen from the set of possible minima such that
\begin{equation}
  \bra{0}\varphi_1\ket{0} = \bra{0}\varphi_3\ket{0} = \bra{0}\varphi_4\ket{0} = 0,
\end{equation}
and the vacuum expectation value of the field $\varphi_3$ becomes
\begin{equation}
  |\bra{0}\varphi_3\ket{0}|^2 = \frac{\mu^2}{\lambda} \equiv v^2.
\end{equation}
The scalar doublet becomes
\begin{equation}
  \phi =
  \begin{pmatrix}
  0 \\
  \varphi_3
  \end{pmatrix} \equiv \frac{1}{\sqrt{2}}
  \begin{pmatrix}
  0 \\
  v + H
  \end{pmatrix},
  \label{eq:theory:SSBdoublet}
\end{equation}
where $H$ is the field of the Higgs boson.

The full electroweak Lagrangian is the sum of the terms defined in Equations~\ref{eq:theory:LG}, \ref{eq:theory:LI}, \ref{eq:theory:LH} and \ref{eq:theory:LY}.
By inserting the value of $\phi$ from Equation~\ref{eq:theory:SSBdoublet} into the full Lagrangian, the physical electroweak gauge bosons now appear as
\begin{equation}
  \begin{array}{rcl}
    W^+_\mu    &=& \frac{1}{\sqrt{2}} \left( W^1_\mu - i W^2_\mu \right), \\
    W^-_\mu    &=& \frac{1}{\sqrt{2}} \left( W^1_\mu + i W^2_\mu \right), \\
    Z^0_\mu    &=& B_\mu \sin\theta_W + W^3_\mu \cos\theta_W, \\
    \gamma_\mu &=& B_\mu \cos\theta_W - W^3_\mu \sin\theta_W,
  \end{array}
\end{equation}
where $\theta_W$ is a free parameter called the Weinberg angle, which relates the masses of the $W^\pm$ and $Z^0$ bosons by $m_W = m_Z \cos\theta_W$.
The Yukawa term in the Lagrangian now provides Dirac mass terms for the fermions, \eg for the electron
\begin{equation}
  \mathcal{L}_{Ye} = -\frac{y_e^1}{\sqrt{2}} \left(v+H\right)\left(\overline{e}_R e_L + \overline{e}_L e_R\right)
                   = -\frac{y_e^1}{\sqrt{2}} \left(v+H\right)\overline{e}e,
\end{equation}
where the electron mass is given by
\begin{equation}
  m_e = \frac{y_e^1 v}{\sqrt{2}}.
\end{equation}

\section{The strong interaction}
\label{sec:app:qcd}

In the Standard Model, the strong interaction is described by a gauge theory called Quantum Chromodynamics (QCD), which is mediated by eight massless vector boson fields, $G$, with symmetry $SU(3)$ and coupling $g_s$.
% The QCD Lagrangian
The QCD Lagrangian is given by
\begin{equation}
  \mathcal{L}_{S} = -\frac{1}{4} \sum_{i=1}^8 G^{i}_{\mu\nu}G^{i\mu\nu} + i \sum_{f} \overline{Q}^f \left(\slashed{D}^{S} - m_f\right) Q^f,
\end{equation}
where $Q^f$ are quark triplets of a particular flavour $f\in\{u,c,t,d,s,b\}$, $G_{\mu\nu}$ is the gluon field strength tensor
\begin{equation}
  G^{i}_{\mu\nu} = \partial_\mu G^i_\nu - \partial_\nu G^i_\mu - g_s f_{ijk} G^j_\mu G^k_\nu,
\end{equation}
where $f_{ijk}$ are the $SU(3)$ structure constants, and the covariant derivative is
\begin{equation}
  D^{S}_\mu = \partial_\mu - i\frac{g_s}{2}\sum_{i=1}^8 \lambda^i G^i_\mu,
\end{equation}
where $\lambda^i$ are the Gell-Mann matrices.
% Running coupling
% QCD potential
