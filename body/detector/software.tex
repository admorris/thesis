\section{Software}
\label{sec:detector:software}

The \lhcb software~\cite{LHCb-TDR-011} consists of several applications constructed in the Gaudi framework~\cite{Barrand:2001ny}.
This section describes the applications used for event simulation (Gauss), digitisation (Boole), reconstruction (Brunel) and analysis (DaVinci).
The \hlt, described in Section~\ref{sec:detector:hlt}, is also written in the Gaudi framework.
The flow of data between the applications is illustrated in Figure~\ref{fig:detector:software}.

\begin{figure}[h]
\includegraphics[width=\textwidth]{body/detector/figs/software_dotfig}
\caption{Data flow between the LHCb data processing applications.}
\label{fig:detector:software}
\end{figure}

\subsection{Simulation}
\label{sec:detector:simulation}

The \lhcb event simulation is performed by two applications: Gauss~\cite{LHCb-PROC-2011-006}, which generates the initial particles and simulates their passage through the \lhcb detector, and Boole, which simulates the detector response and outputs data in the same format as the \lhcb readout electronics.

% Gauss
Gauss simulates events in two independent steps: event generation and detector simulation.
The event generation phase uses tools built from external libraries with a generic interface.
By default, the generator simulates $\proton\proton$ collisions using Pythia~8~\cite{Sjostrand:2006za,Sjostrand:2007gs} and \PB meson decays using EvtGen~\cite{Lange:2001uf}.
The latter was originally developed by the BaBar collaboration for \PB mesons produced in the process $\ep\en \to \PUpsilon \to \B\Bbar$.
It was modified by the \lhcb collaboration to simulate \PB mesons produced in $\proton\proton$ collisions.
The output of this step is referred to in Chapters~\ref{ch:BsPhiPhi} and \ref{ch:BsPhiKK} as `generator-level' events.
The detector simulation phase uses Geant4~\cite{Agostinelli:2002hh,Allison:2006ve} to simulate the interactions that take place between the detector material and the particles produced in the collision.
The output of this step takes the form of `hits' in the subdetector sensors.

% Boole
Digitisation is the conversion of simulated subdetector hits to the same output format as the readout electronics.
This is performed by an application called Boole, which simulates the responses of the subdetector sensors, the readout electronics and the \lone trigger hardware.
The application accounts for the effects of bunch spacing, noise, cross-talk and dead channels.
After simulating the detector response, Boole then simulates the \lone trigger decision.
It is possible to configure the application to output all events or just those which pass the \lone.
The \hlt decisions are added to the event information using a package called Moore~\cite{LHCb-TDR-016}, which runs the same applications used during real data taking.

\subsection{Reconstruction}
\label{sec:detector:reconstruction}

% Brunel
The `raw' information from the readout electronics --- whether real or simulated --- is reconstructed using Brunel~\cite{LHCb-TDR-011}.
This application has access to the same information about the detector geometry as the simulation, ensuring consistency between the two.
The main reconstruction step begins with forming `clusters' of hits in the tracking detectors, which are passed to the tracking algorithms described in Section~\ref{sec:detector:trackreco}.
Reconstructed tracks are then passed to particle identification algorithms which use information from the calorimeters, \rich detectors and muon detectors.
A second step is performed for simulated events, which attempts to associate tracks with generator-level particles.
If more than a predetermined fraction of clusters used to build a given track are from the same generator-level particle, the track is associated with that particle, otherwise the track is classified as a `ghost'.
If multiple tracks are associated with the same particle, they are classified as `clones'.

\subsection{Analysis}
\label{sec:detector:analysis}

% Davinci
DaVinci~\cite{LHCb-TDR-011} is the application which converts the reconstructed events from Brunel to a format useful for physics analysis.
The application reconstructs primary vertices, assigns particle identification hypotheses to tracks and calorimeter clusters and performs event selection (see Section~\ref{sec:analysis:stripping}).
Several additional tools are available to analyse events within the application, including constrained vertex fitting, flavour tagging, calculation of decay angles and track momentum smearing and scaling.
For the analyses presented in this thesis, DaVinci is used to construct candidate \BdPhiKstar, \BsPhiPhi and \BsPhiKK decays.

