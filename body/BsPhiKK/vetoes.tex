\subsection{Removal of specific backgrounds}
\label{sec:BsPhiKK:vetoes}

Despite the particle identification requirements, there remain some background events from specific decay processes.
Several open charm and four-body charmless beauty decays, with a misidentified particle, are identified and vetoed.

The background contribution from the decay \decay{\Bs}{\Dsm \Kp}, with \decay{\Dsm}{\phiz \Km}, is removed by vetoing events with $|\mass{\Kp\Km  \Km}-\mass{\Dsm}|<24\mevcc$, corresponding to approximately a $\pm3\sigma$ window around the \Dsm mass.
Figure \ref{fig:BsPhiKK:DtophiKveto} shows the effect of this cut on the \mass{\Kp\Km  \Km} distribution.

Backgrounds from several decay modes with one or more tracks misidentified as a kaon are considered.
Specifically, these are \BdPhiKstar, \LbPhipK, \decay{\Dm/\Dsm}{\phiz \pim} and \decay{\Lc}{\phiz \proton}. % What about D-?
For each of the background sources, both of the tracks from the non-\phiz pair are considered as being misidentified pions or protons.
Vetoes or tighter PID cuts are applied in invariant mass windows constructed under both charge-conjugate hypotheses.

% Open charm backgrounds
The background contribution from \decay{\Dm/\Dsm}{\phiz \pim} is removed by vetoing events with $|\mass{\Kp\Km  \pim}-\mass{\Dsm}|<24\mevcc$ or $|\mass{\Kp\Km  \pim}-\mass{\Dm}|<24\mevcc$.
Figure \ref{fig:BsPhiKK:Dtophipiveto} shows the effect of this cut on the \mass{\Kp\Km  \pim} distribution.
Similarly, the contribution from \decay{\Lc}{\phiz \proton} is suppressed by requiring the non-\phiz kaon candidates to have $\prob{\PK}>\prob{\proton}$ for events where $|\mass{\Kp\Km  \proton}-\mass{\Lc}|<24\mevcc$.
Figure \ref{fig:BsPhiKK:Lctophipveto} shows the effect of this cut on the \mass{\Kp\Km  \proton} distribution.
It should be noted that the \decay{\Lc}{\phiz \proton} veto does not remove any events with $\mass{\Kp\Km}<1.8\gevcc$.

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/phiKminusM_mvaVars}
    \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/phiKminusM_mvaVars_vetoes}
    \caption[The \phiz\Km invariant mass spectrum before and after the mass vetoes.]{The \phiz\Km invariant mass spectrum before (left) and after (right) the mass vetoes.
             The prominent peak at $\sim 2.05 \gevcc$ is due to the decay \decay{\Dsm}{\phiz\pim}, visible in Figure~\ref{fig:BsPhiKK:Dtophipiveto}.}
    \label{fig:BsPhiKK:DtophiKveto}
  \end{center}
\end{figure}

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/phipiminusM_mvaVars}
    \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/phipiminusM_mvaVars_vetoes}
    \caption[The \phiz\pim invariant mass spectrum before and after the mass vetoes.]{The \phiz\pim invariant mass spectrum before (left) and after (right) the mass vetoes.}
    \label{fig:BsPhiKK:Dtophipiveto}
  \end{center}
\end{figure}

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/phipM_mvaVars}
    \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/phipM_mvaVars_vetoes}
    \caption[The \phiz\proton invariant mass spectrum before and after the mass vetoes.]{The \phiz\proton invariant mass spectrum before (left) and after (right) the mass vetoes.}
    \label{fig:BsPhiKK:Lctophipveto}
  \end{center}
\end{figure}
\FloatBarrier

% Beauty backgrounds
Two of the potential backgrounds peak under the \Bs peak under the four-kaon mass hypothesis.
For these, tighter track particle identification requirements are used instead of mass vetoes.
The values of these cuts were chosen to reject 90\% of the \BdPhiKstar and \LbPhipK simulation events following the cut-based selection.

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/phiKpiM_mvaVars}
    \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/phiKpiM_mvaVars_vetoes}
    \caption[The \phiz\Kp\pim invariant mass spectrum before and after the tighter PID cuts.]{The \phiz\Kp\pim invariant mass spectrum before (left) and after (right) the tighter PID cuts. The orange histogram is the \BdPhiKstar simulation with the same selection applied.}
    \label{fig:BsPhiKK:BdtophiKstarveto}
  \end{center}
\end{figure}
\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/phiKpM_mvaVars}
    \includegraphics[width=0.49\textwidth]{body/BsPhiKK/figs/phiKpM_mvaVars_vetoes}
    \caption[The \phiz\proton\Km invariant mass spectrum before and after the tighter PID cuts.]{The \phiz\proton\Km invariant mass spectrum before (left) and after (right) the tighter PID cuts. The orange histogram is the \LbPhipK simulation with the same selection applied.}
    \label{fig:BsPhiKK:LbtophiKpveto}
  \end{center}
\end{figure}

The background contribution from the decay \BdPhiKstar is rejected by requiring the non-\phiz kaon candidate to have $\prob{\PK}\times(1-\prob{\pi})>0.4$ for events where $|\mass{\Kp\Km \pip\Km}-\mass{\Bd}|<50\mevcc$, corresponding to approximately a $\pm3\sigma$ window around the \Bd mass.
Figure \ref{fig:BsPhiKK:BdtophiKstarveto} shows the effect of this cut on the \mass{\Kp\Km \Kp\pim} distribution.
Similarly, the contribution from \LbPhipK is rejected by requiring the non-\phiz kaon candidate to have $\prob{\PK}\times(1-\prob{\proton})>0.5$ for events where $|\mass{\Kp\Km \proton\Km}-\mass{\Lb}|<50\mevcc$.
Figure \ref{fig:BsPhiKK:LbtophiKpveto} shows the effect of this cut on the \mass{\Kp\Km \proton\Km} distribution.

\begin{table}[h]
  \begin{center}
    \caption[Efficiencies for the specific background removal.]{Efficiencies of the cuts designed to remove peaking backgrounds on signal simulation events with $\mass{\Kp\Km}<1.8\gevcc$ after stripping, triggers and the cuts in Section \ref{sec:BsPhiKK:cuts}.}
    \label{tab:BsPhiKK:vetoeff}
    \begin{tabular}{|l|c|}
    \hline
    & \multicolumn{1}{c|}{Fraction of events kept [$\%$]} \\
    \multicolumn{1}{|c|}{\raisebox{1.5ex}[1.5ex]{Background-specific cut}} & 
    \multicolumn{1}{c|}{\BsPhiKK simulation}  \\ \hline
    \BdPhiKstar               & \VetoEffBsphiKKMCmvaVarsBdPhiKstar \\
    \LbPhipK                  & \VetoEffBsphiKKMCmvaVarsLbPhiKp    \\
    \decay{\Lc}{\phiz\proton} & \VetoEffBsphiKKMCmvaVarsLcPhip     \\
    \decay{\Dsm}{\phiz\Km}    & \VetoEffBsphiKKMCmvaVarsDPhiK      \\
    \decay{\Dm}{\phiz\pim}    & \VetoEffBsphiKKMCmvaVarsDPhipi     \\
    \decay{\Dsm}{\phiz\pim}   & \VetoEffBsphiKKMCmvaVarsDsPhipi    \\
    \hline
    Combination of the above  &  \VetoEffBsphiKKMCmvaVarstotal \\ \hline
    \end{tabular}
  \end{center}
\end{table}

Table \ref{tab:BsPhiKK:vetoeff} summarises the efficiencies of each of the peaking background cuts on the \BsPhiKK simulation with $\mass{\Kp\Km}<1.8\gevcc$, after the cuts described in Section \ref{sec:BsPhiKK:cuts}.
The overall signal rejection rate is dominated by the cut designed to remove $90\%$ of the \BdPhiKstar background.
%This is necessary to reduce the size of this background to a level where it can be safely ignored in the amplitude fit, discussed further in Section~\ref{sec:BsPhiKK:peakbkgsize}.
%This approach is chosen, as to model it accurately would require knowledge of the $\Kp\pim$ spectrum and angular distributions of the various resonant contributions to the \decay{\Bd}{\phi\Kp\pim} decay.

\FloatBarrier
