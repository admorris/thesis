\section{Detection efficiency}
\label{sec:BsPhiPhi:efficiencies}

% Introduction
The branching fraction calculation, Equation~\ref{eq:BsPhiPhi:phiphiBR}, requires the ratio of the overall detection efficiencies for the signal and normalisation decay modes.
In this analysis, the total detection efficiency is the product of the generator, selection and particle identification (PID) efficiencies, $\varepsilon^\mathrm{tot} = \varepsilon^\mathrm{gen} \cdot \varepsilon^\mathrm{sel} \cdot \varepsilon^\mathrm{PID},$ which are calculated independently with different methods and datasets.
The generator and selection efficiencies are calculated using the simulation.
Since the simulation does not accurately reproduce the PID variables, the efficiency of the cuts on these variables is calculated using a data-driven technique.

\subsection{Generator efficiency}
\label{sec:BsPhiPhi:geneff}

% Introduction
During the event generator phase of the simulation, cuts are applied to the events in order to reduce the computing load required to fully simulate and digitise the sample.
Both the signal and normalisation simulation samples have cuts applied which ensure the final state particles are in the geometric acceptance of the detector.
The \BsPhiPhi sample has an additional cut that requires the \pt of the final state kaons to be above 400\mevc, which leads to a significantly lower generator-level efficiency than for the \BdPhiKstar sample.
The generator-level efficiencies are quoted relative to the number of events that would have been generated in the full $\eta$ range.

\begin{table}[h]
  \centering
  \caption[Generator-level efficiencies.]{Generator-level efficiencies (in \%) for the simulation samples used in the \BsPhiPhi branching fraction measurement and the \BdPhiPhi branching fraction limit.}
  \begin{tabular}{|c|c|c|c|c|}
    \hline
    Decay Mode & Polarity & \raisebox{-0.2ex}[1.5ex]{$\PB\;\varepsilon^\mathrm{gen}$} [\%] & \raisebox{-0.2ex}[1.5ex]{$\Bbar\;\varepsilon^\mathrm{gen}$} [\%] & Total $\varepsilon^\mathrm{gen} [\%]$ \\
    \hline
    \BdPhiKstar & Up   & $18.69 \pm 0.07$ & $18.67 \pm 0.07$ & \bf{$18.69 \pm 0.04$} \\
                & Down & $18.77 \pm 0.07$ & $18.63 \pm 0.07$ &                     \\
    \hline
    \BsPhiPhi   & Up   & $17.10 \pm 0.09$ & $17.05 \pm 0.09$ & \bf{$17.09 \pm 0.04$} \\
                & Down & $17.15 \pm 0.09$ & $17.06 \pm 0.09$ &                     \\
    \hline
    \BdPhiPhi   & Up   & $19.84 \pm 0.08$ & $19.83 \pm 0.08$ & \bf{$19.81 \pm 0.04$} \\
                & Down & $19.86 \pm 0.08$ & $19.70 \pm 0.08$ &                     \\
    \hline
  \end{tabular}
  \label{tab:BsPhiPhi:geneff}
\end{table}

%Results
Table~\ref{tab:BsPhiPhi:geneff} shows the generator-level cut efficiency of the signal and normalisation samples, calculated for each magnet polarity and \PB meson flavour.
The total efficiency of each decay mode is taken as the average of these numbers, since they are consistent with each other.
For comparison, as mentioned in Section~\ref{sec:detector:overview}, approximately 25\,\% of the $\bquark\bquarkbar$ pairs produced in \lhc collisions are in the acceptance of the \lhcb detector.

The ratio of generator efficiencies used in the \BsPhiPhi branching fraction calculation is:
\[
  \frac{\varepsilon^\mathrm{gen}_{\PhiKstar}}{\varepsilon^\mathrm{gen}_{\PhiPhi}} = 1.094 \pm 0.004.
\]
This is larger than unity due to the $\pt>400\mevc$ cut on the kaons in the \BsPhiPhi sample.
The effect of this cut is compensated for in the ratio of selection efficiencies, calculated in Section~\ref{sec:BsPhiPhi:seleff}.

\subsection{Selection efficiency}
\label{sec:BsPhiPhi:seleff}

%Method
The selection efficiency is determined by passing the simulation samples through the event selection described in Section~\ref{sec:BsPhiPhi:selection} without the cuts on the PID variables.
Fits are performed to the invariant mass distributions of the samples in order to determine the yields of selected events, which are divided by the numbers of events in the generator-level samples to give the selection efficiencies.
The numbers are shown in Table \ref{tab:BsPhiPhi:seleff}.

\begin{table}[h]
  \centering
  \caption[Selection efficiencies.]{Selection efficiencies for the MC samples used in the \BsPhiPhi branching fraction measurement.}
  \begin{tabular}{|c|c|c|c|}
    \hline
    Decay Mode   & Generated & Selected    & $\varepsilon^\mathrm{sel}$ [\%] \\
    \hline
    \BdPhiKstar  & 4027985 & $74758 \pm 273$ & $1.86 \pm 0.01$ \\
    \BsPhiPhi    & 2058745 & $49211 \pm 221$ & $2.39 \pm 0.02$ \\
    \hline
  \end{tabular}
  \label{tab:BsPhiPhi:seleff}
\end{table}

%Results
The ratio of selection efficiencies used in the \BsPhiPhi branching fraction calculation is
\[
  \frac{\varepsilon^\mathrm{sel}_{\PhiKstar}}{\varepsilon^\mathrm{sel}_{\PhiPhi}} = 0.776 \pm 0.006.
\]
This number deviates from unity for two reasons.
Firstly, as mentioned in Section~\ref{sec:BsPhiPhi:geneff} there is a difference in the cuts applied at the generator level: namely the \pt cut on the kaons in the \BsPhiPhi sample means that a larger proportion of events subsequently passes the later cut on the same variables.
Secondly, the pion from the \Kstar decay in the \BdPhiKstar sample has a much softer \pt spectrum than the kaons in either sample, which further reduces the efficiency of the \pt cut.

\subsection{Particle identification efficiency}
\label{sec:BsPhiPhi:pideff}

%Introduction
The signal and normalisation channels have similar kinematics and have been selected using similar selection requirements, with the difference lying in the PID.
Since the simulation does not accurately describe the hadron PID performance of the detector, it is necessary to use a data-driven method to calculate the efficiencies of the PID requirements.
This is standard practice in \lhcb analyses, and uses a tool called `PIDCalib'~\cite{LHCb-PUB-2016-021}.

%Method
A calibration sample of kaons and pions from \decay{\Dstarp}{\left(\decay{\Dz}{\Kpm\pimp}\right)\pip} decays is used to calculate the efficiency as a function of $p$, \pt, and $\eta$ of the track, as well as the multiplicity of tracks in the event and the polarity of the \lhcb magnet.
These values are translated to a per-event efficiency for each track in the \BsPhiPhi and \BdPhiKstar simulation samples after applying all the selection requirements described in Section~\ref{sec:BsPhiPhi:selection} apart from the PID cuts.

%Assumptions
This method of calculating the efficiencies of the PID cuts relies on three assumptions.
The first is that $p$, \pt, $\eta$, multiplicity and polarity are the only dependent variables.
The second is that the kinematic distributions agree between the simulation and data.
The third is that the binning scheme in the dependent variables is sufficiently fine to determine the PID efficiency to the required accuracy.
A systematic uncertainty, arising from the validity of these assumptions, is quantified in Section~\ref{sec:BsPhiPhi:pidsystematic}.

% Results
For the signal and normalisation modes, all kaons are required to have a value of ${\prob{\PK}\times(1-\prob{\Ppi})}$ greater than 0.025.
Figure~\ref{fig:BsPhiPhi:kaonPID_calib} compares the efficiencies for a range of cuts on ${\prob{\PK}\times(1-\prob{\Ppi})}$ between the particles in the calibration sample and simulated kaons from \BsPhiPhi decays.
The pion in the normalisation mode is required to have $\prob{\Ppi}>0.2$ and $\prob{\PK}<0.2$.
Figures~\ref{fig:BsPhiPhi:pion_calib} and~\ref{fig:BsPhiPhi:pik_calib} compare the efficiencies for a range of cuts on \prob{\Ppi} and \prob{\PK} between calibration tracks and simulated pions from \BdPhiKstar decays.

\begin{figure}[h]
  \centering
  \includegraphics[height=6.cm]{body/BsPhiPhi/figs/Kaon_calib.pdf}
  \includegraphics[height=6.cm]{body/BsPhiPhi/figs/Kaon_calib_MC_compare.pdf}
  \caption[Comparison of PID efficiencies for a range of cuts on ${\prob{\PK}\times(1-\prob{\Ppi})}$ between calibrated and simulated events.]
  {
    Comparison of PID efficiencies for a range of cuts on ${\prob{\PK}\times(1-\prob{\Ppi})}$ between data calibration sample and simulated events for kaons from the \BsPhiPhi decay, showing
    (left) data-calibrated PID efficiency and
    (right) difference between MC and data-calibrated efficiencies.
    Red vertical lines are placed at the cut values used in the selection.
  }
  \label{fig:BsPhiPhi:kaonPID_calib}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[height=6.cm]{body/BsPhiPhi/figs/pipi_calib.pdf}
  \includegraphics[height=6.cm]{body/BsPhiPhi/figs/pipi_calib_MC_compare.pdf}
  \caption[Comparison of PID efficiencies for a range of cuts on \prob{\Ppi} between calibrated and simulated events.]
  {
    Comparison of PID efficiencies for a range of cuts on \prob{\Ppi} between the data calibration sample and simulated events for pions from the \BdPhiKstar decay, showing
    (left) data-calibrated PID efficiency and
    (right) difference between MC and data-calibrated efficiencies.
    Red vertical lines are placed at the cut values used in the selection.
  }
  \label{fig:BsPhiPhi:pion_calib}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[height=6.cm]{body/BsPhiPhi/figs/pik_calib.pdf}
  \includegraphics[height=6.cm]{body/BsPhiPhi/figs/pik_calib_MC_compare.pdf}
  \caption[Comparison of PID efficiencies for a range of cuts on \prob{\PK} between calibrated and simulated events.]
  {
    Comparison of PID efficiencies for a range of cuts on \prob{\PK} between the data calibration sample and simulated events for pions from the \BdPhiKstar decay, showing
    (left) data-calibrated PID efficiency and
    (right) difference between MC and data-calibrated efficiencies.
    Red vertical lines are placed at the cut values used in the selection.
  }
  \label{fig:BsPhiPhi:pik_calib}
\end{figure}

%The PID cut efficiencies for the \BsPhiPhi simulation, calculated using both the simulated and data-driven methods, are summarised in Table~\ref{tab:BsPhiPhi:pid_eff_calib1}, with separate numbers quoted for each magnet polarity.
The simulated PID efficiency is found to be $(94.5 \pm 0.2)\,\%$, whereas the data-driven method gives a smaller efficiency of $(90.7 \pm 0.1)\,\%$.
The PID cut efficiency for the \BdPhiKstar sample, using the data-driven method, is $(84.9 \pm 0.1)\,\%$.
%The values for the different magnet polarities are summarised in Table~\ref{tab:BsPhiPhi:pid_eff_calib_PhiKst}.

%\begin{table}[h]
%  \centering
%  \caption[Comparison of PID efficiencies between calibrated and simulated \BsPhiPhi events.]
%  {
%    Comparison of PID efficiencies for the \BsPhiPhi decay using a data-driven PID calibration and using the MC PID simulation.
%    Only the trigger cuts have been applied to the MC sample.
%  }
%  \begin{tabular}{|c|c|c|c|}
%    \hline
%    \multirow{2}{*}{Source} & \multicolumn{3}{c|}{Efficiency [\%]} \\
%    & \multicolumn{1}{c}{Magnet up} & \multicolumn{1}{c}{Magnet down} & \multicolumn{1}{c|}{Total} \\
%    \hline
%    PID calibration & $90.4 \pm 0.1$ & $91.0 \pm 0.1$ & $90.7 \pm 0.1$ \\
%    MC Simulation   & $94.3 \pm 0.2$ & $94.6 \pm 0.2$ & $94.5 \pm 0.2$ \\
%    \hline
%  \end{tabular}
%  \label{tab:BsPhiPhi:pid_eff_calib1}
%\end{table}

%\begin{table}[h]
%  \centering
%  \caption[Comparison of PID efficiencies between calibrated and simulated \BdPhiKstar events.]
%  {
%    Comparison of PID efficiencies for the \BsPhiPhi decay using a data-driven PID calibration and using the MC PID simulation.
%    Only the trigger cuts have been applied to the MC sample.
%  }
%  \begin{tabular}{|c|c|c|c|}
%    \hline
%    \multirow{2}{*}{Source} & \multicolumn{3}{c|}{Efficiency [\%]} \\
%    & \multicolumn{1}{c}{Magnet up} & \multicolumn{1}{c}{Magnet down} & \multicolumn{1}{c|}{Total} \\
%     \hline
%    PID calibration & $84.7 \pm 0.1$ & $85.1 \pm 0.1$ & $84.9 \pm 0.1$ \\
%    \hline
%  \end{tabular}
%  \label{tab:BsPhiPhi:pid_eff_calib_PhiKst}
%\end{table}

The ratio of PID efficiencies is found to be
\[
  \frac{\varepsilon^\mathrm{PID}_{\BdPhiKstar}}{\varepsilon^\mathrm{PID}_{\BsPhiPhi}} = 0.936 \pm 0.001,
\]
using the results from the data-driven method, where the quoted uncertainty is statistical and arises due to the limited size of the calibration samples.

