\section{The Large Hadron Collider}
\label{sec:detector:lhc}

% Introduce the LHC as a machine and international project
The Large Hadron Collider (\lhc) \cite{Evans:2008zzb} is a 27\km circular collider at \cern.
Two beams, composed either of protons or lead ions, are brought to collision at four crossing points around the ring.
At each crossing point, a large detector is situated.
In clockwise order, from above, these are \atlas~\cite{Aad:2008zzm}, \alice~\cite{Aamodt:2008zz}, \cms~\cite{Chatrchyan:2008aa} and \lhcb~\cite{Alves:2008zz}.
The \atlas and \cms experiments are `general-purpose' detectors, with broad physics programmes that notably include Higgs boson, electroweak and \tquark quark physics, together with direct searches for supersymmetric and other non-Standard-Model particles.
The \alice experiment is focused on the physics of the quark--gluon plasma produced in heavy ion collisions.
The \lhcb experiment is designed to study decays of hadrons containing bottom and charm quarks, with particular focus on searching for non-Standard-Model sources of \CP violation and indirect evidence for new physics in rare decays.
In addition, there are three smaller experiments.
The \totem~\cite{Anelli:2008zza} experiment, located either side of \cms, measures the total elastic and diffuse cross section of $\proton\proton$ collisions.
The \lhcf~\cite{Adriani:2008zz} experiment, located either side of \atlas, measures neutral particle production for use in cosmic ray research.
Finally, \moedal~\cite{Pinfold:2009oia}, located in the \lhcb cavern, searches for magnetic monopoles.
% Some history
% Principles of particle acceleration?

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.9\textwidth]{body/detector/figs/CERN-accelerator-complex}
  \caption[The \lhc injector chain.]{A schematic representation of the \lhc injector chain. Solid lines represent accelerators, dotted lines represent transfer lines, and solid circles represent crossing points.}
  \label{fig:detector:accelerators}
\end{figure}

% Describe the accelerator chain leading to the LHC
The chain of accelerators comprising the \lhc complex is illustrated in Figure~\ref{fig:detector:accelerators}.
The proton beam starts in a linear accelerator, called Linac2, which takes protons from a hydrogen ion source and accelerates them to 50\mev.
The beam then traverses three successively larger, circular accelerators --- the 1.4\gev Proton Synchrotron Booster (PSB), the 28\gev Proton Synchrotron (PS) and the 450\gev Super Proton Synchrotron (SPS).
The beams are composed of bunches of $\order(10^{11})$ protons, with a minimum spacing between bunches of 25\ns.
An \lhc beam is composed of a maximum of 2808 bunches, accounting for gaps corresponding to kicker magnet rise times in the injector chain and the beam dump system.
Ion beams start at a different linear accelerator, Linac3, and traverse the Low-Energy Ion Ring (LEIR) before following the same route as proton beams through the PS and SPS.

% Details about the LHC: magnets, RF system, lumi levelling, etc
The \lhc receives 450\gev protons from the SPS and further accelerates them using superconducting radio-frequency cavities.
The design beam energy is 7\tev, however 6.5\tev is the highest energy achieved so far.
The ring has eight bending arcs, with superconducting dipole magnets capable of generating magnetic fields up to {8.3\,T}.
Multipole magnets are used to focus the beam.
Between the bending arcs are straight sections: four have crossing points, two are used for collimation, one for acceleration and the last for the beam dump system.
From a top-down perspective, beam 1 circulates clockwise and is injected just before it traverses through \alice.
Beam 2 circulates anticlockwise and is injected just before it traverses \lhcb, \ie `downstream' from the detector in the coordinate system defined in Section~\ref{sec:detector:coordinates}.

%\subsection{Operational history and luminosity}
%\label{sec:detector:luminosity}

\begin{figure}[h]
  \centering
  \includegraphics[width=0.9\textwidth]{body/detector/figs/luminosity}
  \caption[Integrated luminosity recorded by \lhcb.]{Evolution of integrated luminosity of proton--proton collisions recorded by the \lhcb detector.
           The legend shows the beam energy and integrated luminosity for each year.}
  \label{fig:detector:luminosity}
\end{figure}

% Operational history: delivered luminosity and collision energies
The first beams circulated in the \lhc during 2008, with first collisions in 2009 at a beam energy of 450\gev.
The beam energy in 2010 and 2011 was 3.5\tev, rising to 4\tev in 2012.
During this period, referred to as Run 1, a total integrated luminosity of 3.2\invfb of proton--proton collisions was recorded by \lhcb, of which 1.15\invfb was collected at a centre-of-mass energy of $\sqs=7\tev$ and 2.1\invfb at 8\tev.
In 2015, after a two-year shutdown for maintenance and upgrades, Run 2 of the \lhc started.
By the end of 2016, \lhcb had collected 2.0\invfb of proton--proton collision data at $\sqs=13\tev$.
The evolution of recorded integrated luminosity is shown in Figure~\ref{fig:detector:luminosity}.

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{body/detector/figs/Fill_Lumi.png}
  \caption[Luminosity during a typical \lhc fill.]{Evolution of the instantaneous luminosity at \lhcb (green), \atlas (blue) and \cms (purple) during a typical `fill' in Run 1.
           Note that the instantaneous luminosity at \lhcb is constant until about 14 hours, when the beams have zero separation.
           }
  \label{fig:detector:luminosity_levelling}
\end{figure}

% Luminosity levelling
The \lhc delivers a constant instantaneous luminosity to \lhcb, using a technique called `luminosity levelling' \cite{Follin:1955354}.
The lateral separation of the beams at the crossing point is adjusted periodically to achieve a constant target luminosity.
As the beam intensity reduces, the separation reduces accordingly.
The effect of luminosity levelling is demonstrated in Figure~\ref{fig:detector:luminosity_levelling}.
%The target luminosity for \lhcb is mainly determined by the desired amount of pileup.
During 2011, the target luminosity was $3.5 \times 10^{32} \invcma\invsec$, increasing to $4.0 \times 10^{32} \invcma\invsec$ in 2012.
Luminosity levelling is performed so that events predominantly contain a single collision, which makes them easier to reconstruct and allows for more precise measurements.

