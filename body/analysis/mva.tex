\section{Multivariate selection}
\label{sec:analysis:mva}
The data after stripping contains a large contribution from combinatorial and misidentified events.
This is reduced by applying more stringent requirements on discriminating variables.
Both analyses impose tighter PID requirements on the tracks, \eg $\prob{\PK}\times(1-\prob{\pi})>0.025$ for kaon candidates, and windows on the two- and four-body invariant masses, \eg $\left|\mass{\Kp\Km} - \mass{\phiz}\right| < 15\mevcc$ for \phiz candidates.
Following this, multivariate algorithms are used to further reject combinatorial background events.
Multivariate algorithms can provide much stronger background rejection and signal retention than simple `rectangular' cuts on the same variables.

Algorithms provided by the TMVA package~\cite{Höcker:1019880} are trained to distinguish between signal and combinatorial background events using multiple discriminating variables.
%training samples
The training samples consist of simulated \PB decays for the signal and data in a region of invariant mass which is expected to contain purely combinatorial background.
The samples are split in two, with one half used for training and the other for testing.
% variables
The input variables to the MVA algorithms are chosen based on their ability to discriminate between signal and combinatorial background and how well they agree between simulation and data.
%overtraining
Care must be taken to avoid `overtraining', which is a form of bias where the algorithm interprets statistical fluctuations in the training sample as being significant.
Overtraining can be identified by a difference in performance between the training and test samples.

The types of MVA algorithm used in this thesis are boosted decision trees (BDT)~\cite{Breiman,Roe} and multi-layer perceptrons (MLP), a type of artificial neural network (ANN)~\cite{bishop2006pattern}.

%BDTs
A decision tree is a graph whose nodes represent decisions, and the connections between the nodes represent the outcomes of those decisions, much like a flowchart.
`Boosting' is a technique of combining a set of `weak' classifiers to form a strong classifier\footnote{A classifier is an algorithm that identifies which category the input belongs to. In the context of event selection, a classifier decides whether an event is signal or background. A weak classifier is one that performs slightly better than random chance.}.
A BDT algorithm builds decision trees in an iterative process based on a weighted dataset.
Misclassified events are weighted more strongly for the next tree.
The result is a large number of decision trees: each individual tree is a weak classifier, but their combination should in principle be a strong classifier.
An event is assigned a `score' based on that proportion of these trees that classified it as signal.

%ANNs
An ANN consists of a collection of connected `neurons', which are functions that have a number of inputs and a single output.
Each neuron has an associated set of weights that are used to perform a weighted sum of the inputs.
The output of the neuron is the `activation function' evaluated on the weighted sum.
Training consists of finding the optimal set of weights for each neuron.
A feed-forward neural network consists of ordered `layers' of neurons, with the outputs of the neurons from one layer feeding into the inputs of those in the next.

%MLP
A MLP is a feed-forward ANN with at least 3 layers, including the input and output layers.
The activation function is typically sigmoid (\ie `S'-shaped), \eg $(1+e^{-x})^{-1}$ or $\operatorname{erf}(x)$, since these are useful for generally approximating non-linear functions~\cite{HORNIK1989359}.
Training is typically performed using back-propagation~\cite{Rumelhart:1988:LRB:65669.104451}, which is an iterative procedure that adjusts weights based on how much the output of the network differs from the desired output.

%evaluating the output and choosing an algorithm
The performance of an algorithm can be determined from its curve in the space formed by background rejection and signal efficiency, called a receiver operating characteristic (ROC) curve.
Better performance is indicated by the curve's proximity to the ideal performance of 100\% background rejection and signal efficiency.

%determining the optimal cut
Optimal cuts on the MVA algorithm output are determined by maximising a figure of merit, the choice of which depends on the goal of the analysis.
A common figure of merit, used in both analyses presented in this thesis, is the `signal significance' figure of merit: the ratio of the number of signal candidates to the Poisson error on the sample size.
For cases where the number of signal candidates is unknown, such as with \BdPhiPhi, the `Punzi' figure of merit \cite{Punzi:2003bu} is used.
This uses the signal efficiency and Poisson error on the number of background events.

