\section{Systematic uncertainties}
\label{sec:BsPhiKK:systematics}

The systematic uncertainties on each fit parameter are summarised in Table~\ref{tab:BsPhiKK:systematics}.
The uncertainties are grouped together as arising from the choice of resonances in the signal model, choice of resonance parameters, modelling the detector effects and modelling the background.

The systematic uncertainty arising from the choice of resonances is found by taking the largest deviation on each fit parameter when using either of the two next-best fit models.
These are summarised in the `model' column of Table~\ref{tab:BsPhiKK:systematics}.
This is the dominant source of systematic uncertainty for the fit fraction and $|\Azero|$ of the \BsPhiftwop component.
Therefore, it is quoted as a separate uncertainty on the results presented in Section~\ref{sec:BsPhiKK:results}.

The uncertainty arising from the acceptance function is found by generating a toy sample of 100,000 events from Model II and fitting it 100 times with each coefficient, given in Table~\ref{tab:BsPhiKK:acceptance}, varied randomly within its statistical uncertainty.
This results in distributions for each of the fit parameters.
The standard deviations of these distributions are taken as the uncertainty due to the acceptance coefficients.
The uncertainty arising from the resolution function is found by performing two fits to the data with the parameter $\sigma_0$, defined in Equation~\ref{eq:BsphiKK:resolution}, fixed to the upper or lower bound of its uncertainty.
The largest deviation on each parameter from either of these fits is taken as a systematic uncertainty.
The acceptance and resolution systematic uncertainties are added in quadrature to give the `detector' uncertainty in Table~\ref{tab:BsPhiKK:systematics}.

The fit is repeated with varied Blatt--Weisskopf barrier factor radii for the \Bs meson and the $\Kp\Km$ resonance.
Two sets of values were chosen: $r_\Bs=1.0\invgevc$ and $r_{\Kp\Km}=1.8\invgevc$, commonly used by BaBar, and $r_\Bs=1.5\invgevc$ and $r_{\Kp\Km}=5.0\invgevc$, used in the \lhcb analysis of \decay{\Bs}{\jpsi\Kp\Km} in Ref.~\cite{LHCb-PAPER-2012-040}.
The largest deviation on each parameter is taken as the systematic uncertainty.
The fit is repeated with two sets of Flatt\'e parameters for the \fzero{980} resonance: $m = 953 \pm 20 \mev$, $g_{\pi\pi} = 329 \pm 96 \mev$ and $R_g = 2$, measured by CLEO~\cite{Bonvicini:2007tc}, and $m = 965 \pm 10 \mev$, $g_{\pi\pi} = 165 \pm 18 \mev$ and $R_g = 4.21 \pm 0.33$ measured by BES~\cite{Ablikim:2004wn}.
The largest deviation on each fit parameter is taken as a systematic uncertainty.
The mass and width of the $\phiz(1680)$, which are fixed to the PDG averages in the nominal fit, are allowed to float with Gaussian constraints, and the deviations from the nominal result are taken as systematic uncertainties.
Finally, the mass and width of the \fzero{1500}, by default fixed to an LHCb measurement~\cite{LHCb-PAPER-2013-069}, are fixed to the PDG averages, and the deviations from the nominal result are taken as systematic uncertainties.
The uncertainties from the choice of Blatt--Weisskopf barrier factor radii and other resonance parameters are summed in quadrature to give the `params' uncertainty in Table~\ref{tab:BsPhiKK:systematics}.

The systematic uncertainty due to the background model is found by repeating the fit to data with the fraction of signal candidates and the size of each peaking background constrained to the values found in the fit to the $\Kp\Km\Kp\Km$ invariant mass, given in Section~\ref{sec:BsPhiKK:massfits}.
Another fit is performed assuming the background is entirely combinatorial.
The deviations on each parameter are summed in quadrature and given as the `background' uncertainty in Table~\ref{tab:BsPhiKK:systematics}.

\newcommand{\E}[1]{\times 10^{#1}}
\begin{table}
  \centering
  \caption[Summary of the systematic uncertainties on each of the fit parameters for the best fit model.]{Summary of the systematic uncertainties on each of the fit parameters for the best fit model. The column `model' lists the uncertainties due to the choice of resonances, `params' refers to the choice of resonance parameters, `detector' refers to the uncertainties due to acceptance and resolution, and `background' refers to the uncertainties due to the size of the combinatorial and peaking backgrounds.}
  \label{tab:BsPhiKK:systematics}
  \begin{tabular}{|l|c|c|c|c|}
    \hline
    Fit parameter            & Model       & Params    & Detector & Background \\
    \hline\multicolumn{5}{|c|}{Breit--Wigner parameters}\\\hline
    $\phi(1020)$ $m$ [\mevcc]    & 0.0010  & 0.0020 & 0.0010   & 0.0010     \\
    $\phi(1020)$ $\Gamma$ [\mev] & 0.003   & 0.005  & 0.0021   & 0.0008     \\
    \ftwop{1525} $m$ [\mevcc]    & 4       & 1.3    & 0.08     & 0.07       \\
    \ftwop{1525} $\Gamma$ [\mev] & 2       & 0.5    & 0.4      & 0.06       \\
    \hline\multicolumn{5}{|c|}{Fit fractions}\\\hline
    nonresonant             & 0.015       & 0.021  & 0.0008   & 0.0011     \\
    \fzero{980}              & 0.033       & 0.007  & 0.0003   & 0.0015     \\
    $\phiz(1020)$            & 0.004       & 0.021  & 0.005    & 0.0009     \\
    \fzero{1500}             & ---         & 0.004  & 0.0003   & 0.0005     \\
    \ftwop{1525}             & 0.011       & 0.004  & 0.0009   & 0.0005     \\
    $\phiz(1680)$            & 0.006       & 0.005  & 0.0007   & 0.0006     \\
    \hline\multicolumn{5}{|c|}{Magnitudes}\\\hline
    $\phiz(1020)$ $|A_+|$    & $1.0\E{-6}$ & $1.4\E{-6}$  & 0.0026    & $1.0\E{-6}$\\
    $\phiz(1020)$ $|A_0|$    & $2.0\E{-6}$ & $2.5\E{-6}$  & 0.003     & $1.0\E{-6}$\\
    \ftwop{1525} $|A_+|$     & 0.03        & 0.016  & 0.004    & 0.004      \\
    \ftwop{1525} $|A_0|$     & 0.016       & 0.006  & 0.0018   & 0.0011     \\
    $\phiz(1680)$ $|A_+|$    & 0.06        & 0.015  & 0.0015   & 0.0031     \\
    $\phiz(1680)$ $|A_0|$    & 0.07        & 0.03   & 0.0016   & 0.004     \\
    \hline\multicolumn{5}{|c|}{Phases}\\\hline
    nonresonant $\delta_0$  & 0.0022      & 0.05   & 0.0016   & 0.0030     \\
    \fzero{980} $\delta_0$   & 0.4         & 0.22   & 0.009    & 0.04       \\
    $\phiz(1020)$ $\delta_+$ & 0.005       & 0.012  & 0.0008   & 0.0011     \\
    \fzero{1500} $\delta_0$  & ---         & 0.7    & 0.010    & 0.018      \\
    \ftwop{1525} $\delta_+$  & 0.4         & 0.16   & 0.006    & 0.014      \\
    \ftwop{1525} $\delta_0$  & 0.31        & 0.11   & 0.009    & 0.027      \\
    \ftwop{1525} $\delta_-$  & 0.2         & 0.14   & 0.012    & 0.023      \\
    $\phiz(1680)$ $\delta_+$ & 0.4         & 0.08   & 0.006    & 0.016      \\
    $\phiz(1680)$ $\delta_0$ & 0.5         & 0.4    & 0.004    & 0.04       \\
    $\phiz(1680)$ $\delta_-$ & 0.4         & 0.22   & 0.007    & 0.030      \\
    \hline
  \end{tabular}
\end{table}

\FloatBarrier
