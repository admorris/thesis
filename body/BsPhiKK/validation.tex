\section{Fit model validation}
\label{sec:BsPhiKK:validation}

The fit model is validated in three steps.
First, samples of toy events are generated from the signal PDF using reasonable starting values, which are then fitted in order to check for consistency and biases.
Second, fits are performed to generator-level simulation samples of \BsPhiPhi and nonresonant \BsPhiKK decays to validate the matrix element calculation.
Third, fits are performed to the fully simulated and fully selected simulation samples of the same decays, in order to validate the implementation of the mass resolution and acceptance function in the fitter.

\subsection{Toy studies}
\label{sec:BsPhiKK:toys}

An important step in the validation is to fit a sample of toy events generated from the PDF itself and check that the results agree with the input values.
If the results are not consistent with the starting values, it indicates that there are systematic biases, lack of sensitivity to that particular fit parameter, or even mistakes in the code.
The number of toys generated in each study is 4000.
This is roughly the same size as the data sample, allowing for comparable statistical uncertainties.
For each toy study, approximately 100 samples are generated and fitted, and a distribution of `pulls' is obtained for each free parameter.
The pulls are calculated as $$\text{pull} = \frac{V_\text{obs} - V_\text{exp}}{\sigma V_\text{obs}},$$ where $V_\text{obs}$ is the observed (fitted) value, $V_\text{exp}$ is the expected (generated) value, and $\sigma V_\text{obs}$ is the uncertainty on $V_\text{obs}$ as reported by the fitter.
The pull distribution should have a mean $\mu = 0$ and standard deviation $\sigma=1$.
Deviations from $\mu=0$ indicate biases in the fit, and deviations from $\sigma=1$ indicate that the incorrect uncertainty is reported by the fitter.
In the case of the latter, uncertainties on fitted parameters should be calculated by running toy studies and taking the standard deviation on the distributions of $V_\text{obs}$.

Toy events are generated from the signal PDF without acceptance or resolution effects for the exclusive processes \BsPhifzero, \decay{\Bs}{\phiz\fzero{1500}}, \BsPhiPhi and \BsPhiftwop in order to validate the Flatt\'e and spin-0, 1 and 2 Breit--Wigner lineshapes.
For the \BsPhifzero toys, the Flatt\'e coupling parameters $g_{\pi\pi}$ and $R_g = \frac{g_{KK}}{g_{\pi\pi}}$ were floated, and the pull distributions had means and standard deviations of $0.11 \pm 0.89$ and $0.02 \pm 1.06$, respectively.
The results of the toy studies using the other processes are shown in Table~\ref{tab:BsPhiKK:toyampres}.
The standard deviations on the pull distributions of $|\Aplus|$ and \deltaplus of the \BsPhiftwop study indicate that the uncertainties on these parameters are over-estimated by the fitter by a factor of two.

\begin{table}[h]
  \centering
  \caption[Results of toy studies without detector effects.]{Results of toy studies without detector effects. For each parameter, the mean, $\mu$, and standard deviation, $\sigma$, of the pull distribution is shown.}
  \label{tab:BsPhiKK:toyampres}
  \begin{tabular}{|c|r|r|r|}
    \hline &
    \multicolumn{3}{c|}{$\mu\pm\sigma$ of pulls} \\
    \multicolumn{1}{|c|}{\raisebox{1.5ex}[1.5ex]{Parameter}} &
    \multicolumn{1}{c}{\decay{\Bs}{\phiz\fzero{1500}}} &
    \multicolumn{1}{c}{\BsPhiPhi} &
    \multicolumn{1}{c|}{\BsPhiftwop} \\ \hline
    $m$           & $-0.04 \pm 0.95$ & $ -0.01 \pm 1.02 $ & $0.17 \pm 0.94$  \\
    $\Gamma$      & $0.03 \pm 0.87$  & $ 0.13 \pm 0.98 $  & $-0.19 \pm 0.97$ \\
    $|\Azero|$    & $-$              & $ 0.05 \pm 0.93 $  & $0.07 \pm 0.96$  \\
    $|\Aplus|$    & $-$              & $ -0.08 \pm 1.11 $ & $-0.01 \pm 0.52$ \\
    $\deltaplus$  & $-$              & $ 0.03 \pm 1.06 $  & $-0.09 \pm 0.52$ \\
    \hline
  \end{tabular}
\end{table}

To test that the detector effects in the PDF, namely the acceptance and \mass{\Kp\Km} resolution, toy studies are performed using the \BsPhiPhi PDF with each effect included individually and together.
The results of this are shown in Table~\ref{tab:BsPhiKK:toyampresdet}.

\begin{table}[h]
  \centering
  \caption[Results of toy studies with detector effects.]{Results of toy studies using the \BsPhiPhi PDF with detector effects. For each parameter, the mean, $\mu$, and standard deviation, $\sigma$, of the pull distribution is shown.}
  \label{tab:BsPhiKK:toyampresdet}
  \begin{tabular}{|c|r|r|r|}
    \hline &
    \multicolumn{3}{c|}{$\mu\pm\sigma$ of pulls} \\
    \multicolumn{1}{|c|}{\raisebox{1.5ex}[1.5ex]{Parameter}} &
    \multicolumn{1}{c}{Acceptance} &
    \multicolumn{1}{c}{Resolution} &
    \multicolumn{1}{c|}{Both} \\ \hline
    $m$           & $0.06 \pm 1.02$    & $0.03\pm1.01$  & $0.02\pm1.03$ \\
    $\Gamma$      & $0.02 \pm 0.93$    & $0.02\pm0.85$  & $-0.14\pm1.05$ \\
    $|\Azero|$    & $-0.04 \pm 0.84$   & $0.02\pm1.04$  & $0.05\pm0.94$ \\
    $|\Aplus|$    & $0.09 \pm 0.86$    & $-0.16\pm0.94$ & $-0.12\pm0.86$ \\
    $\deltaplus$  & $0.07 \pm 0.88$    & $-0.07\pm0.90$ & $-0.06\pm0.96$ \\
    \hline
  \end{tabular}
\end{table}

\subsection{Fits to simulation}
\label{sec:BsPhiKK:MCfits}

The generator-level simulation samples are not subject to the simulated \lhcb detector, hence they can be used to validate the amplitude calculation independently from the resolution and acceptance effects.
The \BsPhiPhi simulation sample is generated using the {\tt PVV\_CPLH} model in EvtGen~\cite{Lange:2001uf} with the parameters given in Table~\ref{tab:BsPhiKK:genparams}.
The helicity amplitude parameters come from the CDF result~\cite{Aaltonen:2011rs}.

\begin{table}[h]
  \centering
  \caption[Physics parameters used in generating the \BsPhiPhi simulation sample.]{Physics parameters used in generating the \BsPhiPhi simulation sample. The symbol $\tau$ denotes lifetime, $L$ and $H$ denote the light and heavy \Bs mass eigenstates, and $r_\Bs$ and $r_{\Kp\Km}$ are Blatt--Weisskopf barrier factor radii.}
  \label{tab:BsPhiKK:genparams}
  \begin{tabular}{|l|l|}\hline
    % βs  η   |A‖|    δ‖   |A0|   δ0   |A⊥|    δ⊥
    %  0  1  0.536  2.71  0.590  0.0  0.604  2.39
    Parameter & Value\\\hline
    $|\Apara|$ & 0.536 \\
    $|\Azero|$ & 0.590 \\
    $|\Aperp|$ & 0.604 \\
    $\deltapara$ & 2.71 \\
    $\deltazero$ & 0.0 \\
    $\deltaperp$ & 2.39 \\
    $m_\phi$ & 1.019461\gevcc\\
    $\tau_\phi$ & $1.5451 \times 10^{-22}$\sec\\
    $\tau_L$ & 1.405\ps \\
    $\tau_H$ & 1.661\ps \\
    $r_\Bs$ & 1.0 \invgevc \\
    $r_{\Kp\Km}$ & 3.0 \invgevc \\
    \hline
  \end{tabular}
\end{table}

The signal PDF is, in principle, sensitive to two of the magnitudes of the helicity amplitudes, one of the phases, the mass and width of the $\phi$, the width splitting of the \Bs mass eigenstates, $\Delta\Gamma_s = \Gamma_L - \Gamma_H$, and the barrier factor radii.
A fit is made to a sample of generator-level \BsPhiPhi events with these parameters free in the fit, with the exception of $\Delta\Gamma_s$ and the \Bs barrier factor radius.
The results are shown in Table~\ref{tab:BsPhiKK:genfit}.

\begin{table}[h]
  \centering
  \caption[Results the fit to generator-level simulation.]{Results of the fit to the \BsPhiPhi generator-level simulation}
  \label{tab:BsPhiKK:genfit}
  \begin{tabular}{|c|c|r|}
    \hline Parameter & Fit result                                         & $\sigma$ from input \\ \hline
    $m$              & $1019.448 \pm 0.004$\mevcc                         & $-2.9$              \\
    $\Gamma$         & $\phantom{101}4.269 \pm 0.009$\mev\phantom{$c^2$}  & 0.33                \\
    $|\Azero|$       & $\phantom{101}0.7924 \pm 0.0007\phantom{\invgevc}$ & $-2.2$              \\
    $|\Aplus|$       & $\phantom{101}0.5899 \pm 0.0007\phantom{\invgevc}$ & $-0.13$             \\
    $\deltaplus$     & $\phantom{101}2.532 \pm 0.004\phantom{\invgevc}$   & $-1.8$              \\
    $r_{\Kp\Km}$     & $\phantom{101}3.08 \pm 0.08$\invgevc               & 1.1                 \\ \hline
  \end{tabular}
\end{table}

The difference between the generator-level and fully simulated and fully selected simulation samples is due to acceptance and resolution.
To check how well the PDF models these effects, a fit is performed to the fully selected \BsPhiPhi simulation sample with the $\phiz(1020)$ mass, width and helicity amplitude parameters left free.
The results are shown in Table~\ref{tab:BsPhiKK:simfit}.

\begin{table}[h]
  \centering
  \caption[Results the fit to the full simulation.]{Results of the fit to the full \BsPhiPhi simulation}
  \label{tab:BsPhiKK:simfit}
  \begin{tabular}{|c|c|r|}
    \hline Parameter & Fit result                                       & $\sigma$ from input \\ \hline
    $m$              & $1019.42 \pm 0.02$\mevcc                         & $-2.2$              \\
    $\Gamma$         & $\phantom{101}4.3 \pm 0.3$\mev\phantom{$c^2$}    & 1.4                 \\
    $|\Azero|$       & $\phantom{101}0.7917 \pm 0.0027\phantom{\mevcc}$ & $-1.6$              \\
    $|\Aplus|$       & $\phantom{101}0.5904 \pm 0.0027\phantom{\mevcc}$ & 0.13                \\
    $\deltaplus$     & $\phantom{101}2.508 \pm 0.016\phantom{\mevcc}$   & $-2.1$              \\ \hline
  \end{tabular}
\end{table}

The deviations on the parameters $m$, $|\Azero|$ and $\deltaplus$ suggest that there are biases in the fit model which were not evident from Section~\ref{sec:BsPhiKK:toys}.
These could be due to \eg a difference in formalism between EvtGen and the PDF.
This is an issue that must be resolved before the analysis can be published.

\FloatBarrier
