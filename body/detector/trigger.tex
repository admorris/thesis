\section{Trigger}
\label{sec:detector:trig}

%Introduce the trigger
While the maximum bunch crossing frequency of the \lhc is 40\mhz, the nominal frequency of interactions visible to the \lhcb detector, assuming $\sqs=14\tev$ and $\mathcal{L}=200\invnb\invsec$, is about 10\mhz.\footnote{The target luminosity during Run 1 was about twice this. See Section~\ref{sec:detector:lhc}.}
An interaction is defined as visible if there are at least two charged particles that leave enough hits in the \velo and tracking stations to be reconstructed.
The rate of visible interactions is lower than the maximum bunch crossing frequency due to several factors, including `abort gaps' in the beam\footnote{Gaps in the beam that allow for kicker magnets in the \lhc and its injectors to operate.} and the fact that many interactions are elastic or diffractive scatters.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.4\textwidth]{body/detector/figs/LHCb_Trigger_RunIAlgDetail_May2015}
  \caption{A summary of the trigger layers, decisions and rates for Run~1.}
  \label{fig:detector:trigger}
\end{figure}

The role of the trigger~\cite{LHCb-PUB-2014-046} is to reduce the rate at which events are read out of the detector and written to storage to 2 -- 5\khz.
This is done at two levels: the hardware `level zero' trigger (\lone) and the software `high-level' trigger (\hlt).
The design of the trigger is illustrated in Figure~\ref{fig:detector:trigger}.
The \lone uses custom electronics which synchronise with the 40\mhz bunch crossing frequency, whereas the \hlt runs asynchronously on a processor farm.
The nominal rate of $\bquark\bquarkbar$ pairs produced within the acceptance of the detector is about 100\khz.
Of these, about 15\,\% contain a \PB meson with all of its decay products contained within the acceptance. % TODO meson or hadron?
The branching fractions of decay modes of interest to the experiment are typically below $10^{-3}$, hence a selective trigger is needed to select this small fraction of interesting events with high efficiency while also rejecting background events.

A set of conditions which, if satisfied, causes any level of the trigger to pass the event to the next stage, or write it to storage, is referred to as a `trigger line'.

\subsection{Hardware trigger}
\label{sec:detector:lzero}

%Introduce L0
The \lone trigger reduces the rate at which the entire detector is read out to 1\mhz.
It is divided into two independent systems: the calorimeter trigger and the muon trigger.
These feed into the \lone decision unit \cite{Cornat:691537}, which makes the final decision whether to pass the full event to the \hlt or not.
The decision unit allows for prescaling and the overlapping of several conditions.
It has 2\mus in which to make a decision, after accounting for the latency of the electronics, particle flight time and cable lengths.
The calorimeter and muon triggers take advantage of the fact that the decay products of \bquark hadrons typically have large transverse energy, \et, and transverse momentum, \pt.

\begin{table}[htb]
  \centering
  \caption[Typical L0 thresholds used in Run 1]{Typical L0 thresholds used in Run 1~\cite{LHCb-PUB-2014-046}.}
  \begin{tabular}{|l|c|c|c|}
    \hline
    \multirow{2}{*}{Decision} & \multicolumn{2}{c|}{\pt or \et threshold}            & \multicolumn{1}{c|}{SPD hits}\\
                              & \multicolumn{1}{c}{2011} & \multicolumn{1}{c|}{2012} & \multicolumn{1}{c|}{2011 and 2012}\\
    \hline
    single muon&1.48\gevc&1.76\gevc& 600\\
    dimuon $\pt_1 \times \pt_2$ &$(1.30\gev/c)^2$&$(1.60\gev/c)^2$&900\\
    hadron   & 3.50\gev&3.70\gev&600\\
    electron & 2.50\gev&3.00\gev&600\\
    photon   & 2.50\gev&3.00\gev&600\\
    \hline
  \end{tabular}
  \label{tab:detector:l0cuts}
\end{table}

%L0 CALO
The calorimeter trigger looks for high \et electrons, photons, neutral pions and other hadrons.
It forms clusters by adding the \et of $2 \times 2$ cells in the \ecal and \hcal and selects the clusters with the highest \et.
Clusters are identified as electrons, photons or hadrons using information from the \spd and \presh.
Hadrons are identified from the \hcal cluster with the highest \et, including the \et from the matching \ecal cluster.
Electromagnetic showers are selected from the highest \et \ecal cluster with one or two hits in the \presh.
Electrons are discriminated from photons by requiring that there be a matching hit in the \spd.
The trigger reads out the event if the \et of any candidate is above a certain threshold.
Typical values from Run 1 are given in Table~\ref{tab:detector:l0cuts}.
The total number of hits in the \spd is used to veto high-multiplicity events that would take too long to process in the \hlt.

%L0 MUON
The muon trigger looks for the two highest \pt muons in each quadrant.
The \muondetector system is able to perform stand-alone track reconstruction, and the first two stations are used to measure \pt in the trigger.
Tracks are built from hits that form a straight line through all five stations and must be consistent with originating from the interaction point.
An event is triggered if either the highest \pt of any muon candidate in the event is above a certain threshold, typically 1.5--1.8\gevc, or the product of the highest and second-highest \pt is above another threshold, typically $(1.3\text{--}1.6\gevc)^2$.

\subsection{Software trigger}
\label{sec:detector:hlt}

The \hlt reduces the event rate from 1\mhz to 2--5\khz using the full event data.
It is split into two layers: \hltone and \hlttwo~\cite{LHCb-TDR-016}.

% HLT1
The \hltone step refines candidates found at \lone using reconstructed candidate tracks and vertices from the \velo and tracking stations.
The \lone decision unit passes information about which trigger conditions were met, and \hltone uses this information to decide which sequence of algorithms to run.
The algorithms in each sequence work to verify the decision made by the previous step using progressively more of the event information.
If multiple \lone candidates were found, then the corresponding sequences will be run independently.
To minimise CPU time, the \hlt ensures that the same track or vertex is not reconstructed twice.
Additional \pt and impact parameter requirements reduce the output rate of \hltone to 80\khz.

% hlt1 track all decision
The `single-track all \lone' line~\cite{LHCb-PUB-2011-003}, used in both analyses presented in this thesis, looks for a single detached high-momentum track.
This line starts with \velo tracks and applies cuts on impact parameter, hits per track and the difference between measured and expected number of hits.
The selected \velo tracks are used to seed the forward tracking algorithm, described in Section~\ref{sec:detector:trackreco}, with a tight search window corresponding to high-momentum tracks.
The resulting long tracks are then subject to cuts on the track fit \chisq and impact parameter \chisq.

% Introduce HLT2
The \hlttwo step applies a set of inclusive or exclusive final-state selections based on the full event information.
The input rate to \hlttwo is low enough that a simplified version of the full offline event reconstruction can be run.
Tracks which pass loose kinematic cuts are used to reconstruct candidates for common decays, such as \decay{\Kstarz}{\Kp\pim}, \decay{\phiz}{\Kp\Km}, \decay{\jpsi}{\mup\mun}, which are used in the subsequent selections.

% Exclusive lines
The \hlttwo stage has a large number of exclusive trigger lines, designed to search for specific processes.
Since the lines require full reconstruction of decay modes, they are sensitive to the slightly poorer track resolution resulting from the simplified reconstruction that runs in the trigger.
However, due to their smaller rate compared to inclusive lines, exclusive lines can use a more relaxed set of cuts.
Since the rate of visible $\cquark\cquarkbar$ production is much higher than that of $\bquark\bquarkbar$, the trigger lines designed to look for \cquark hadrons are exclusive lines with tight cuts.

%Inclusive lines
Inclusive \hlt lines trigger on partially reconstructed decays.
This is achieved by searching for typical distinguishing properties of \bquark hadron decays, such as displaced decay vertices and high-\pt tracks, as well as final-state particles present in key channels, such as the \phiz or \jpsi.
Since inclusive lines do not include cuts on the \bquark hadron invariant mass, they are less sensitive to the quality of the track reconstruction than exclusive lines.

% Topological lines
Inclusive `topological' trigger lines~\cite{BBDT} are used in both analyses presented in this thesis.
These are designed to trigger on partially reconstructed \bquark hadron decays with at least two charged particles in the final state and a displaced decay vertex.
Tracks are selected using fit quality, impact parameter and particle identification information.
Vertices are constructed using combinations of two, three or four of the selected tracks.
Signal candidates are selected based on several kinematic and isolation variables using a multivariate algorithm, trained on simulated signal events and collision data recorded in 2010.

%Inclusive phi line
There are several inclusive \hlttwo lines which look for particular decay products of key channels.
One of these is the inclusive \phiz line, which is used in the analysis presented in Chapter~\ref{ch:BsPhiPhi}.
The trigger line constructs \decay{\phiz}{\Kp\Km} candidates from track pairs and applies selection requirements in two steps.
The first step cuts on the \pt, impact parameter \chisq and track fit \chisq of the kaons, as well as the \pt, invariant mass, vertex fit \chisq and distance of closest approach to the primary vertex of the \phiz candidate.
Then \rich PID requirements are applied in the form of a cut on the difference in log-likelihood of the tracks being pions versus kaons.
%Considering only tracking efficiency, assuming a value of 96.6\,\%, the inclusive \phiz line gives an efficiency of 99.5\,\% for the \BsPhiPhi decay mode, compared to 87.1\,\% if all four tracks were required to be reconstructed.

\subsection{TIS and TOS}
\label{sec:detector:TISTOS}

%TIS and TOS
Trigger decisions at \lhcb are classified into three categories: `trigger on signal' (TOS), `trigger independently of signal' (TIS) and `trigger on both' (TOB).
TIS events are those which are triggered independently of the presence of the signal decay.
TOS events are those which are triggered on the signal decay independently of the presence of the rest of the event.
TOB events are those which require both the signal and the rest of the event in order to trigger.
The latter are typically caused by a combination of a signal track with a ghost track forming a displaced vertex~\cite{LHCb-PUB-2011-016}.

