\section{Choice of resonances}
\label{sec:BsPhiKK:resonances}

This section describes the choices of signal model with different combinations of $\Kp\Km$ resonances.
Table~\ref{tab:BsPhiKK:KKresonances} lists seventeen possible resonant contributions to the $\Kp\Km$ invariant mass spectrum.
Since the valence quark content of $\rho$ and $a$ mesons has no $\squark\squarkbar$ component, and the decay \BsPhiKK is a \decay{\bquark}{\squark\squarkbar\squark} transition, these resonances may be safely ignored.
This leaves eleven remaining $f^{(\prime)}$ and $\phi$ resonances, of which three spin-2 states --- the \ftwo{1565}, \ftwo{1640} and \ftwo{1810} --- are omitted from the PDG meson summary table.
The \ftwo{1565} is predominantly observed in nucleon--antinucleon annihilation and requires confirmation in other channels.
There is only one analysis which reports possibly seeing the decay \decay{\ftwo{1640}}{\Kp\Km}~\cite{Amsler:2006du}.
The authors state that it is only tentatively included in the fit.
The existence of the \ftwo{1810} requires confirmation, but there is some evidence of a $\Kp\Km$ channel~\cite{Costa:1980ji}.
For these reasons, these resonances are not included in the amplitude model.

Further information about which resonances to expect to contribute to the total amplitude can be taken from LHCb amplitude analyses of the \decay{\Bs}{\jpsi\Kp\Km}~\cite{LHCb-PAPER-2012-040} and \decay{\Bs}{\phi\pip\pim}~\cite{LHCb-PAPER-2016-028} decays.

\begin{table}[h]
	\begin{center}
		\caption[A list of $\Kp\Km$ resonances below 1.85\gevcc.]{A list of resonances below 1.85\gevcc that have been observed to decay to $\Kp\Km$. Branching fractions to $\Kp\Km$ are quoted where available from Ref.~\cite{PDG2016}.}
		\label{tab:BsPhiKK:KKresonances}
		\begin{tabular}{|l|c|c|r|}
			\hline
			Resonance             & Mass [\mevcc]          & Width [\mev]          & \BF(\decay{}{\Kp\Km}) \\\hline
			\fzero{980}           & $990 \pm 20$           & 10 to 100             & seen \\
			\azero{980}           & $980 \pm 20$           & $92 \pm 8$            & seen \\
			\phiz(1020)           & $1019.461 \pm 0.019$   & $4.266 \pm 0.031$     & $(4.89 \pm 0.05) \times 10^{-1}$ \\
			\ftwo{1270}           & $1275.1 \pm 1.2$       & $185.1^{+2.9}_{-2.4}$ & $(2.3 \pm 0.2)   \times 10^{-2}$ \\
			\atwo{1320}           & $1318.3^{+0.5}_{-0.6}$ & $107 \pm 5$           & $(2.45 \pm 0.4)  \times 10^{-2}$ \\
			\fzero{1370}          & $1475 \pm 6$           & $113 \pm 11$          & seen \\
			\azero{1450}          & $1474 \pm 19$          & $265 \pm 13$          & seen \\
			\fzero{1500}          & $1505 \pm 6$           & $109 \pm 7$           & $(4.3 \pm 0.5)   \times 10^{-2}$ \\
			\ftwop{1525}          & $1525 \pm 5$           & $73^{+6}_{-5}$        & $(4.44 \pm 0.11)  \times 10^{-1}$ \\
			\ftwo{1565}           & $1562 \pm 13$          & $134 \pm 8$           & \\
			\ftwo{1640}           & $1639 \pm 6$           & $99^{+60}_{-40}$      & seen \\
			\phiz(1680)           & $1680 \pm 20$          & $159 \pm 50$          & seen \\
			$\rho_3(1690)$        & $1696 \pm 4$           & $161 \pm 10$          & $(7.9 \pm 1.3)\times 10^{-3}$ \\
			$\rho$\xspace$(1700)$ & $1720 \pm 20$          & $250 \pm 100$         & seen \\
			\atwo{1700}           & $1732 \pm 16$          & $194 \pm 40$          & seen \\
			\fzero{1710}          & $1723^{+6}_{-5}$       & $139 \pm 8$           & seen \\
			\ftwo{1810}           & $1815 \pm 12$          & $197 \pm 22$          & \\
			\hline
		\end{tabular}
	\end{center}
\end{table}

The \BsPhipipi analysis~\cite{LHCb-PAPER-2016-028} observed and measured the branching fractions of the decays \BsPhifzero and \decay{\Bs}{\phi \ftwo{1270}} using the \BsPhiPhi branching fraction result from Chapter~\ref{ch:BsPhiPhi} for normalisation.
With knowledge of the $\pip\pim$ and $\Kp\Km$ channels of the \fzero{980} and \ftwo{1270}, it is possible to estimate the contribution of these intermediate resonances to the total amplitude.

In Ref.~\cite{LHCb-PAPER-2016-028}, a measurement is made of the ratio
\[
  \frac{\BF(\BsPhifzero)\BF(\decay{\fzero{980}}{\pip\pim})}{\BF(\BsPhiPhi)} = 0.068 \pm 0.008\text{\,(stat)} \pm 0.007\text{\,(syst)}.
\]
The ratio of partial widths of the \fzero{980} to $\pi\pi$ and $\PK\overline{\PK}$ final states
\footnote{This includes pairs of charged and neutral mesons.}
has been measured by the BESII collaboration to be~\cite{Ablikim:2005kp}:
\[
  \frac{\Gamma(\pi\overline{\pi})}{\Gamma(\pi\overline{\pi})+\Gamma(\PK\overline{\PK})} = 0.75^{+0.11}_{-0.13} .
\]
From this, the expected yield of the \BsPhifzero component in the \BsPhiKK amplitude fit, relative to the \BsPhiPhi component, can be calculated using:
\begin{equation}
  \frac{N_{\BsPhifzero}}{N_{\BsPhiPhi}} = \frac{\BF(\BsPhifzero)\BF(\decay{\fzero{980}}{\pip\pim})}{\BF(\BsPhiPhi)\BF(\decay{\phiz}{\Kp\Km})} \left(\frac{\Gamma(\pi\overline{\pi})+\Gamma(\PK\overline{\PK})}{\Gamma(\pi\overline{\pi})}-1\right).
\end{equation}
This gives $$\frac{N_{\BsPhifzero}}{N_{\BsPhiPhi}} = 0.05 \pm 0.03.$$% which is consistent with the S-wave fraction measured in a window $|\mass{\Kp\Km}-\mass{\phi}|<15\mevcc$ in Ref.~\cite{LHCb-PAPER-2014-026} ($2.1 \pm 1.6\%$).
Assuming $\sim 3000$ \BsPhiPhi events in the sample, this translates to $\sim 150$ \BsPhifzero events.
A \BsPhifzero component is therefore included in the fit to data.

Also in Ref.~\cite{LHCb-PAPER-2016-028}, a measurement of the ratio
\[
  \frac{\BF(\decay{\Bs}{\phiz\ftwo{1270}})\BF(\decay{\ftwo{1270}}{\pip\pim})}{\BF(\BsPhiPhi)} = 0.033 \pm 0.005\text{\,(stat)} \pm 0.003\text{\,(syst)}
\]
is given.
The branching fractions of the \ftwo{1270} to the $\pi\pi$ and $\PK\overline{\PK}$ final states are given by the PDG to be~\cite{PDG2016}:
\begin{eqnarray*}
  \Gamma(\pi\pi)        &=& \left(84.8^{+2.4}_{-1.2}\right)\%, \\
  \Gamma(K\overline{K}) &=& \left(4.6 \pm 0.4\right)\%.
\end{eqnarray*}
The expected yield of the \decay{\Bs}{\phiz\ftwo{1270}} component in the \BsPhiKK amplitude fit, relative to the \BsPhiPhi component, can therefore be calculated using:
\begin{equation}
  \frac{N_{\decay{\Bs}{\phiz\ftwo{1270}}}}{N_{\BsPhiPhi}} = \frac{\BF(\decay{\Bs}{\phiz\ftwo{1270}})\BF(\decay{\ftwo{1270}}{\pip\pim})}{\BF(\BsPhiPhi)\BF(\decay{\phiz}{\Kp\Km})} \frac{\Gamma(K\overline{K})}{\Gamma(\pi\pi)}.
\end{equation}
The result is $$\frac{N_{\decay{\Bs}{\phiz\ftwo{1270}}}}{N_{\BsPhiPhi}} = 0.0037 \pm 0.007,$$ which, assuming $\sim 3000$ \BsPhiPhi events, converts to a yield of $\sim 11$.
Therefore this component can be neglected.

The amplitude fit in Ref.~\cite{LHCb-PAPER-2016-028} includes a high-mass component, but there is ambiguity as to whether it is the \fzero{1370}, \fzero{1500} or a mixture of the two states.
The \fzero{1370} state is not well known: the PDG quotes a range of [1.2,1.5]\gevcc for the pole mass and [200,500]\mevcc for the width.
Measurements of the ratio of partial widths $\Gamma(\PK\overline{\PK})/\Gamma(\pi\overline{\pi})$ vary from 8 to 91\,\%.
By contrast, the \fzero{1500} state is much better established.
Assuming this high-mass component is entirely the \fzero{1500}, then the fit fraction of this component is equivalent to
\[
  F_{{\decay{\Bs}{\phiz\fzero{1500}}}} \equiv \frac{\BF(\decay{\Bs}{\phiz\fzero{1500}})\BF(\decay{\fzero{1500}}{\pip\pim})}{\BF(\BsPhipipi)} = 34.7 \pm 0.34\,\%.
\]
The PDG world average for the ratio of partial widths for this state is~\cite{PDG2016}
$$\frac{\Gamma(\PK\overline{\PK})}{\Gamma(\pi\overline{\pi})} = 0.241 \pm 0.028.$$
The expected yield relative to the \BsPhiPhi component is calculated using:
\begin{equation}
  \frac{N_{\decay{\Bs}{\phiz\fzero{1500}}}}{N_{\BsPhiPhi}} = \frac{F_{{\decay{\Bs}{\phiz\fzero{1500}}}}\BF(\BsPhipipi)}{\BF(\BsPhiPhi)\BF(\decay{\phiz}{\Kp\Km})} \frac{\Gamma(\PK\overline{\PK})}{\Gamma(\pi\overline{\pi})}.
\end{equation}
This gives $$\frac{N_{\decay{\Bs}{\phiz\fzero{1500}}}}{N_{\BsPhiPhi}} = 0.032 \pm 0.006.$$
This is large enough to expect $\sim 100$ events in the sample, hence this component needs to be considered in the fit.
However, due to the ambiguity over the identity of the high-mass component in the \BsPhipipi analysis, this cannot reliably be used to predict the number of \decay{\Bs}{\phiz\fzero{1500}} events.
%The hypothesis of a \decay{\Bs}{\phiz\fzero{1370}} component is also considered in the fit, but the inclusion of both the \fzero{1370} and \fzero{1500} is not.

In the amplitude analysis of the decay \decay{\Bs}{\jpsi\Kp\Km} in Ref.~\cite{LHCb-PAPER-2012-040}, the best fit model contains the \fzero{980}, the $\phiz(1020)$, the \fzero{1370}, the \ftwop{1525}, the \ftwo{1640}, the $\phiz(1680)$ and the \ftwo{1750}.
The \ftwo{1640}, $\phiz(1680)$ and \ftwo{1750} components are reported with fit fractions above $1\,\%$ and are important in describing the shape of the distribution above the mass of the \ftwop{1525}.
Therefore they are considered when choosing resonance models in this analysis.
Although it does not appear in the PDG, the existence of the \ftwo{1750} is reported by Belle in Ref.~\cite{Abe:2003vn} and is therefore considered.

This fit assumes that the `first' $\Kp\Km$ pair --- in the narrow invariant mass window --- is always from a \phiz meson.
Decays such as \decay{\Bs}{f_0\phiz}, \decay{\Bs}{f_0f_0}, nonresonant \decay{\Bs}{\Kp\Km \phiz} or even nonresonant \decay{\Bs}{\Kp\Km \Kp\Km} are neglected.

\section{Signal model}
\label{sec:BsPhiKK:signal}

Based on the arguments and calculations presented in Section~\ref{sec:BsPhiKK:resonances}, the $\Kp\Km$ resonances considered for the `minimal' fit in this analysis are \fzero{980}, $\phiz(1020)$ and the \ftwop{1525}, plus a nonresonant component to account for true three-body \BsPhiKK decays.
Additional models containing components for the \fzero{1370}, \fzero{1500}, \ftwo{1640}, $\phiz(1680)$, \fzero{1710} or \ftwo{1750} states are also considered.
As discussed in Section~\ref{sec:BsPhiKK:lineshapes}, the resonances are described by relativistic Breit--Wigner distributions, with the exception of the \fzero{980}, which is described by a Flatt\'e shape.

The shape of each Breit--Wigner resonance is controlled by the pole mass $m$ and natural width $\Gamma$.
The Flatt\'e parameters are the coupling $g_{\pi\pi}$ and the ratio of couplings $R_g = g_{\Kp\Km}/g_{\pip\pim}$.
Each component in the signal model has an overall scaling factor $f$, which is fixed to unity for the \BsPhiPhi component.
The nonresonant and spin-0 resonant amplitudes have a phase \deltazero.
The resonant amplitudes with spin $> 0$ have three complex helicity amplitudes: \Azero, \Aplus and \Aminus, controlled by the fit parameters $|\Azero|$, $|\Aplus|$, \deltazero, \deltaplus and \deltaminus.
The magnitude $|\Aminus|$ is calculated using the unitarity condition $\sum_{\lambda}|\helamp{\lambda}|^2=1$

The natural width of the $\phiz(1020)$ meson and the pole mass and natural width of the \ftwop{1525} meson are constrained to the PDG values, given in Table~\ref{tab:BsPhiKK:KKresonances}.
Gaussian constraints are applied to the parameters.
Asymmetric uncertainties are handled by using the average of the upper and lower uncertainty as the width of the constraint function.
The pole mass of the $\phiz(1020)$ meson is left free in the fit.
The \fzero{980} Flatt\'e couplings are taken from Ref.~\cite{LHCb-PAPER-2012-005} to be $g_{\pip\pim} = 199 \pm 30\mev$ and $g_{\Kp\Km} = (3.0 \pm 0.3)\times g_{\pip\pim}$.
The values of the couplings are later varied to quantify a systematic uncertainty.
The pole mass and natural width of the \fzero{1500} are fixed to the \lhcb measurement in Ref.~\cite{LHCb-PAPER-2013-069}.
The parameters of the \ftwo{1750} are taken from the Belle measurement in Ref.~\cite{Abe:2003vn}.
The Breit--Wigner parameters of all other resonances considered in the fit are fixed to the central values given in Table~\ref{tab:BsPhiKK:KKresonances}.

The time-integrated angular distribution of a $\decay{\PP}{\PV\PV}$ or $\decay{\PP}{\PV\PT}$ decay, assuming small \CP violation, is sensitive only to one difference in phase between two of the helicity amplitudes.
Additionally, interference between resonances leads to sensitivity in the PDF to the difference in phase between the resonant components for a particular polarisation.\footnote{There is no interference between amplitudes with different polarisations.}
Since the PDF depends on the differences between phases, at least one phase in the total amplitude must be fixed.
The phase of \Azero for the \BsPhiPhi component is fixed to zero following the convention established by previous analyses of this decay, which allows for easier comparison of results.
The time-integrated angular distribution of the \BsPhiPhi decay, assuming equal numbers of \Bs and \Bsb, is sensitive to only the difference between the phases of the transverse helicity amplitudes and not their absolute values.
Since the magnitude of \Aminus is known to be the smaller of the two, the phase \deltaminus of the \BsPhiPhi component is fixed to the value found in the \lhcb angular analysis of \BsPhiPhi in Ref.~\cite{LHCb-PAPER-2014-026}, to simplify comparison.
The phases of all other resonant components are left free, since they can be measured relative to the \BsPhiPhi component.

