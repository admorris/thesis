\section{Background model}
\label{sec:BsPhiKK:background}

The background model used in the amplitude fit includes the three sources of background included in the \mass{\Kp\Km\Kp\Km} fit.
The relative fraction of each component is fixed to the values found in Section~\ref{sec:BsPhiKK:massfits}.
The \mass{\Kp\Km} distribution of the combinatorial background is modelled a histogram of the upper sideband of the data sample, with $\mass{\phiz\Kp\Km}>5.5\gevcc$.
The \mass{\Kp\Km} distribution of the \BdPhiKstar background is modelled using a histogram from the fully selected simulation sample.
Although the resonant structure of \LbPhipK is unknown, this background is assumed to be dominated by \decay{\Lb}{\phi\Lambda(1520)} decays.
It is therefore modelled using the \mass{\Kp\Km} distribution of misidentified \decay{\Lb}{\phi\Lambda(1520)} generator-level events with momentum-smearing applied.
The \mass{\Kp\Km} distributions of the background components are shown in Figure~\ref{fig:bkgmasshists}.

The angular distribution of the entire background is expressed as a sum of spherical harmonics, $Y$, and associated Legendre polynomials, $P$, given in Equation \ref{eq:background_angdep}:
\begin{equation}
  \label{eq:background_angdep}
  B(\theta_1,\theta_2,\Phi) = \sum\limits_{i,j,k} c^{ijk} Y_{j}^{k}(\cos{\theta_1},\Phi) P_{i} (\cos{\theta_2}),
\end{equation}
where the coefficients of each term in the sum, $c^{ijk}$, are determined using the following sum over events in the sideband sample:
\begin{equation}
  \label{eq:background_coefficients}
  c^{ijk} = \frac{1}{N} \sum\limits_{n=0}^{N} \frac{2i+1}{2} Y_{j}^{k}(\cos{\theta_{1n}},\Phi_n) P_{i} (\cos{\theta_{2n}}),
\end{equation}
where $N$ is the number of events in the sample.
The indices $i$, $j$ and $k$ are required to be below $3$, and $j$ must be greater than $k$.
Only coefficients larger than $5\sigma$ are kept, where $\sigma$ is calculated as:
\begin{equation}
  \label{eq:accsigma}
  \sigma = \sqrt{\frac{1}{N^2} \left((c^{ijk})^2 - \frac{(c^{ijk})^2}{N}\right)}.
\end{equation}
The accepted coefficients are $c^{000}$, $c^{002}$ and $c^{200}$; their values are summarised in Table~\ref{tab:background_coefficients}

\begin{table}[h]
\centering
\caption{Values of the accepted background function coefficients.}
\label{tab:background_coefficients}
\begin{tabular}{|c|r|}
\hline
Coefficient & Value \\
\hline
$c^{000}$ & $(7.05 \pm 0.00)\times 10^{-2}$ \\
$c^{002}$ & $(1.91 \pm 0.32)\times 10^{-2}$ \\
$c^{200}$ & $(3.71 \pm 0.68)\times 10^{-2}$ \\
\hline
\end{tabular}
\end{table}

The one-dimensional projections of the background from the collision data sideband sample and the fitted background function for each of the helicity angles are shown in Figure~\ref{fig:bkg1dproj}.
The two-dimensional projections of the background function are shown in Figure~\ref{fig:bkg2dproj}.

\begin{figure}[h]
\centering
\includegraphics[width=0.3\textwidth]{body/BsPhiKK/figs/background_nomva_mKK_proj}
\includegraphics[width=0.3\textwidth]{body/BsPhiKK/figs/phikstar_background_mKK_proj}
\includegraphics[width=0.3\textwidth]{body/BsPhiKK/figs/phiKp_background_mKK_proj}
\caption[Histograms used for the mass-dependent part of the background.]{Histograms used for the \mass{\Kp\Km}-dependent part of the combinatorial (left), \BdPhiKstar (centre) and \LbPhipK (right) backgrounds.}
\label{fig:bkgmasshists}
\end{figure}
\begin{figure}[h]
\centering
\includegraphics[width=0.3\textwidth]{body/BsPhiKK/figs/background_nomva_phi_proj}
\includegraphics[width=0.3\textwidth]{body/BsPhiKK/figs/background_nomva_ctheta_1_proj}
\includegraphics[width=0.3\textwidth]{body/BsPhiKK/figs/background_nomva_ctheta_2_proj}
\caption[1D projections of the angular part of the background.]{1D projections of the angular part of the background. The black points are sideband data events, and the red line is the fitted background function.}
\label{fig:bkg1dproj}
\end{figure}
\begin{figure}[h]
\centering
\includegraphics[width=0.3\textwidth]{body/BsPhiKK/figs/background_nomva_ctheta_1-phi}
\includegraphics[width=0.3\textwidth]{body/BsPhiKK/figs/background_nomva_ctheta_2-phi}
\includegraphics[width=0.3\textwidth]{body/BsPhiKK/figs/background_nomva_ctheta_1-ctheta_2}
\caption{2D projections of the angular part of the fitted background function.}
\label{fig:bkg2dproj}
\end{figure}
\FloatBarrier

