% General introduction to the chapter
\section{The Standard Model}
\label{theory:sec:sm}

% Introduction to the SM
The Standard Model of particle physics~\cite{Abers:1973qs} describes the behaviour and properties of subatomic particles, including three of the four fundamental forces in physics: electromagnetism, the weak interaction and the strong interaction.
It was developed from separate descriptions of the strong and electroweak interactions formulated during the 1960s and early 1970s.
The acceptance of the Standard Model was motivated by the discoveries of the charm quark~\cite{Augustin:1974xw,Aubert:1974js}, tau lepton~\cite{Perl:1975bf}, bottom quark~\cite{Herb:1977ek}, gluon~\cite{Berger:1978rr,Berger:1979cj,Brandelik:1979bd,Bartel:1979ut} and the $W^\pm$ and $Z^0$ bosons~\cite{Arnison:1983rp,Arnison:1983mk,Bagnaia:1983zx,Banner:1983jy}, in the period from 1974 to 1983.
The final three particles predicted by the Standard Model, namely the top quark, tau neutrino and Higgs boson, were discovered in 1995~\cite{Abachi:1995iq,Abe:1995hr}, 2000~\cite{Kodama:2000mp} and 2012~\cite{Aad:2012tfa,Chatrchyan:2012xdj} respectively.


% More technical introduction
The Standard Model is a quantum field theory with twelve fermion fields (quarks and leptons), twelve gauge boson fields (eight gluons, $W^+$, $W^-$, $Z^0$ and \Pgamma) and a complex scalar doublet field (the Higgs boson).
The particle content of the Standard Model is summarised in Figure~\ref{fig:theory:sm}.
The theory contains the internal symmetries of the $SU(3) \times SU(2) \times U(1)$ gauge group.

\begin{figure}[t]
  \centering
  \includegraphics[width=\textwidth]{body/theory/figs/Standard_Model_of_Elementary_Particles}
  \caption[The particle content of the Standard Model.]{The particle content of the Standard Model \cite{wikipedia:sm}.}
  \label{fig:theory:sm}
\end{figure}

% Electromagnetism
Electromagnetism is a familiar interaction whose effects can be observed on a macroscopic scale.
It is the interaction responsible for phenomena including electricity, magnetism, chemical interactions and the structure of matter.
The charge of the interaction is called electric charge, and it is mediated by a massless vector boson called the photon ($\gamma$).

% Weak force
The weak interaction is a short-range force, mediated by three massive vector bosons: $W^+$, $W^-$ and $Z^0$.
It couples to all species of fundamental fermions, but only to left-handed fermions and right-handed antifermions.
Here, `handedness' refers to the chirality of a particle.
Chirality can be defined using the fifth Dirac gamma matrix $\gamma^5 \equiv i \gamma^0 \gamma^1 \gamma^2 \gamma^3$.
A Dirac fermion field $\psi$ can be projected onto its left-handed and right-handed components using
\begin{equation}
	\psi_L = \frac{1}{2} \left(1-\gamma^5\right)\psi \text{ and } \psi_R = \frac{1}{2} \left(1+\gamma^5\right).
\end{equation}
It is the interaction through which neutrino scattering and beta decay occur, and it plays a crucial role in the proton--proton fusion process inside stars.
The charge of the weak interaction is called `weak isospin'.
Unlike the electromagnetic and strong interactions, the weak force can change quark flavour.

% Electroweak interaction and the Higgs mechanism
The electroweak interaction~\cite{Glashow:1961tr,Weinberg:1967tq,Salam:1968rm} is a unified description of electromagnetism and the weak interaction.
It is mediated by four massless vector-boson fields: $W^1$, $W^2$ and $W^3$ with symmetry $SU(2)$ and coupling $g$, and $B$ with symmetry $U(1)$ and coupling $g^\prime$.
The observed electromagnetic and weak interactions arise due to the Higgs mechanism~\cite{Englert:1964et,Higgs:1964pj,Guralnik:1964eu}, which introduces a complex scalar doublet field with a potential that has a non-zero vacuum expectation value.
Upon spontaneous breaking of the symmetry, the physical (massive) weak gauge bosons and the (massless) photon appear as linear combinations of the $W$ and $B$ fields:
\begin{equation}
  \begin{array}{rcl}
    W^+    &=& \frac{1}{\sqrt{2}} \left( W^1 - i W^2 \right), \\
    W^-    &=& \frac{1}{\sqrt{2}} \left( W^1 + i W^2 \right), \\
    Z^0    &=& B \sin\theta_W + W^3 \cos\theta_W, \\
    \gamma &=& B \cos\theta_W - W^3 \sin\theta_W,
  \end{array}
\end{equation}
where $\theta_W$ is a free parameter, the Weinberg angle~\cite{Weinberg:1967tq}, which relates the masses of the $W^\pm$ and $Z^0$ bosons by $m_W = m_Z \cos\theta_W$.
The quarks and charged leptons acquire mass via their coupling to the Higgs field.
The electroweak interaction is discussed in further detail in Section~\ref{sec:app:ew}.

% The strong interaction
The strong interaction is the force which binds quarks to make hadrons and nucleons to make nuclei.
%It is the dominant interaction for hard scattering processes in high-energy proton--proton collisions, such as at the \lhc.
%The \decay{\bquark}{\squark\squarkbar\squark} transition, which is the focus of the analyses presented in this thesis, has contributions from both the electroweak and strong interactions.
The charge of the strong interaction is called `colour', which can take the values `red', `green' or `blue' (\PR, \PG, \PB) for quarks, or `antired', `antigreen' or `antiblue' ($\overline{\PR}$, $\overline{\PG}$, $\overline{\PB}$) for antiquarks.
Neutral combinations of colour charges may be formed from either a colour with its anticolour or the sum of all three colours or anticolours~\cite{GellMann:1964nj} \ie
\begin{equation}
  \PR \overline{\PR} = \PG \overline{\PG} = \PB \overline{\PB} = \PR \PG \PB = \overline{\PR} \overline{\PG} \overline{\PB} = 0.
\end{equation}
The interaction is mediated by eight massless vector boson fields (gluons) with symmetry $SU(3)$ and coupling $g_s$.
Each gluon field carries colour and anticolour in combinations corresponding to the Gell-Mann matrices, are electrically neutral and have zero weak isospin.
At low energy scales, the value of $g_s$ is large enough that perturbation theory cannot be used in calculations of strong interaction processes.
The size of $g_s$, combined with the gluon self-interaction vertex, restricts the strong force to a short range.
This is known as colour confinement~\cite{Fritzsch:1974ki}.
The strong interaction is discussed in further detail in Section~\ref{sec:app:qcd}.

% Quarks
Quarks are fundamental fermions which couple to the strong, weak and electromagnetic interactions~\cite{GellMann:1964nj}.
There are six `flavours' of quark, three of which (\uquark, \cquark and \tquark) have electric charge $+2/3$ and weak isospin $+1/2$, and three (\dquark, \squark and \bquark) have electric charge $-1/3$ and weak isospin $-1/2$.
%The quarks and their properties are summarised in Table~\ref{tab:theory:quarks}.
% Hadrons
Colour confinement means that quarks are never observed in isolation, but only in colour-singlet bound states called hadrons.
Hadrons are categorised as either mesons ($\quark\quarkbar$ states) or baryons ($\quark\quark\quark$ or $\quarkbar\quarkbar\quarkbar$ states).
The $\quark\quarkbar$ colour-singlet state is $$\frac{1}{\sqrt{3}}\left(\PR \overline{\PR} + \PG \overline{\PG} + \PB \overline{\PB}\right),$$ and the colour-singlet $\quark\quark\quark$ state is $$\frac{1}{\sqrt{6}}\left(RGB-RBG+GBR-GRB+BRG-BGR\right).$$
Other colour-singlet combinations of quarks (\eg $\quark\quarkbar\quark\quarkbar$, $\quark\quark\quark\quark\quarkbar$ \etc) are possible and are known as exotic hadrons~\cite{GellMann:1964nj}.

% Leptons
Leptons are fundamental fermions which do not couple to the strong interaction.
They are divided into three charged leptons (\electron, \muon and \tauon), with electric charge $-1$ and weak isospin $-1/2$, and three neutrinos (\neue, \neum and \neut), which are electrically neutral and have weak isospin $+1/2$.
%The leptons and their properties are summarised in Table~\ref{tab:theory:leptons}.


The field of experimental particle physics is centred around rigorous tests of the Standard Model and searching for effects which do not fit within its framework.
To date, the only observed behaviour of fundamental particles to disagree with the theory is that neutrinos oscillate between flavours~\cite{Ahmad:2001an}.
This can only be explained by the introduction of non-zero neutrino masses.
In addition, the Standard Model does not account for gravity, dark matter, dark energy and the matter--antimatter asymmetry of the universe.

